/*
A knight's tour is a sequence of moves of a knight on a chessboard such that the knight visits every square only once.
If the knight ends on a square that is one knight's move from the beginning square (so that it could tour the board again immediately, following
the same path), the tour is closed, otherwise it is open. Variations of the knight's tour problem involve chessboards of different sizes than
the usual 8 � 8, as well as irregular boards.
The knight's tour problem is an instance of the more general Hamiltonian path problem in graph theory. Unlike the general Hamiltonian path problem,
the knight's tour problem can be solved in linear time.
Warnsdorf's rule is a heuristic for finding a knight's tour. The knight is moved so that it always proceeds to the square from which the knight
will have the fewest onward moves. When calculating the number of onward moves for each candidate square, we do not count moves that revisit any
square already visited. It is, of course, possible to have two or more choices for which the number of onward moves is equal; there are various
methods for breaking such ties, including one devised by Pohl, another by Arnd Roth and another by Squirrel and Cull.
The following Prolog code aims at solve the knight's tour problem using the Warnsdorf's heuristic and a random breaking tie on a general board.
*/

% range(+lowValue, +highValue, -valuesInRange)
% Generates a list representation for an inclusive range.
range(Low, High, Values) :-
	integer(Low),
	integer(High),
	Low =< High,
	range_(Low, High, Values).
range_(Low, Low, Values) :-
	!,
	Values = [Low].
range_(Low, High, [Low|Values]) :- NewLow is Low + 1, range_(NewLow, High, Values).

% boardPos(+boardWidth, +boardHeight, ?rank, ?file)
% The positions on the chess board.
boardPos(W, H, X, Y) :-
	range(1, W, WidthRange),
	range(1, H, HeightRange),
	member(X, WidthRange),
	member(Y, HeightRange).

% knightMoves(+boardWidth, +boardHeight, ?startingRank, ?startingFile, ?arrivalRank, ?arrivalFile)
% The squares that can be reached with one move from a knight piece.
% In other words, the relationship between the current position of a knight and the next one on a chess board.
knightMoves(W, H, X, Y, NewX, NewY) :-
	boardPos(W, H, X, Y),								% to check if the starting position is on the board
	( NewX is X - 1, NewY is Y - 2;
		NewX is X - 1, NewY is Y + 2;
		NewX is X + 1, NewY is Y - 2;
		NewX is X + 1, NewY is Y + 2;
		NewX is X - 2, NewY is Y - 1;
		NewX is X - 2, NewY is Y + 1;
		NewX is X + 2, NewY is Y - 1;
		NewX is X + 2, NewY is Y + 1 ),
	boardPos(W, H, NewX, NewY).					% to check if the arrival position is on the board

% unvisitedKnightMoves(+boardWidth, +boardHeight, +visitedSquares, ?startingRank, ?startingFile, ?arrivalRank, ?arrivalFile)
% The squares reachable with a knight move that are not visited yet.
unvisitedKnightMoves(W, H, Visits, X, Y, NewX, NewY) :-
	is_list(Visits),
	knightMoves(W, H, X, Y, NewX, NewY),
	not(member((NewX, NewY), Visits)).

% onwardKnightMovesCount(+boardWidth, +boardHeight, +visitedSquares, ?startingRank, ?startingFile, ?arrivalRank, ?arrivalFile, -onwardMovesCount)
% The number of unvisited squares reachable with an onward knight move.
% It represents the number of edges for a node in the knight's moves graph. Since the edges are bidirectional, the number of single moves that can
% be made to get to a certain position is equal to the number of single moves that can be made from that position. However, consider both the
% starting square and the arrival square makes it possible to question the reachability of one position from another very easily.
onwardKnightMovesCount(W, H, Visits, X, Y, NewX, NewY, Count) :-
	findall(_, unvisitedKnightMoves(W, H, Visits, X, Y, NewX, NewY), Moves),
	length(Moves, Count).

% warnsdorfEvaluation(+boardWidth, +boardHeight, +visitedSquares, ?startingRank, ?startingFile, ?arrivalRank, ?arrivalFile, ?warnsdorfMoveScore)
% The heuristic score based on the number of onward moves for each candidate square,
% without consider any square already visited.
warnsdorfEvaluation(W, H, Visits, X, Y, NewX, NewY, Score) :-
	unvisitedKnightMoves(W, H, Visits, X, Y, NewX, NewY),
	onwardKnightMovesCount(W, H, [(NewX, NewY) | Visits], NewX, NewY, _, _, Score).

% warnsdorfMinScores(+movesWithScoresList, +currentMinimums, -minimums)
% True when minimums are the candidate squares with the smallest score.
warnsdorfMinScores([], Mins, Mins).
warnsdorfMinScores([(X, Y, NewX, NewY, Score)|T], [(_, _, _, _, CurScore)|_], Mins) :-
	Score < CurScore,
	!,
	warnsdorfMinScores(T, [(X, Y, NewX, NewY, Score)], Mins).
warnsdorfMinScores([(X, Y, NewX, NewY, Score)|T], [(CurX, CurY, CurNewX, CurNewY, CurScore)|CurT], Mins) :-
	Score =:= CurScore,
	!,
	warnsdorfMinScores(T, [(X, Y, NewX, NewY, Score),(CurX, CurY, CurNewX, CurNewY, CurScore)|CurT], Mins).
warnsdorfMinScores([_|T], X, Mins) :-
	warnsdorfMinScores(T, X, Mins).

% warnsdorfBestMoves(+boardWidth, +boardHeight, +visitedSquares, ?startingRank, ?startingFile, ?BestArrivalSquares)
% Searches the squares from which the knight has the fewest onward moves.
warnsdorfBestMoves(W, H, Visits, X, Y, BestMoves) :-
	findall((X, Y, NewX, NewY, Score), warnsdorfEvaluation(W, H, Visits, X, Y, NewX, NewY, Score), [HE|T]),
	warnsdorfMinScores(T, [HE], BestMoves).

% getByIndex(+list, +index, -element)
% Retrieves a list element by its index.
getByIndex([H|_], 0, H) :- !.
getByIndex([_|T], I, E) :- NewIndex is I-1, getByIndex(T, NewIndex, E).

% randomMember(+list, -randomELement)
% Randomly retrieves a member of a list. Fails if the list is empty.
randomMember(List, X) :-
	list(List),
	length(List, Len),
	Len > 0,
	rand_int(Len, N),
	getByIndex(List, N, X).

% knightTour(+boardWidth, +boardHeight, +startingRank, +startingFile, -knightPath)
% Finds a sequence of moves of a knight on a chessboard such that the knight visits every square only once.
knightTour(W, H, X, Y, Path) :-
	knightTour_(W, H, [], X, Y, Path).
knightTour_(W, H, Visits, X, Y, Path) :-
	length(Visits, L),
	NewVisits = [(X, Y) | Visits],
	(	L =:= W * H - 1 ->	reverse(NewVisits, Path);
												% to select a random option in case of more choices with the same number of onward moves
												warnsdorfBestMoves(W, H, Visits, X, Y, BestMoves),
												randomMember(BestMoves, (_, _, NewX, NewY, _)),
												knightTour_(W, H, NewVisits, NewX, NewY, Path)	).
/*
Equivalent to:
knightTour_(W, H, Visits, X, Y, Path) :-
	length(Visits, L),
	L =:= W * H - 1,
	NewVisits = [(X, Y) | Visits],
	reverse(NewVisits, Path),
	!.
knightTour_(W, H, Visits, X, Y, Path) :-
	length(Visits, L),
	L < W * H - 1,
	warnsdorfBestMoves(W, H, Visits, X, Y, BestMoves),
	NewVisits = [(X, Y) | Visits],
	randomMember(BestMoves, (_, _, NewX, NewY, _)),
	knightTour_(W, H, NewVisits, NewX, NewY, Path).
*/



/*
Possible queries (assuming N=8 and M=8).

1) range(1, 8, X).
2) boardPos(8, 8, 4, 4).
3) boardPos(8, 8, 9, 1).
4) boardPos(8, 8, 2, Y).
5) boardPos(8, 8, X, Y).
6) knightMoves(8, 8, 1, 1, X, Y).
7) knightMoves(8, 8, 4, 4, X, Y).
8) knightMoves(8, 8, X, Y, 8, 8).
9) knightMoves(8, 8, 4, 4, 6, 6).
10) knightMoves(8, 8, 4, 4, 3, 2).
11) knightMoves(8, 8, X1, Y1, X2, Y2).
12) unvisitedKnightMoves(8, 8, [], 3, 4, X, Y).
13) unvisitedKnightMoves(8, 8, [(4, 2), (4, 6)], 3, 4, X, Y).
14) unvisitedKnightMoves(8, 8, [(4, 4)], X, Y, 4, 4).
15) unvisitedKnightMoves(8, 8, [], X1, Y1, X2, Y2).
16) onwardKnightMovesCount(8, 8, [], 4, 4, X, Y, Count).
17) onwardKnightMovesCount(8, 8, [(6, 3)], 4, 4, X, Y, Count).
18) onwardKnightMovesCount(8, 8, [], 4, 4, X, Y, 8).
19) onwardKnightMovesCount(8, 8, [], 4, 4, X, Y, 3).
20) onwardKnightMovesCount(8, 8, [], X, Y, 4, 4, Count).
21) onwardKnightMovesCount(8, 8, [], X, Y, 8, 8, Count).
22) onwardKnightMovesCount(8, 8, [], 1, 1, 3, 2, 1).
23) onwardKnightMovesCount(8, 8, [], 1, 1, 3, 3, 1).
24) onwardKnightMovesCount(8, 8, [], X1, Y1, X2, Y2, Count).
25) warnsdorfEvaluation(8, 8, [], 6, 6, X, Y, Score).
26) warnsdorfEvaluation(8, 8, [(5, 4), (4, 7)], 6, 6, X, Y, Score).
27) warnsdorfEvaluation(8, 8, [], X1, Y1, 7, 7, Score).
28) warnsdorfEvaluation(8, 8, [], X1, Y1, 7, 7, 3).
29) warnsdorfEvaluation(8, 8, [], X1, Y1, X2, Y2, Score).
30) warnsdorfEvaluation(8, 8, [], X1, Y1, X2, Y2, 2).
31) warnsdorfMinScores([(1, 1, 3, 2, 4), (1, 4, 2, 6, 8), (1, 1, 2, 3, 5)], [(1, 1, 3, 2, 10)], Min).
32) warnsdorfMinScores([(1, 1, 3, 2, 4), (1, 4, 2, 6, 8), (1, 1, 2, 3, 4)], [(1, 1, 3, 2, 10)], Min).
33) warnsdorfMinScores([(1, 1, 3, 2, 4), (1, 4, 2, 6, 8), (1, 1, 2, 3, 4)], [(1, 1, 3, 2, 10)], [(X, Y, NextX, NextY, S)|_]).
34) warnsdorfMinScores([(1, 1, 3, 2, 4), (1, 4, 2, 6, 8), (1, 1, 2, 3, 4)], [(1, 1, 3, 2, 10)], [(_, _, _, _, 4)|_]).
35) warnsdorfMinScores([(1, 1, 3, 2, 4), (1, 4, 2, 6, 8), (1, 1, 2, 3, 4)], [(1, 1, 3, 2, 10)], [(_, _, _, _, 8)|_]).
36) warnsdorfBestMoves(8, 8, [], 1, 1, BestMoves).
37) warnsdorfBestMoves(8, 8, [(3, 2)], 1, 1, BestMoves).
38) warnsdorfBestMoves(8, 8, [], 1, 1, BestMoves), member((_, _, 3, 2, _), BestMoves).
39) warnsdorfBestMoves(8, 8, [], 1, 1, BestMoves), member((_, _, 4, 4, _), BestMoves).
40) warnsdorfBestMoves(8, 8, [], X, Y, BestMoves).
41) getByIndex([1, 2, 3, 4], 2, X).
42) randomMember([1, 2, 3, 4], X).
43) knightTour(8, 8, 1, 1, Path).
*/