package it.unibo.chess.view.dummy

import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.Intent
import it.unibo.chess.view.{GenericWindow, MessageTypes}

class DummyWindowManager extends GenericWindow {

  override def showView(): Unit = {}

  override def closeView(): Unit = {}

  override def isShowing: Boolean = true

  override def setTitle(title: String): Unit = {}

  override def getTitle: String = ""

  override def setScene(scene: Intent): Unit = {}

  override def getScene: Option[SceneType.Value] = None

  override def showMessage(message: String, messageType: MessageTypes.MessageType): Unit = {}

  override def showMessage(title: String, message: String, messageType: MessageTypes.MessageType): Unit = {}

}
