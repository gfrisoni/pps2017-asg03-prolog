package it.unibo.chess.controller.settings

import com.typesafe.config.ConfigException.Missing
import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}
import org.junit.runner.RunWith
import org.mongodb.scala.ServerAddress
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class MongoSettingsSpecs extends FunSpec with Matchers {

  describe("The settings related to MongoDb") {

    it("should be able to be loaded from a specific configuration") {
      val config: Config = ConfigFactory.parseString(
        """mongo {
          |  username = ClientUser
          |  password = tgNLKWNZrLdjp37B
          |  cluster = [
          |    {
          |      host = chessmultiplayercluster-shard-00-00-j8clo.mongodb.net
          |      port = 27017
          |    },
          |    {
          |      host = chessmultiplayercluster-shard-00-01-j8clo.mongodb.net
          |      port = 27017
          |    },
          |    {
          |      host = chessmultiplayercluster-shard-00-02-j8clo.mongodb.net
          |      port = 27017
          |    }
          |  ]
          |  authSource = admin
          |  dbName = chess-mp
          |  gamesCollectionName = games
          |}
        """.stripMargin)
      val mongoSettings: MongoSettings = MongoSettings(config)
      mongoSettings.username should be("ClientUser")
      mongoSettings.password should be("tgNLKWNZrLdjp37B")
      mongoSettings.cluster should be(List(
        ServerAddress("chessmultiplayercluster-shard-00-00-j8clo.mongodb.net", 27017),
        ServerAddress("chessmultiplayercluster-shard-00-01-j8clo.mongodb.net", 27017),
        ServerAddress("chessmultiplayercluster-shard-00-02-j8clo.mongodb.net", 27017)
      ))
      mongoSettings.authSource should be("admin")
      mongoSettings.dbName should be("chess-mp")
      mongoSettings.gamesCollectionName should be("games")
    }

    it("should be able to be loaded from the file configuration") {
      val mongoSettings: MongoSettings = MongoSettings()
      mongoSettings.username should be("ClientUser")
      mongoSettings.password should be("tgNLKWNZrLdjp37B")
      mongoSettings.cluster should be(List(
        ServerAddress("chessmultiplayercluster-shard-00-00-j8clo.mongodb.net", 27017),
        ServerAddress("chessmultiplayercluster-shard-00-01-j8clo.mongodb.net", 27017),
        ServerAddress("chessmultiplayercluster-shard-00-02-j8clo.mongodb.net", 27017)
      ))
      mongoSettings.authSource should be("admin")
      mongoSettings.dbName should be("chess-mp")
      mongoSettings.gamesCollectionName should be("games")
    }

    it("should not be loaded if configuration parameters are missing") {
      val incompleteConfig: Config = ConfigFactory.parseString(
        """mongo {
          |  username = ClientUser
          |  authSource = admin
          |  dbName = chess-mp
          |  gamesCollectionName = games
          |}
        """.stripMargin)
      an [Missing] should be thrownBy MongoSettings(incompleteConfig)
    }

    it("should be properly represent in string format") {
      val mongoSettings: MongoSettings = MongoSettings()
      mongoSettings.toString should be(
        """username = ClientUser
          |password = <hidden>
          |cluster = {chessmultiplayercluster-shard-00-00-j8clo.mongodb.net:27017;chessmultiplayercluster-shard-00-01-j8clo.mongodb.net:27017;chessmultiplayercluster-shard-00-02-j8clo.mongodb.net:27017}
          |authSource = admin
          |databaseName = chess-mp
          |gamesCollectionName = games""".stripMargin)
    }

  }

}
