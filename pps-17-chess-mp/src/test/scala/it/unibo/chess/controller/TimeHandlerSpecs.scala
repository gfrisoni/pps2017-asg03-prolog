package it.unibo.chess.controller

import it.unibo.chess.controller.scheduling.TimeHandler
import it.unibo.chess.controller.scheduling.TimeHandler.RelTime.toRelTime
import org.scalatest.{FunSpec, PrivateMethodTester}

import scala.concurrent.duration.{FiniteDuration, SECONDS}

class TimeHandlerSpecs extends FunSpec with PrivateMethodTester {

  describe("A time handler") {

    it("should keep the real time consistent with the refresh rate") {
      val clockRate: Long = TimeHandler invokePrivate PrivateMethod[Long]('clockRate)()
      val realTimeDuration: FiniteDuration = FiniteDuration(1L, SECONDS)

      assert(realTimeDuration.rel.toMillis * clockRate == realTimeDuration.toMillis)
    }

  }

}
