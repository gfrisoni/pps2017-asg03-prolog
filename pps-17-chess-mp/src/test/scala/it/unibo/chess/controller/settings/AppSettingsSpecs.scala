package it.unibo.chess.controller.settings

import com.typesafe.config.ConfigException.Missing
import com.typesafe.config.{Config, ConfigFactory}
import org.junit.runner.RunWith
import org.mongodb.scala.ServerAddress
import org.scalatest.{FunSpec, Matchers}
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class AppSettingsSpecs extends FunSpec with Matchers {

  describe("The application settings") {

    it("should be able to be loaded data from a specific configuration") {
      val config: Config = ConfigFactory.parseString(
        """mongo {
          |  username = ClientUser
          |  password = tgNLKWNZrLdjp37B
          |  cluster = [
          |    {
          |      host = chessmultiplayercluster-shard-00-00-j8clo.mongodb.net
          |      port = 27017
          |    },
          |    {
          |      host = chessmultiplayercluster-shard-00-01-j8clo.mongodb.net
          |      port = 27017
          |    },
          |    {
          |      host = chessmultiplayercluster-shard-00-02-j8clo.mongodb.net
          |      port = 27017
          |    }
          |  ]
          |  authSource = admin
          |  dbName = chess-mp
          |  gamesCollectionName = games
          |}
        """.stripMargin)
      val appSettings = AppSettings(mongoConfig = config)
      appSettings.mongoSettings.username should be("ClientUser")
      appSettings.mongoSettings.password should be("tgNLKWNZrLdjp37B")
      appSettings.mongoSettings.cluster should be(List(
        ServerAddress("chessmultiplayercluster-shard-00-00-j8clo.mongodb.net", 27017),
        ServerAddress("chessmultiplayercluster-shard-00-01-j8clo.mongodb.net", 27017),
        ServerAddress("chessmultiplayercluster-shard-00-02-j8clo.mongodb.net", 27017)
      ))
      appSettings.mongoSettings.authSource should be("admin")
      appSettings.mongoSettings.dbName should be("chess-mp")
      appSettings.mongoSettings.gamesCollectionName should be("games")
    }

    it("should be able to be loaded from the file configuration") {
      val appSettings = AppSettings()
      appSettings.mongoSettings.username should be("ClientUser")
      appSettings.mongoSettings.password should be("tgNLKWNZrLdjp37B")
      appSettings.mongoSettings.cluster should be(List(
        ServerAddress("chessmultiplayercluster-shard-00-00-j8clo.mongodb.net", 27017),
        ServerAddress("chessmultiplayercluster-shard-00-01-j8clo.mongodb.net", 27017),
        ServerAddress("chessmultiplayercluster-shard-00-02-j8clo.mongodb.net", 27017)
      ))
      appSettings.mongoSettings.authSource should be("admin")
      appSettings.mongoSettings.dbName should be("chess-mp")
      appSettings.mongoSettings.gamesCollectionName should be("games")
    }

    it("should not be loaded if configuration parameters of internal settings are missing") {
      val incompleteMongoConfig: Config = ConfigFactory.parseString(
        """mongo {
          |  username = ClientUser
          |  authSource = admin
          |  dbName = chess-mp
          |  gamesCollectionName = games
          |}
        """.stripMargin)
      an [Missing] should be thrownBy AppSettings(mongoConfig = incompleteMongoConfig)
    }

    it("should be properly represent in string format") {
      val appSettings: AppSettings = AppSettings()
      appSettings.toString should be(
        """Mongo settings:
          |username = ClientUser
          |password = <hidden>
          |cluster = {chessmultiplayercluster-shard-00-00-j8clo.mongodb.net:27017;chessmultiplayercluster-shard-00-01-j8clo.mongodb.net:27017;chessmultiplayercluster-shard-00-02-j8clo.mongodb.net:27017}
          |authSource = admin
          |databaseName = chess-mp
          |gamesCollectionName = games""".stripMargin)
    }

  }

}
