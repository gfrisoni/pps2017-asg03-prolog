package it.unibo.chess.controller.database

import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.exceptions.Database.DatabaseException
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.{Black, ChessGame, White}
import it.unibo.chess.model.fen.FenParser
import it.unibo.chess.model.modes.ChessModes
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, Matchers}

import scala.concurrent.duration._
import scala.util.Try

@RunWith(classOf[JUnitRunner])
class ChessGameMongoSpecs extends FunSpec with Matchers {

  describe("The representation of a chess game document") {

    val gameClock: ChessClock = ChessClock(
      ChessModes.SuddenDeath,
      Map(White -> 10.minutes, Black -> 10.minutes)
    )

    it("should not be created from a finished game") {
      val finishedGame: ChessGame = FenParser
        .fromFen("6k1/5ppp/8/8/8/8/8/R3K3 w KQkq - 0 1")
        .move('a1, 'a8)
      Try(ChessGameMongo(finishedGame, gameClock)).isSuccess should be(true)
    }

    it("should not be created from an unfinished game") {
      val unfinishedGame: ChessGame = FenParser
        .fromFen("6k1/5ppp/8/8/8/8/8/R3K3 w KQkq - 0 1")
      assertThrows[DatabaseException](ChessGameMongo(unfinishedGame, gameClock))
    }

  }

}