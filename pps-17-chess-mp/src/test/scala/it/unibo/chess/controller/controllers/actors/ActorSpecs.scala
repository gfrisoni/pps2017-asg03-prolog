package it.unibo.chess.controller.controllers.actors

import java.net.InetAddress

import akka.actor.{ActorSystem, _}
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout
import com.github.simplyscala.MongoEmbedDatabase
import it.unibo.chess.controller.controllers.actors.MultiplayerRoles.{Challenger, Creator}
import it.unibo.chess.controller.controllers.actors.messages.ChallengerMessages.{DiscoveryCompleted, LaunchChallenge, UploadAgreement}
import it.unibo.chess.controller.controllers.actors.messages.CommonMessages.{Draw, Move, MoveFeedback, Promote}
import it.unibo.chess.controller.controllers.actors.messages.ControllerMessages.{GetAddressAndPort, SetLaunchChallenge, SetMove, SetUploadAgreement}
import it.unibo.chess.controller.controllers.actors.messages.CreatorMessages.{ChallengeAccepted, UpdateStatus}
import it.unibo.chess.controller.controllers.game.MultiplayerGameSceneControllerImpl
import it.unibo.chess.controller.database.LocalConnectors
import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.model.core.ChessRoles.{Knight, Rook}
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.modes.ChessModes.{ChessMode, SuddenDeath}
import it.unibo.chess.view.dummy.DummyView
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, Matchers, PrivateMethodTester, WordSpecLike}

import scala.concurrent.duration._
import scala.language.{implicitConversions, postfixOps}

@RunWith(classOf[JUnitRunner])
class ActorSpecs extends TestKit(ActorSystem("ActorSpecs")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll with MongoEmbedDatabase with PrivateMethodTester {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val players: Map[ChessColor, ChessPlayer] = Map(White -> ChessPlayer("Matthew"), Black -> ChessPlayer("Luis"))
  val gameMode: ChessMode = SuddenDeath
  val timer: FiniteDuration = 5.minutes
  val clock: ChessClock = ChessClock(gameMode, Map(White -> timer, Black -> timer))
  val controller: MultiplayerGameSceneControllerImpl = new MultiplayerGameSceneControllerImpl(ChessGame(players = players))
  controller.setGameClock(clock)

  import TimerShortcut._

  "A CreatorActor" must {
    val players: Map[ChessColor, ChessPlayer] = Map(White -> ChessPlayer("Matthew"), Black -> ChessPlayer(""))
    val gameMode: ChessMode = SuddenDeath
    val timer: FiniteDuration = 5 minutes
    val controller: MultiplayerGameSceneControllerImpl = new MultiplayerGameSceneControllerImpl(ChessGame(players = players))
    controller.setGameClock( ChessClock(gameMode, Map(White -> timer, Black -> timer)))
    val dbFunctions = LocalConnectors.getCollectionAndFunctions(LocalConnectors.config1)
    val creator = MultiplayerRoles.launchActor(Creator)(controller, dbFunctions._2)

    DummyView.setup(controller, players)
    controller.localInit(creator, White)

    "accept a challenge" in {
      creator ! LaunchChallenge
      val clock: ChessClock = controller.getGameClock.get
      expectMsg(ChallengeAccepted(players(White).name, clock.timerStrategy.chessMode, clock.startingTimers(White)._1))
    }

    "start a game" in {
      creator ! DiscoveryCompleted(players(Black).name)
    }

    "then play with his opponent" in {
      controller.onPieceMove('e2, 'e4)
      expectMsgType[Move]
      creator ! Move('e7, 'e5, controller.time(White), controller.time(Black))
      expectMsg(MoveFeedback(None))
      controller.onPieceMove('f1, 'c4)
      expectMsgType[Move]
      creator ! Move('c7, 'c5, controller.time(White), controller.time(Black))
      expectMsg(MoveFeedback(None))
      creator ! Move('b7, 'b5, controller.time(White), controller.time(Black))
      expectMsg(MoveFeedback(Some("Cannot move a piece when it isn't your turn!")))
      controller.onPieceMove('d1, 'f3)
      expectMsgType[Move]
      creator ! Move('h7, 'h6, controller.time(White), controller.time(Black))
      expectMsg(MoveFeedback(None))
      controller.onPieceMove('f3, 'f7)
      expectMsgType[Move]
    }

    "once the game is over, no more moves can be performed" in {
      creator ! Move('a7, 'a5, controller.time(White), controller.time(Black))
      expectNoMessage(75 millis)
      creator ! SetMove('f3, 'f7)
      expectNoMessage(75 millis)
    }

    "receive upload agreements from the opponent" in {
      creator ! UploadAgreement(true)
      withEmbedMongoFixture(LocalConnectors.config1.port) { _ =>
        creator ! SetUploadAgreement(false)
        within(max = 10000 milliseconds) {
          expectMsgType[UpdateStatus]
        }
      }
    }

  }

  "A ChallengerActor" must {
    val players: Map[ChessColor, ChessPlayer] = Map(White -> ChessPlayer(""), Black -> ChessPlayer("Luis"))
    val gameMode: ChessMode = SuddenDeath
    val timer: FiniteDuration = 5 minutes
    val controller: MultiplayerGameSceneControllerImpl = new MultiplayerGameSceneControllerImpl(ChessGame(players = players))
    controller.setGameClock(ChessClock(gameMode, Map(White -> timer, Black -> timer)))
    val dbFunctions = LocalConnectors.getCollectionAndFunctions(LocalConnectors.config2)
    val challenger = MultiplayerRoles.launchActor(Challenger)(controller, dbFunctions._2)

    DummyView.setup(controller, players)
    controller.localInit(challenger, Black)

    "challenge a Creator" in {
      challenger ! ChallengeAccepted("Creator", gameMode, timer.toMinutes)
      expectMsg(DiscoveryCompleted("Luis"))
    }

    "then play with his opponent" in {
      challenger ! Move('f2, 'f4, controller.time(Black), controller.time(White))
      expectMsg(MoveFeedback(None))
      controller.onPieceMove('a7, 'a5)
      expectMsgType[Move]
      challenger ! Move('f4, 'f5, controller.time(Black), controller.time(White))
      expectMsg(MoveFeedback(None))
      challenger ! Move('a2, 'a4, controller.time(Black), controller.time(White))
      expectMsg(MoveFeedback(Some("Cannot move a piece when it isn't your turn!")))
      controller.onPieceMove('a5, 'a4)
      expectMsgType[Move]
      controller.onPieceMove('b7, 'b5)
      challenger ! Move('f5, 'f6, controller.time(Black), controller.time(White))
      expectMsg(MoveFeedback(None))
      controller.onPieceMove('a4, 'a3)
      expectMsgType[Move]
      challenger ! Move('f6, 'g7, controller.time(Black), controller.time(White))
      expectMsg(MoveFeedback(None))
      controller.onPieceMove('a3, 'b2)
      expectMsgType[Move]
    }

    "also handle promotion" in  {
      challenger ! Move('g7, 'h8, controller.time(Black), controller.time(White))
      expectMsg(MoveFeedback(None))
      challenger ! Promote('h7, Rook)
      expectMsg(MoveFeedback(Some("A Black Pawn can only be promoted on line 1")))
      challenger ! Promote('h8, Rook)
      expectMsg(MoveFeedback(None))
      challenger ! Promote('h8, Rook)
      expectMsg(MoveFeedback(Some("Cannot promote if there isn't any pawn on the allowed row")))
      controller.onPieceMove('b2, 'a1)
      expectMsgType[Move]
      controller.onPromotionSelected('a5, Knight)
      controller.onPromotionSelected('a1, Knight)
      expectMsgType[Promote]
      challenger ! Move('h8, 'g8, controller.time(Black), controller.time(White))
      expectMsg(MoveFeedback(None))
      controller.onPieceMove('a1, 'c2)
      expectMsgType[Move]
    }

    "negotiate for a Draw" in {
      challenger ! Move('d1, 'c2, controller.time(Black), controller.time(White))
      expectMsg(MoveFeedback(None))
      challenger ! Draw(true)
      Thread.sleep(100) // Waits the message to be processed
      controller.onPieceMove('a8, 'a2)
      expectMsgType[Move]
      assert(controller.drawApplicant.isEmpty)
      controller.onDrawApplication(Black, status = true)
      expectMsg(Draw(true))
      assert(controller.drawApplicant.get == Black)
      challenger ! Draw(false)
      challenger ! Move('c2, 'b2, controller.time(Black), controller.time(White))
      expectMsg(MoveFeedback(None))
      controller.onDrawApplication(Black, status = true)
    }

    "accept a Draw" in {
      expectMsg(Draw(true))
      challenger ! Draw(true)
      Thread.sleep(200) // Waits the message to be processed
      assert(controller.gameOver)
    }

  }

  case class TimerShortcut(controller: MultiplayerGameSceneControllerImpl) {
    def time(color: ChessColor): Long = {
      controller.getGameClock.get.getTimerValues(color).toMillis
    }
  }

  private object TimerShortcut {
    implicit def toShortcut(controller: MultiplayerGameSceneControllerImpl): TimerShortcut = TimerShortcut(controller)
  }

}
