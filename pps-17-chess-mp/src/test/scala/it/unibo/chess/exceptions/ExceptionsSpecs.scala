package it.unibo.chess.exceptions

import it.unibo.chess.exceptions.Board.BoardException
import it.unibo.chess.exceptions.Board.Types.OutOfBoard
import it.unibo.chess.exceptions.Game.GameException
import it.unibo.chess.exceptions.Game.Types.WrongTurn
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ExceptionsSpecs extends FlatSpec {

  "A Custom Exception" should "pass its parameters through all the process once thrown" in {
    val thrown = intercept[BoardException] {
      throw BoardException("Error message", OutOfBoard)
    }
    assert(thrown.exceptionType == OutOfBoard)
    assert(thrown.message == "Error message")

    val thrown2 = intercept[GameException] {
      throw GameException("Error message!!!", WrongTurn)
    }
    assert(thrown2.exceptionType == WrongTurn)
    assert(thrown2.message == "Error message!!!")
  }

}
