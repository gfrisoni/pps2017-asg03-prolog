package it.unibo.chess

import com.github.nscala_time.time.Imports.DateTime
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core._
import it.unibo.chess.model.fen.FenParser

/**
  * Handles advanced game states, handy for testing purposes.
  */
object GameExamples {

  /**
    * Simulator URL.
    * @see [[https://lichess.org/editor/rnbq1bnr/pppk1ppp/8/3pp3/2B1P3/5N2/PPPP1PPP/RNBQK2R_w_KQkq_-]]
    */
  val advancedGameState: ChessGame = FenParser.fromFen("rnbq1bnr/pppk1ppp/8/3pp3/2B1P3/5N2/PPPP1PPP/RNBQK2R w KQ - 2 4")

  /**
    * Fool's mate.
    * Is the checkmate in the fewest possible number of moves from the start of the game.
    * @see [[https://en.wikipedia.org/wiki/Fool%27s_mate]]
    */
  val foolMate: ChessGame = ChessGame(
    players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")),
    dateTime = (new DateTime).withYear(2018).withMonthOfYear(9).withDayOfMonth(15)
  ).move('f2, 'f3).move('e7, 'e5).move('g2, 'g4).move('d8, 'h4)

}
