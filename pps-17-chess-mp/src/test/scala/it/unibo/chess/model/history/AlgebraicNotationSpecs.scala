package it.unibo.chess.model.history

import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.model.core.KingStates.{Check, Checkmate}
import it.unibo.chess.model.history.ChessActions._
import it.unibo.chess.model.core._
import it.unibo.chess.model.history.notation._
import it.unibo.chess.model.utilities.ChessUtilities.AnUtilities._
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class AlgebraicNotationSpecs extends FlatSpec {

  val standardSerializer: ChessGameActionSerializer = AlgebraicNotationGameActionSerializer(STANDARD_AN_RULES)
  val longSerializer: ChessGameActionSerializer = AlgebraicNotationGameActionSerializer(STANDARD_AN_RULES.copy(longNotation = true))

  "An Algebraic Notation" should "be able to calculate all the possible rules combination" in {
    assert(AlgebraicNotation.allPossibleRules.size == 1728)
  }

  it should "be able to calculate the correct AN for a piece movement action" in {
    val pawnGameAction: MoveAction = MoveAction((ChessPiece(White, Pawn(White)), 'a2), AmbiguityTypes.None, 'a3)
    val knightGameAction: MoveAction = MoveAction((ChessPiece(Black, Knight), 'g8), AmbiguityTypes.None, 'f6)
    assert(standardSerializer.serializeGameActions(pawnGameAction, knightGameAction) == Seq("a3", "Nf6"))
    assert(longSerializer.serializeGameActions(pawnGameAction, knightGameAction) == Seq("a2-a3", "Ng8-f6"))
  }

  it should "be able to calculate the correct AN for a capture action" in {
    val pawnGameAction: CaptureAction = CaptureAction((ChessPiece(White, Pawn(White)), 'e4), AmbiguityTypes.None, 'd5, ChessPiece(Black, Pawn(Black)))
    val bishopGameAction: CaptureAction = CaptureAction((ChessPiece(White, Bishop), 'c4), AmbiguityTypes.None, 'd5, ChessPiece(Black, Pawn(Black)))
    assert(standardSerializer.serializeGameActions(pawnGameAction, bishopGameAction) == Seq("exd5", "Bxd5"))
    assert(longSerializer.serializeGameActions(pawnGameAction, bishopGameAction) == Seq("e4xd5","Bc4xd5"))
  }

  it should "be able to calculate the correct AN for a capture and promotion action" in {
    val promotionGameAction: CapturePromoteAction = CapturePromoteAction(
      (ChessPiece(White, Pawn(White)), 'b7),
      AmbiguityTypes.None,
      'c8,
      ChessPiece(Black, Bishop),
      Queen
    )
    assert(standardSerializer.serializeGameAction(promotionGameAction) == "bxc8=Q")
    assert(longSerializer.serializeGameAction(promotionGameAction) == "b7xc8=Q")
  }

  it should "be able to calculate the correct AN for an en passant action" in {
    val enPassantGameAction: EnPassantAction = EnPassantAction(
      (ChessPiece(White, Pawn(White)), 'd5),
      AmbiguityTypes.None,
      'e6,
      ChessPiece(Black, Pawn(Black))
    )
    assert(standardSerializer.serializeGameAction(enPassantGameAction) == "dxe6e.p.")
    assert(longSerializer.serializeGameAction(enPassantGameAction) == "d5xe6e.p.")
  }

  it should "be able to calculate the correct AN for a promotion action" in {
    val promotionGameAction: PromoteAction = PromoteAction((ChessPiece(White, Pawn(White)), 'b7), AmbiguityTypes.None, 'b8, Knight)
    assert(standardSerializer.serializeGameAction(promotionGameAction) == "b8=N")
    assert(longSerializer.serializeGameAction(promotionGameAction) == "b7-b8=N")
  }

  it should "be able to calculate the correct AN for a castling action" in {
    val shortCastlingGameAction: CastlingAction = CastlingAction(isKingSide = true)
    val longCastlingGameAction: CastlingAction = CastlingAction(isKingSide = false)
    assert(standardSerializer.serializeGameActions(shortCastlingGameAction, longCastlingGameAction) == Seq("0-0", "0-0-0"))
  }

  it should "be able to calculate the correct AN for a draw game action" in {
    val drawGameAction: DrawAction = DrawAction(DrawReasons.MutualDraw)
    assert(standardSerializer.serializeGameAction(drawGameAction) == "½-½")
  }

  it should "be able to calculate the correct AN for a lose game action" in {
    val loseWhiteGameAction: LoseAction = LoseAction(loserColor = White, LoseReasons.CheckmateLoss)
    val loseBlackGameAction: LoseAction = LoseAction(loserColor = Black, LoseReasons.CheckmateLoss)
    assert(standardSerializer.serializeGameActions(loseWhiteGameAction, loseBlackGameAction) == Seq("0-1", "1-0"))
  }

  it should "be able to calculate the correct AN with a specific file case preference" in {
    val pawnGameAction: MoveAction = MoveAction((ChessPiece(White, Pawn(White)), 'a2), AmbiguityTypes.None, 'a3)
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        longNotation = true,
        lowerCaseFiles = true
      )
    ).serializeGameAction(pawnGameAction) == "a2-a3")
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        longNotation = true,
        lowerCaseFiles = false
      )
    ).serializeGameAction(pawnGameAction) == "A2-A3")
  }

  it should "be able to calculate the correct AN with a specific piece representation" in {
    val kingGameAction: MoveAction = MoveAction((ChessPiece(White, King(White)), 'e1), AmbiguityTypes.None, 'e2)
    val queenGameAction: CaptureAction = CaptureAction((ChessPiece(White, Queen), 'd1), AmbiguityTypes.None, 'd6, ChessPiece(Black, Pawn(Black)))
    val rookGameAction: CaptureAction = CaptureAction((ChessPiece(White, Rook), 'a3), AmbiguityTypes.None, 'a7, ChessPiece(Black, Bishop))
    val bishopGameAction: MoveAction = MoveAction((ChessPiece(White, Bishop), 'b3), AmbiguityTypes.None, 'd5)
    val knightGameAction: MoveAction = MoveAction((ChessPiece(Black, Knight), 'g8), AmbiguityTypes.None, 'f6)
    val pawnGameAction: MoveAction = MoveAction((ChessPiece(White, Pawn(White)), 'c2), AmbiguityTypes.None, 'c4)
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        figurine = false
      )
    ).serializeGameActions(
      kingGameAction,
      queenGameAction,
      rookGameAction,
      bishopGameAction,
      knightGameAction,
      pawnGameAction) == Seq("Ke2", "Qxd6", "Rxa7", "Bd5", "Nf6", "c4"))
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        figurine = true
      )
    ).serializeGameActions(
      kingGameAction,
      queenGameAction,
      rookGameAction,
      bishopGameAction,
      knightGameAction,
      pawnGameAction) == Seq("♔e2", "♕xd6", "♖xa7", "♗d5", "♘f6", "c4"))
  }

  it should "be able to calculate the correct AN with a specific type of capture notation" in {
    val pawnGameAction: CaptureAction = CaptureAction((ChessPiece(White, Pawn(White)), 'e4), AmbiguityTypes.None, 'd5, ChessPiece(Black, Pawn(Black)))
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        captureNotation = CaptureNotations.XCaptureNotation
      )
    ).serializeGameAction(pawnGameAction) == "exd5")
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        captureNotation = CaptureNotations.ColonCaptureNotation
      )
    ).serializeGameAction(pawnGameAction) == "e:d5")
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        captureNotation = CaptureNotations.OmittedCaptureNotation
      )
    ).serializeGameAction(pawnGameAction) == "ed5")
  }

  it should "be able to calculate the correct AN with a specific type of castling notation" in {
    val shortCastlingGameAction: CastlingAction = CastlingAction(isKingSide = true)
    val longCastlingGameAction: CastlingAction = CastlingAction(isKingSide = false)
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        castlingNotation = CastlingNotations.ZeroesCastlingNotation
      )
    ).serializeGameActions(shortCastlingGameAction, longCastlingGameAction) == Seq("0-0", "0-0-0"))
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        castlingNotation = CastlingNotations.OsCastlingNotation
      )
    ).serializeGameActions(shortCastlingGameAction, longCastlingGameAction) == Seq("O-O", "O-O-O"))
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        castlingNotation = CastlingNotations.WordCastlingNotation
      )
    ).serializeGameAction(longCastlingGameAction) == "castles")
  }

  it should "be able to calculate the correct AN with a specific type of promotion notation" in {
    val promotionGameAction: PromoteAction = PromoteAction((ChessPiece(White, Pawn(White)), 'b7), AmbiguityTypes.None, 'b8, Queen)
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        promotionNotation = PromotionNotations.EqualsPromoteNotation
      )
    ).serializeGameAction(promotionGameAction) == "b8=Q")
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        promotionNotation = PromotionNotations.ParenthesisPromoteNotation
      )
    ).serializeGameAction(promotionGameAction) == "b8(Q)")
  }

  it should "be able to calculate the correct AN with a specific type of check notation" in {
    val checkGameAction: MoveAction = MoveAction((ChessPiece(White, Bishop), 'f1), AmbiguityTypes.None, 'b5, kingState = Check)
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        checkNotation = CheckNotations.PlusCheckNotation
      )
    ).serializeGameAction(checkGameAction) == "Bb5+")
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        checkNotation = CheckNotations.DaggerCheckNotation
      )
    ).serializeGameAction(checkGameAction) == "Bb5†")
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        checkNotation = CheckNotations.WordCheckNotation
      )
    ).serializeGameAction(checkGameAction) == "Bb5 ch")
  }

  it should "be able to calculate the correct AN with a specific type of checkmate notation" in {
    val checkmateGameAction: MoveAction = MoveAction((ChessPiece(White, Bishop), 'f1), AmbiguityTypes.None, 'b5, kingState = Checkmate)
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        checkmateNotation = CheckmateNotations.HashtagCheckmateNotation
      )
    ).serializeGameAction(checkmateGameAction) == "Bb5#")
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        checkmateNotation = CheckmateNotations.DoublePlusCheckmateNotation
      )
    ).serializeGameAction(checkmateGameAction) == "Bb5++")
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        checkmateNotation = CheckmateNotations.DoubleDaggerCheckmateNotation
      )
    ).serializeGameAction(checkmateGameAction) == "Bb5‡")
    assert(AlgebraicNotationGameActionSerializer(
      STANDARD_AN_RULES.copy(
        checkmateNotation = CheckmateNotations.WordCheckmateNotation
      )
    ).serializeGameAction(checkmateGameAction) == "Bb5 mate")
  }

  it should "be able to calculate the correct AN for ambiguous moves" in {
    val moveAction: MoveAction = MoveAction((ChessPiece(White, Knight), 'g2), AmbiguityTypes.CompleteAmbiguity, 'f4)
    val captureAction: CaptureAction = CaptureAction(
      (ChessPiece(White, Knight), 'g2),
      AmbiguityTypes.SameFileAmbiguity,
      'f4,
      ChessPiece(Black, Pawn(Black))
    )
    val promotionAction: CapturePromoteAction = CapturePromoteAction(
      (ChessPiece(White, Pawn(White)), 'b7),
      AmbiguityTypes.SameRankAmbiguity,
      'c8,
      ChessPiece(Black, Bishop),
      Queen
    )
    val enPassantAction: EnPassantAction = EnPassantAction(
      (ChessPiece(White, Pawn(White)), 'd5),
      AmbiguityTypes.SameRankAmbiguity,
      'e6,
      ChessPiece(Black, Pawn(Black))
    )
    assert(AlgebraicNotationGameActionSerializer(STANDARD_AN_RULES).serializeGameAction(moveAction) == "Ng2f4")
    assert(AlgebraicNotationGameActionSerializer(STANDARD_AN_RULES).serializeGameAction(captureAction) == "N2xf4")
    assert(AlgebraicNotationGameActionSerializer(STANDARD_AN_RULES).serializeGameAction(promotionAction) == "bxc8=Q")
    assert(AlgebraicNotationGameActionSerializer(STANDARD_AN_RULES).serializeGameAction(enPassantAction) == "dxe6e.p.")
  }

}
