package it.unibo.chess.model.logic

import it.unibo.chess.exceptions.Role.InvalidRoleAssigned
import it.unibo.chess.model.core.ChessRoles.Pawn
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.logic.PieceQueries._
import it.unibo.chess.model.core._
import it.unibo.chess.GameExamples._
import it.unibo.chess.model.core.position.ChessBoardPosition
import org.junit.runner.RunWith
import org.scalatest.FunSpec
import org.scalatest.junit.JUnitRunner

import scala.language.postfixOps

@RunWith(classOf[JUnitRunner])
class LegalMovesSpecs extends FunSpec {

  val gameState: ChessGame = advancedGameState

  describe("Every piece/color combination excluding pawns") {

    val notPawnPieces: Iterable[ChessPiece] = for (color <- List(White, Black);
                                                   role <- ChessRoles.roles
                                                   if !role.isInstanceOf[Pawn]) yield ChessPiece(color, role)

    val genericPieceQueries: Iterable[GenericPieceLimitQuery] = for (piece <- notPawnPieces) yield GenericPieceLimitQuery(piece)

    it("should stop before something else of the same color") {
      genericPieceQueries.foreach(q => q.piece.color match {
        case White => assert(q.applyQuery(gameState, 'c4) == Stop)
        case Black =>
          assert(q.applyQuery(gameState, 'e5) == Stop)
          assert(q.applyQuery(gameState, 'd5) == Stop)
      })
    }

    it("should include and stop before something else of the opposite color") {
      genericPieceQueries.foreach(q => q.piece.color match {
        case White =>
          assert(q.applyQuery(gameState, 'e5) == IncludeAndStop)
          assert(q.applyQuery(gameState, 'd5) == IncludeAndStop)
        case Black =>
          assert(q.applyQuery(gameState, 'c4) == IncludeAndStop)
      })
    }

    it("should be able to move on free position") {
      genericPieceQueries.foreach(q => {
        assert(q.applyQuery(gameState, 'b4) == Continue)
        assert(q.applyQuery(gameState, 'h4) == Continue)
        assert(q.applyQuery(gameState, 'e2) == Continue)
      })
    }

    it("should throw exception if it uses pawn logic") {
      notPawnPieces.foreach(p => {
        assertThrows[InvalidRoleAssigned](PawnDiagonalLimitQuery(p))
        assertThrows[InvalidRoleAssigned](PawnForwardLimitQuery(p))
      })
    }

  }

  describe("A pawn") {

    it("should stop if there is another piece in front of it") {
      val blockedWhitePawnQuery: ChessPieceQuery = PawnForwardLimitQuery(gameState.board pieceAt 'e4 get)
      assert(blockedWhitePawnQuery.applyQuery(gameState, 'e5) == Stop)

      val blockedBlackPawnQuery: ChessPieceQuery = PawnForwardLimitQuery(gameState.board pieceAt 'e5 get)
      assert(blockedBlackPawnQuery.applyQuery(gameState, 'e4) == Stop)
    }

    it("should continue if there isn't another piece in front of it") {
      val freeWhitePawnQuery: ChessPieceQuery = PawnForwardLimitQuery(gameState.board pieceAt 'a2 get)
      assert(freeWhitePawnQuery.applyQuery(gameState, 'a3) == Continue)

      val freeBlackPawnQuery: ChessPieceQuery = PawnForwardLimitQuery(gameState.board pieceAt 'a7 get)
      assert(freeBlackPawnQuery.applyQuery(gameState, 'a6) == Continue)
    }

    it("should include and stop if there is another piece of the opposite color in diagonal of it") {
      val includeAndStopPawnQueries: ChessPieceQuery = PawnDiagonalLimitQuery(gameState.board pieceAt 'd5 get)
      assert(includeAndStopPawnQueries.applyQuery(gameState, 'c4) == IncludeAndStop)
    }

    it("should stop if there is another piece of the same color in diagonal of it") {
      val includeAndStopPawnQuery: ChessPieceQuery = PawnDiagonalLimitQuery(gameState.board pieceAt 'g2 get)
      assert(includeAndStopPawnQuery.applyQuery(gameState, 'f3) == Stop)
    }

    it("should throw exception if it doesn't use pawn logic") {
      assertThrows[InvalidRoleAssigned](GenericPieceLimitQuery(gameState.board pieceAt 'a2 get))
    }

    it("shouldn't perform an enpassant if the opposite pawn didn't move the immediate previous turn") {
      val enPassantGame = gameState.move('e4, 'd5).move('c7, 'c5)
      assert(enPassantGame.movesFrom('d5).contains('c6))
      val expiredEnPassantGame = enPassantGame.move('a2, 'a3).move('a7,'a6)
      assert(!expiredEnPassantGame.movesFrom('d5).contains('c6))
    }

  }

  describe("A King") {

    val allowedCastlingGameState: ChessGame = gameState.move('c4, 'b3).move('b7, 'b6)
    it("should not be allowed to perform a castling if under check during the path") {
      assert(allowedCastlingGameState.movesFrom('e1) == ChessBoardPosition.getPositionsFromSymbols(Set('f1, 'g1, 'e2)))

      val blockedCastlingGameState = allowedCastlingGameState.move('a2, 'a3).move('c8, 'a6)
      assert(blockedCastlingGameState.movesFrom('e1) == Set.empty)

      val coveredCastlingGameState = blockedCastlingGameState.move('d1, 'e2).move('d8, 'h4)
      assert(coveredCastlingGameState.movesFrom('e1) == ChessBoardPosition.getPositionsFromSymbols(Set('d1, 'f1, 'g1)))
    }

    it("shouldn't have castling as a move option if a rook or itself were previously moved") {
      assert(allowedCastlingGameState.movesFrom('e1).exists(White.castling.kingDestinations.map(_.head) contains))
      val dirtyCastlingGameState = allowedCastlingGameState
        .move('h1,'g1).move('a7, 'a5)
        .move('g1, 'h1).move('a5,'a4)
      assert(!dirtyCastlingGameState.movesFrom('e1).forall(White.castling.kingDestinations.map(_.head) contains))
    }
  }

}
