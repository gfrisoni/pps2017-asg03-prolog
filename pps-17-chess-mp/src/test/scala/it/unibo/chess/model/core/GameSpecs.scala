package it.unibo.chess.model.core

import it.unibo.chess.exceptions.Board.BoardException
import it.unibo.chess.exceptions.Game.GameException
import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.GameExamples.advancedGameState
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.KingStates._
import it.unibo.chess.model.core.LoseReasons.DisconnectLoss
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.fen.FenParser
import it.unibo.chess.model.history.ChessActions.{ChessPromotionAction, LoseAction}
import it.unibo.chess.model.logic.SideEffects.ChessPieceSideEffect
import org.junit.runner.RunWith
import org.scalatest.{FunSpec, PrivateMethodTester}
import org.scalatest.junit.JUnitRunner

import scala.language.reflectiveCalls

@RunWith(classOf[JUnitRunner])
class GameSpecs extends FunSpec with PrivateMethodTester {

  describe("A game without player's map") {
    it("should throw an exception") {
      assertThrows[GameException](ChessGame())
    }
  }

  describe("A game without a Black and a White player") {
    it("should throw an exception") {
      assertThrows[GameException](ChessGame(players = Map(White -> ChessPlayer("player1"))))
      assertThrows[GameException](ChessGame(players = Map(White -> ChessPlayer("player1"))))
    }
  }

  describe("A game") {

    it("should be created with default settings") {
      val game = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))
      assert(game.board == ChessBoard().reset())
      assert(game.currentTurn == White)
    }

    it("should allow a move to a piece updating correctly the flags") {
      val game = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))
      assert((game.board pieceAt 'a2).isDefined)
      assert((game.board pieceAt 'a3).isEmpty)
      val nextGame = game move('a2, 'a3)
      assert((nextGame.board pieceAt 'a2).isEmpty)
      assert((nextGame.board pieceAt 'a3).isDefined)
      assert(nextGame.flags.halfMove == 0)
      assert(nextGame.flags.fullMove == 1)
      val otherGame = nextGame move('b7, 'b5)
      assert((otherGame.board pieceAt 'b7).isEmpty)
      assert((otherGame.board pieceAt 'b5).isDefined)
      assert(otherGame.flags.enPassantTarget.contains(ChessBoardPosition('b6)))
    }

    it("should throw an exception if the turn is not respected") {
      val game = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))
      assert((game.board pieceAt 'b7).isDefined)
      assert((game.board pieceAt 'c4).isEmpty)
      assertThrows[GameException](game move('b7, 'c4))
    }

    it("should allow taking a piece") {
      val whitePawn = advancedGameState.board pieceAt 'e4
      val nextGame = advancedGameState move('e4, 'd5)
      assert((nextGame.board pieceAt 'e4).isEmpty)
      assert(whitePawn == (nextGame.board pieceAt 'd5))
    }

    it("should calculate the allowed moves according to the board status") {
      assert((advancedGameState movesFrom 'f3) == ChessBoardPosition.getPositionsFromSymbols(Set('e5, 'g5, 'h4, 'g1, 'd4)))
    }

    it("should throw an exception if the selected position is occupied by a piece owned by the opposite player") {
      assertThrows[GameException](advancedGameState move('d7, 'd6))
    }

    it("should throw an exception if there isn't any piece on the selected position") {
      assertThrows[BoardException](advancedGameState movesFrom 'd6)
    }

    it("should recognize an enpassant movement and apply its effect") {
      val enPassantGameState = advancedGameState
        .move('b2, 'b4).move('g7, 'g6)
        .move('b4, 'b5).move('c7, 'c5)
      enPassantGameState movesFrom 'b5
      val nextState = enPassantGameState.move('b5, 'c6)
      assert((nextState.board pieceAt 'c6).get == ChessPiece(White, Pawn(White)))
      assert((nextState.board pieceAt 'c5).isEmpty)
      assert(nextState.board.taken.contains(ChessPiece(Black, Pawn(Black))))
    }

    it("should recognize a castling movement and apply its effect") {
      advancedGameState movesFrom 'e1
      val nextState = advancedGameState move('e1, 'g1)
      assert((nextState.board pieceAt 'e1).isEmpty)
      assert((nextState.board pieceAt 'h1).isEmpty)
      assert((nextState.board pieceAt 'f1).get == ChessPiece(White, Rook))
      assert((nextState.board pieceAt 'g1).get == ChessPiece(White, King(White)))
    }

    it("should cache the previously calculated moves") {
      val map = PrivateMethod[Map[BoardPosition, List[(BoardPosition, Option[ChessPieceSideEffect])]]]('legalMovesMap)
      val pos = 'a1
      assert(!(advancedGameState invokePrivate map()).contains(pos))
      advancedGameState.movesFrom(pos)
      advancedGameState.movesFrom('a2)
      assert((advancedGameState invokePrivate map()).contains(pos))
    }

    it("should detect a Check situation") {
      val checkGameState1 = advancedGameState move('c4, 'b5)
      assert(checkGameState1.kingStateOf(Black) == Check)
      assert(checkGameState1.kingStateOf(White) == Free)

      val checkGameState2 = checkGameState1 move('d7, 'e6)
      assert(checkGameState2.kingStateOf(Black) == Free)

      val checkGameState3 = checkGameState2 move('f3, 'g5)
      assert(checkGameState3.kingStateOf(Black) == Check)
    }

    it("should detect a Checkmate situation from a fresh game") {
      val newGame: ChessGame = ChessGame(ChessBoard().reset(), players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))
      val scholarMateGame = newGame.move('e2, 'e4).move('e7, 'e5)
        .move('f1, 'c4).move('c7, 'c6)
        .move('d1, 'f3).move('a7,'a6)
        .move('f3, 'f7)
      assert(scholarMateGame.kingStateOf(Black) == Checkmate)
      assert(scholarMateGame.kingStateOf(White) == Free)
    }

    it("should detect a Checkmate situation on a loaded game") {
      for (color <- List(White, Black)) {
        val checkmateBoard = ChessBoard(Map(ChessBoardPosition('a1) -> ChessPiece(color, King(color)), ChessBoardPosition('g8) -> ChessPiece(!color, King(!color)),
          ChessBoardPosition('c6) -> ChessPiece(!color, Rook), ChessBoardPosition('b5) -> ChessPiece(!color, Rook), ChessBoardPosition('f5) -> ChessPiece(!color, Bishop),
          ChessBoardPosition('h8) -> ChessPiece(color, Knight)), Nil)
        val checkmateGame = ChessGame(checkmateBoard, players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")), !color).move('c6, 'a6)
        assert(checkmateGame.movesFrom('a1) == Set.empty)
        assertThrows[GameException](checkmateGame.move('a1, 'b1))
        assert(checkmateGame.kingStateOf(color) == Checkmate)
      }
    }

    it("should detect a Stalemate situation") {
      for (color <- List(White, Black)) {
        val checkmateBoard = ChessBoard(Map(ChessBoardPosition('a1) -> ChessPiece(color, King(color)), ChessBoardPosition('g8) -> ChessPiece(!color, King(!color)),
          ChessBoardPosition('d2) -> ChessPiece(!color, Rook), ChessBoardPosition('f5) -> ChessPiece(!color, Bishop)), Nil)
        val checkmateGame = ChessGame(checkmateBoard, players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")), color)
        assert(checkmateGame.movesFrom('a1) == Set.empty)
        assertThrows[GameException](checkmateGame.move('a1, 'a2))
        assert(checkmateGame.kingStateOf(color) == Stalemate)
      }
    }

    it("should prune any piece movement that would cause a check on the current player's king") {
      val checkGameState1 = advancedGameState move('c4, 'b5)
      assert((checkGameState1 movesFrom 'g7) == Set.empty)
      assert((checkGameState1 movesFrom 'b8) == Set(ChessBoardPosition('c6)))
      assert((checkGameState1 movesFrom 'c7) == Set(ChessBoardPosition('c6)))
      assert((checkGameState1 movesFrom 'd7) == ChessBoardPosition.getPositionsFromSymbols(Set('d6, 'e6, 'e7)))

      val blockedGameState: ChessGame = advancedGameState move('c4, 'b5) move('c7, 'c6) move('f3, 'g5)
      assert((blockedGameState movesFrom 'c6) == Set(ChessBoardPosition('b5)))
    }

    describe("in a promotion game"){
      val almostPromotionBoard = ChessBoard(Map(
        ChessBoardPosition('e2) -> ChessPiece(Black, Pawn(Black)),
        ChessBoardPosition('a1) -> ChessPiece(Black, King(Black)),
        ChessBoardPosition('h1) -> ChessPiece(White, King(White))))
      val almostPromotionGame = ChessGame(almostPromotionBoard, advancedGameState.players, Black)

      it("should allow the Promotion of a Pawn, control the flow, then swap the turn") {
        assert(almostPromotionGame.currentTurn == Black)
      }

      val promotionGame = almostPromotionGame.move('e2, 'e1)
      it("should block any current turn's moves") {
        assert(promotionGame.currentTurn == Black)
        assertThrows[GameException](promotionGame.move('h1,'h2))
      }

      it("should also block the other player's moves") {
        assertThrows[GameException](promotionGame.move('a1,'a2))
      }

      it("should unlock the game once the promotion action has been performed") {
        val promotedGame = promotionGame.promote('e1, Rook)
        assert(promotedGame.board == (promotionGame.board promote 'e1 to Rook))
        assert(promotedGame.currentTurn == White)
      }

      it("should mark the next gameStep as check if applicable") {
        FenParser.fromFen("rn2kbnr/ppP1pppp/1q2b3/8/8/8/PPPP1PPP/RNBQKBNR w KQkq - 0 1")
          .move('c7, 'b8).promote('b8, Queen).history.gameSteps.last.action match {
          case p: ChessPromotionAction => assert(p.kingState == Check)
          case _ => fail()
        }
      }

      it("should end the game if the lose method is called") {
        assert(FenParser.fromFen("rn2kbnr/ppP1pppp/1q2b3/8/8/8/PPPP1PPP/RNBQKBNR w KQkq - 0 1").lose(White, DisconnectLoss).history.gameSteps.last.action == (LoseAction(White, DisconnectLoss)))
      }

    }

  }

}