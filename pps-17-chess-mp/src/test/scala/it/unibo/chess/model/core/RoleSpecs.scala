package it.unibo.chess.model.core

import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.position.ChessBoardPosition.getPositionsFromSymbols
import it.unibo.chess.model.core.ChessRoles._
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class RoleSpecs extends FlatSpec {

  "a rook" should "be able to move to any ChessBoardPosition along the same row or col" in {
    val rook: ChessRole = Rook
    assert((rook directionsFrom 'e3) == List(
      getPositionsFromSymbols(List('e4, 'e5, 'e6, 'e7, 'e8)),
      getPositionsFromSymbols(List('f3, 'g3, 'h3)),
      getPositionsFromSymbols(List('e2, 'e1)),
      getPositionsFromSymbols(List('d3, 'c3, 'b3, 'a3))
    ))
  }

  "a queen" should "be able to move to any ChessBoardPosition along the same row or col or diagonal" in {
    val queen: ChessRole = Queen
    assert((queen directionsFrom 'e3) == List(
      getPositionsFromSymbols(List('e4, 'e5, 'e6, 'e7, 'e8)),
      getPositionsFromSymbols(List('f3, 'g3, 'h3)),
      getPositionsFromSymbols(List('e2, 'e1)),
      getPositionsFromSymbols(List('d3, 'c3, 'b3, 'a3)),
      getPositionsFromSymbols(List('d4, 'c5, 'b6, 'a7)),
      getPositionsFromSymbols(List('f4, 'g5, 'h6)),
      getPositionsFromSymbols(List('d2, 'c1)),
      getPositionsFromSymbols(List('f2, 'g1))
    ))
  }

  "a king" should "be able to move to any ChessBoardPosition around it" in {
    val king: ChessRole = King(White)
    assert((king directionsFrom 'e3) == List(
      getPositionsFromSymbols(List('d4)),
      getPositionsFromSymbols(List('e4)),
      getPositionsFromSymbols(List('f4)),
      getPositionsFromSymbols(List('d3)),
      getPositionsFromSymbols(List('f3)),
      getPositionsFromSymbols(List('d2)),
      getPositionsFromSymbols(List('e2)),
      getPositionsFromSymbols(List('f2))
    ))
  }

  "a pawn in the starting ChessBoardPosition" should "move forward of 1 or 2 steps" in {
    val pawn: ChessRole = Pawn(White)
    assert((pawn directionsFrom 'd2) == List(
      getPositionsFromSymbols(List('d3, 'd4)),
      getPositionsFromSymbols(List('c3)),
      getPositionsFromSymbols(List('e3))
    ))
  }

  "a white pawn" should "move upwards" in {
    val pawn: ChessRole = Pawn(White)
    assert((pawn directionsFrom 'b6) == List(
      getPositionsFromSymbols(List('b7)),
      getPositionsFromSymbols(List('a7)),
      getPositionsFromSymbols(List('c7))))
  }

  "a black pawn" should "move downwards" in {
    val pawn: ChessRole = Pawn(Black)
    assert((pawn directionsFrom 'b6) == List(
      getPositionsFromSymbols(List('b5)),
      getPositionsFromSymbols(List('a5)),
      getPositionsFromSymbols(List('c5))
    ))
  }

  "a bishop" should "be able to move in diagonal" in {
    val bishop: ChessRole = Bishop
    assert((bishop directionsFrom 'e3) == List(
      getPositionsFromSymbols(List('d4, 'c5, 'b6, 'a7)),
      getPositionsFromSymbols(List('f4, 'g5, 'h6)),
      getPositionsFromSymbols(List('d2, 'c1)),
      getPositionsFromSymbols(List('f2, 'g1))
    ))
  }

  "a knight" should "be able to move as an L" in {
    val knight: ChessRole = Knight
    assert((knight directionsFrom 'e1) == List(
      getPositionsFromSymbols(List('f3)),
      getPositionsFromSymbols(List('g2)),
      getPositionsFromSymbols(List('d3)),
      getPositionsFromSymbols(List('c2))
    ))
  }

}
