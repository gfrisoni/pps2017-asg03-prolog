package it.unibo.chess.model.pgn

import org.junit.runner.RunWith
import org.scalatest.{FunSpec, Matchers}
import org.scalatest.junit.JUnitRunner

import scalaz.{NonEmptyList, Success}

@RunWith(classOf[JUnitRunner])
class PgnTagsParserSpecs extends FunSpec with Matchers {

  describe("A PGN tag parser") {

    describe("should be able to convert successfully") {

      it("a correct string with more PGN tags into their objects representation") {
        val pgnTags: String =
          """[Event "Casual Game"]
            |[Site "Chess Multiplayer"]
            |[Date "2018.09.10"]
            |[White "Giacomo Frisoni"]
            |[Black "Leonardo Montini"]
            |[Result "1-0"]
            |[Termination "Giacomo Frisoni won by resignation"]
          """.stripMargin
        PgnTagsParser(pgnTags) should be(scalaz.Success(PgnTags(
          Seq(
            PgnTag(PgnTagTypes.Event, "Casual Game"),
            PgnTag(PgnTagTypes.Site, "Chess Multiplayer"),
            PgnTag(PgnTagTypes.Date, "2018.09.10"),
            PgnTag(PgnTagTypes.White, "Giacomo Frisoni"),
            PgnTag(PgnTagTypes.Black, "Leonardo Montini"),
            PgnTag(PgnTagTypes.Result, "1-0"),
            PgnTag(PgnTagTypes.Termination, "Giacomo Frisoni won by resignation")
          ).collect { case Success(tag) => tag }
        )))
      }

      it("a correct string with only one PGN tag into its object representation") {
        val pgnTag: String = """[Event "Casual Game"]"""
        PgnTagsParser(pgnTag) should be(scalaz.Success(PgnTags(
          Seq(
            PgnTag(PgnTagTypes.Event, "Casual Game")
          ).collect { case Success(tag) => tag }
        )))
      }

    }

    describe("should fail if the string with PGN tags contains errors") {

      it("related to invalid quotes on a line") {
        val pgnTagsQuotesOmitted: String = "[Event Casual Game]"
        val pgnTagsLeftQuotesOmitted: String = """[Event "Casual Game]"""
        val pgnTagsRightQuotesOmitted: String = """[Event Casual Game"]"""
        PgnTagsParser(pgnTagsQuotesOmitted) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: '"' expected but 'C' found""".stripMargin)))
        PgnTagsParser(pgnTagsLeftQuotesOmitted) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: string matching regex '"](|\r|\n|\r\n)' expected but ']' found""".stripMargin)))
        PgnTagsParser(pgnTagsRightQuotesOmitted) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: '"' expected but 'C' found""".stripMargin)))
      }

      it("related to invalid brackets on a line") {
        val pgnTagsBracketsOmitted: String = """Event "Casual Game""""
        val pgnTagsLeftBracketOmitted: String = """Event "Casual Game"]"""
        val pgnTagsRightBracketOmitted: String = """[Event "Casual Game""""
        PgnTagsParser(pgnTagsBracketsOmitted) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: '[' expected but 'E' found""".stripMargin)))
        PgnTagsParser(pgnTagsLeftBracketOmitted) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: '[' expected but 'E' found""".stripMargin)))
        PgnTagsParser(pgnTagsRightBracketOmitted) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: string matching regex '"](|\r|\n|\r\n)' expected but '"' found""".stripMargin)))
      }

      it("related to invalid quotes and brackets on a line") {
        val pgnTagsQuotesBracketsOmitted: String = "Event Casual Game"
        PgnTagsParser(pgnTagsQuotesBracketsOmitted) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: '[' expected but 'E' found""".stripMargin)))
      }

      it("related to invalid quotes and carriage returns") {
        val pgnTagsQuotesOpenedWithoutValue: String =
          """[Event Causal Game"]
            |[Site "ChessMultiplayer"]
          """.stripMargin
        val pgnTagsQuotesOpenedButNotClosed: String =
          """[Event "Causal Game]
            |[Site "ChessMultiplayer"]
          """.stripMargin
        PgnTagsParser(pgnTagsQuotesOpenedWithoutValue) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: '"' expected but 'C' found""".stripMargin)))
        PgnTagsParser(pgnTagsQuotesOpenedButNotClosed) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: string matching regex '"](|\r|\n|\r\n)' expected but ']' found""".stripMargin)))
      }

      it("related to invalid brackets and carriage returns") {
        val pgnTagsBracketsOpenedButNotClosed: String =
          """[Event "Causal Game"
            |[Site "ChessMultiplayer"]
          """.stripMargin
        val pgnTagsBracketsClosedButNotOpened: String =
          """Event "Causal Game"]
            |[Site "ChessMultiplayer"]
          """.stripMargin
        PgnTagsParser(pgnTagsBracketsOpenedButNotClosed) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: string matching regex '"](|\r|\n|\r\n)' expected but '"' found""".stripMargin)))
        PgnTagsParser(pgnTagsBracketsClosedButNotOpened) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: '[' expected but 'E' found""".stripMargin)))
      }

      it("related to non-alphabetic tag names") {
        val pgnTagsNameWithNumbers: String = """[123 "Test"]"""
        val pgnTagsNameWithSymbols: String = """[? "Test"]"""
        PgnTagsParser(pgnTagsNameWithNumbers) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: string matching regex '[a-zA-Z]+' expected but '1' found""".stripMargin)))
        PgnTagsParser(pgnTagsNameWithSymbols) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: string matching regex '[a-zA-Z]+' expected but '?' found""".stripMargin)))
      }

      it("related to invalid syntax tag values") {
        val pgnTagsValueWithQuotes: String = """[Event ""Test"]"""
        val pgnTagsValueWithClosedBracket: String = """[Event "Test]"]"""
        PgnTagsParser(pgnTagsValueWithQuotes) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: string matching regex '[^\]\r\n"]+' expected but '"' found""".stripMargin)))
        PgnTagsParser(pgnTagsValueWithClosedBracket) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags.
            |An error occurred: string matching regex '"](|\r|\n|\r\n)' expected but ']' found""".stripMargin)))
      }

      it("related to invalid format tag values") {
        val invalidPgnTagValues: String =
          """[Round "Test"]
            |[Date "26/09/2018"]
            |[Result "White wins"]
          """.stripMargin
        PgnTagsParser(invalidPgnTagValues) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN tags. Format errors detected:
            |The tag value 'Test' does not satisfy the format required by 'Round' type
            |The tag value '26/09/2018' does not satisfy the format required by 'Date' type
            |The tag value 'White wins' does not satisfy the format required by 'Result' type""".stripMargin
        )))
      }

    }

  }

}