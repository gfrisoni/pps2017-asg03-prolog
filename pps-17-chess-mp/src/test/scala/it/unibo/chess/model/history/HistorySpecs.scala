package it.unibo.chess.model.history

import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.model.core.DrawReasons.MutualDraw
import it.unibo.chess.model.history.ChessActions._
import it.unibo.chess.model.core._
import it.unibo.chess.model.fen.FenParser

import org.junit.runner.RunWith
import org.scalatest.FunSpec
import org.scalatest.junit.JUnitRunner
import net.liftweb.json._

import scala.language.postfixOps

@RunWith(classOf[JUnitRunner])
class HistorySpecs extends FunSpec {

  val game: ChessGame = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))

  describe("A movement") {

    it("to an empty position should be recognized as move action") {
      val newGame: ChessGame = game move ('a2, 'a3)
      assert(newGame.history.gameSteps.lengthCompare(1) == 0)
      assert(newGame.history.gameSteps.last.action ==
        MoveAction((game.board pieceAt 'a2 get, 'a2), AmbiguityTypes.None, 'a3))
    }

    it("to a position occupied by an opponent piece should be taken") {
      val newGame: ChessGame = game
        .move('e2, 'e4).move('d7, 'd5)
        .move('e4, 'd5)
      assert(newGame.history.gameSteps.lengthCompare(3) == 0)
      assert(newGame.history.gameSteps.last.action ==
        CaptureAction((game.board pieceAt 'e2 get, 'e4), AmbiguityTypes.None, 'd5, game.board pieceAt 'd7 get))
    }

    it("of a pawn to an empty position, passing an adjacent pawn after a double-step move should be recognized as en passant action") {
      val whiteEnPassantGame: ChessGame = game
        .move('d2, 'd4).move('f7, 'f6)
        .move('d4, 'd5).move('c7, 'c5)
        .move('d5, 'c6)
      val blackEnPassantGame: ChessGame = game
        .move('b2, 'b4).move('d7, 'd5)
        .move('b4, 'b5).move('d5, 'd4)
        .move('e2, 'e4).move('d4, 'e3)
      assert(whiteEnPassantGame.history.gameSteps.lengthCompare(5) == 0)
      assert(whiteEnPassantGame.history.gameSteps.last.action ==
        EnPassantAction((game.board pieceAt 'd2 get, 'd5), AmbiguityTypes.None, 'c6, ChessPiece(Black, Pawn(Black))))
      assert(blackEnPassantGame.history.gameSteps.lengthCompare(6) == 0)
      assert(blackEnPassantGame.history.gameSteps.last.action ==
        EnPassantAction((game.board pieceAt 'd7 get, 'd4), AmbiguityTypes.None, 'e3, ChessPiece(White, Pawn(White))))
    }

    it("involving a player's king and either of the player's original rooks should be recognized as castling action") {
      val kingCastlingGame: ChessGame = game
        .move('f2, 'f4).move('f7, 'f5)
        .move('g2, 'g4).move('g7, 'g5)
        .move('g1, 'f3).move('h7, 'h5)
        .move('f1, 'h3).move('e7, 'e5)
        .move('e1, 'g1)
      val queenCastlingGame: ChessGame = game
        .move('b1, 'c3).move('b7, 'b6)
        .move('b2, 'b4).move('c7, 'c6)
        .move('c1, 'a3).move('d7, 'd6)
        .move('d2, 'd3).move('e7, 'e6)
        .move('d1, 'd2).move('f7, 'f6)
        .move('e1, 'c1)
      assert(kingCastlingGame.history.gameSteps.lengthCompare(9) == 0)
      assert(kingCastlingGame.history.gameSteps.last.action == CastlingAction(isKingSide = true))
      assert(queenCastlingGame.history.gameSteps.lengthCompare(11) == 0)
      assert(queenCastlingGame.history.gameSteps.last.action == CastlingAction(isKingSide = false))
    }

    it("of a pawn that reaches its eighth rank should be recognized as promote action") {
      val commonGame: ChessGame = game
        .move('d2, 'd4).move('c7, 'c5)
        .move('d4, 'c5).move('d7, 'd5)
        .move('c5, 'c6).move('c8, 'd7)
        .move('c6, 'c7).move('b7, 'b6)
      val promoteGame: ChessGame = commonGame
        .move('c7, 'c8) promote ('c8, Queen)
      val capturePromoteGame: ChessGame = commonGame
        .move('c7, 'b8) promote ('b8, Knight)
      assert(promoteGame.history.gameSteps.lengthCompare(9) == 0)
      assert(promoteGame.history.gameSteps.last.action ==
        PromoteAction((game.board pieceAt 'd2 get, 'c7), AmbiguityTypes.None, 'c8, Queen))
      assert(capturePromoteGame.history.gameSteps.lengthCompare(9) == 0)
      assert(capturePromoteGame.history.gameSteps.last.action ==
        CapturePromoteAction((game.board pieceAt 'd2 get, 'c7), AmbiguityTypes.None, 'b8, game.board pieceAt 'b8 get, Knight))
    }

  }

  describe("An ambiguous movement") {

    it("to an empty position should be recognized as an ambiguous move action") {
      val ambiguousGame: ChessGame = FenParser.fromFen("3k4/8/8/8/8/8/4N1N1/3K4 w KQkq - 0 1").move('e2, 'f4)
      assert(ambiguousGame.history.gameSteps.last.action.asInstanceOf[MoveAction].ambiguity == AmbiguityTypes.SameRankAmbiguity)
    }

    it("to a position occupied by an opponent piece should be taken and registered as ambiguous capture") {
      val ambiguousGame: ChessGame = FenParser.fromFen("3k4/8/8/8/5p2/8/4N1N1/3K4 w KQkq - 0 1").move('e2, 'f4)
      assert(ambiguousGame.history.gameSteps.last.action.asInstanceOf[CaptureAction].ambiguity == AmbiguityTypes.SameRankAmbiguity)
    }

    it("of a pawn to an empty position, passing an adjacent pawn after a double-step move should be recognized as an ambiguous en passant action") {
      val ambiguousGame: ChessGame = FenParser.fromFen("3k4/8/8/3PpP2/8/8/8/3K4 w KQkq e6 0 1").move('d5, 'e6)
      assert(ambiguousGame.history.gameSteps.last.action.asInstanceOf[EnPassantAction].ambiguity == AmbiguityTypes.SameRankAmbiguity)
    }

    it("of a pawn that reaches its eighth rank should be recognized as an ambiguous promote action") {
      val ambiguousGame: ChessGame = FenParser.fromFen("1b2k3/P1P5/8/8/8/8/8/3K4 w KQkq - 0 1").move('a7, 'b8) promote ('b8, Queen)
      assert(ambiguousGame.history.gameSteps.last.action.asInstanceOf[CapturePromoteAction].ambiguity == AmbiguityTypes.SameRankAmbiguity)
    }

  }

  describe("A game history") {

    it("should be represented in Json format") {
      implicit val formats: DefaultFormats.type = DefaultFormats
      val newGame: ChessGame = game
        .move('e2, 'e4).move('d7, 'd5)
        .move('e4, 'd5)
      assert(parse(newGame.history.toJson).extract[List[String]] == List("e4", "d5", "exd5"))
    }

    describe("should be able to retrieve the ending action") {

      it("if registered") {
        val finishedGame: ChessGame = game
          .move('e2, 'e4).move('d7, 'd5)
          .draw(MutualDraw)
        assert(finishedGame.history.gameResult.contains(DrawAction(MutualDraw)))
      }

      it("returning an empty value if not registered") {
        assert(game.history.gameResult.isEmpty)
      }

    }

  }

}