package it.unibo.chess.model.core

import java.lang.Math.abs

import it.unibo.chess.exceptions.Board.BoardException
import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.model.core.position.ChessBoardPosition._
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition, GridPositionImpl}
import it.unibo.chess.model.utilities.ChessUtilities.sideLengthIterable
import org.junit.runner.RunWith
import org.scalatest.FunSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PositionSpecs extends FunSpec {

  val boardPositions: Iterable[BoardPosition] = for (col <- sideLengthIterable; row <- sideLengthIterable) yield ChessBoardPosition(col, row)

  describe("A grid position") {

    it("should be used to calculate any relative position") {
      val pos: GridPositionImpl = GridPositionImpl(1, 1)
      assert((pos ^ 10) == Option.apply(GridPositionImpl(1, 11)))
      assert((pos v 20) == Option.apply(GridPositionImpl(1, -19)))
      assert((pos > 4) == Option.apply(GridPositionImpl(5, 1)))
      assert((pos < 1) == Option.apply(GridPositionImpl(0, 1)))
    }

    it("should be used to calculate the positions crossed during a movement in one direction") {
      val pos: GridPositionImpl = GridPositionImpl(2, 2)
      assert((pos ^^ 2) == List(GridPositionImpl(2, 2), GridPositionImpl(2, 3), GridPositionImpl(2, 4)))
      assert((pos vv 3) == List(GridPositionImpl(2, 2), GridPositionImpl(2, 1), GridPositionImpl(2, 0), GridPositionImpl(2, -1)))
      assert((pos >> 1) == List(GridPositionImpl(2, 2), GridPositionImpl(3, 2)))
      assert((pos << 3) == List(GridPositionImpl(2, 2), GridPositionImpl(1, 2), GridPositionImpl(0, 2), GridPositionImpl(-1, 2)))
    }

  }

  describe("A chess board position") {

    it("should be derived from a string") {
      val pos: BoardPosition = ChessBoardPosition((4, 5))
      assert(pos == ChessBoardPosition("d5"))
    }

    it("should be derived from a symbol") {
      val pos: BoardPosition = ChessBoardPosition((4, 5))
      assert(pos == ChessBoardPosition('d5))
    }

    it("should be represented as alphanumeric string") {
      val pos: BoardPosition = ChessBoardPosition((4, 5))
      assert(pos.toString == "d5")
    }

    it("should be not equal to another object") {
      val pos: BoardPosition = ChessBoardPosition((4, 5))
      val whitePawn: ChessPiece = ChessPiece(White, Pawn(White))
      assert(pos != whitePawn)
    }

    it("should be inside the chess board") {
      assertThrows[BoardException](ChessBoardPosition((9, 8)))
    }

    it("should be used to derive a relative position on the board" ) {
      val pos: BoardPosition = 'd5
      assert((pos ^ 1) == Option.apply(ChessBoardPosition('d6)))
      assert((pos v 1) == Option.apply(ChessBoardPosition('d4)))
      assert((pos > 1) == Option.apply(ChessBoardPosition('e5)))
      assert((pos < 1) == Option.apply(ChessBoardPosition('c5)))
    }

    it("should be used to calculate a non-position off the edge of the board") {
      val pos: BoardPosition = 'd5
      assert((pos ^ 4) == Option.empty)
      assert((pos v 5) == Option.empty)
      assert((pos > 5) == Option.empty)
      assert((pos < 4) == Option.empty)
    }

    it("should be able to calculate a relative position with negative numbers") {
      val pos: BoardPosition = 'd5
      assert((pos ^ -2) == Option.apply(ChessBoardPosition('d3)))
      assert((pos > -3) == Option.apply(ChessBoardPosition('a5)))
      assert((pos v -1) == Option.apply(ChessBoardPosition('d6)))
      assert((pos < -3) == Option.apply(ChessBoardPosition('g5)))
    }

    it("should be used to calculate a non-position off the edge of the board using negative numbers") {
      val pos: BoardPosition = 'd5
      assert((pos ^ -6) == Option.empty)
      assert((pos v -6) == Option.empty)
      assert((pos > -6) == Option.empty)
      assert((pos < -6) == Option.empty)
    }

    it("should advise if it is along the same row as another position") {
      for (one <- boardPositions; another <- boardPositions) {
        if (one.row == another.row) {
          assert(one -? another)
          assert(another -? one)
        } else {
          assert(!(one -? another))
          assert(!(another -? one))
        }
      }
    }

    it("should advise if it is along the same column as another position") {
      for (one <- boardPositions; another <- boardPositions) {
        if (one.col == another.col) {
          assert(one |? another)
          assert(another |? one)
        } else {
          assert(!(one |? another))
          assert(!(another |? one))
        }
      }
    }

    it("should advise if it is along the same diagonal as another position") {
      for (one <- boardPositions; another <- boardPositions) {
        if (abs(one.col - another.col) == abs(one.row - another.row)) {
          assert(one /? another)
          assert(another /? one)
        } else {
          assert(!(one /? another))
          assert(!(another /? one))
        }
      }
    }

    it("should advise if it is along the same diagonal or the same row or the same column as another position") {
      for (one <- boardPositions; another <- boardPositions) {
        if ((one.row == another.row) ||
          (one.col == another.col) ||
          (abs(one.col - another.col) == abs(one.row - another.row))) {
          assert(one *? another)
          assert(another *? one)
        } else {
          assert(!(one *? another))
          assert(!(another *? one))
        }
      }
    }

    it("should be used as starting point to calculate radial positions with a round range on the board") {
      val pos: BoardPosition = 'd5
      assert(radialBasedPositions(pos, -1 to 1, (_,_) => true)
        == ChessBoardPosition.getPositionsFromSymbols(Vector('e4, 'd4, 'c4, 'e5, 'd5, 'c5, 'e6, 'd6, 'c6)))
      assert(radialBasedPositions(pos, -1 to 1, (row: Int, col: Int) => {row != 0 || col != 0})
        == ChessBoardPosition.getPositionsFromSymbols(Vector('e4, 'd4, 'c4, 'e5, 'c5, 'e6, 'd6, 'c6)))
      val criticalPos: BoardPosition = 'a8
      assert(radialBasedPositions(criticalPos, -1 to 1, (row: Int, col: Int) => {row != 0 || col != 0})
        == ChessBoardPosition.getPositionsFromSymbols(Vector('b7, 'a7, 'b8)))
    }

    it("should be used as starting point to calculate radial positions on the board with custom offsets on the board") {
      val pos: BoardPosition = 'd5
      assert(radialBasedPositions(pos, Set(-2, -1, 1, 2), (row: Int, col: Int) => {abs(row) != abs(col)})
        == ChessBoardPosition.getPositionsFromSymbols(Set('c7, 'e7, 'b6, 'f6, 'b4, 'f4, 'c3, 'e3)))
    }

    it("should be used as starting point to calculate vector positions on the board") {
      val pos: BoardPosition = 'd5
      val rowVectors = List(List(< _), List(> _))
      val columnVectors = List(List(^ _), List(v _))
      val diagonalVectors = List(List(^ _, < _), List(^ _, > _), List(v _, < _), List(v _, > _))
      assert(vectorBasedPositions(pos, rowVectors)
        == List(ChessBoardPosition.getPositionsFromSymbols(List('c5, 'b5, 'a5)),
        ChessBoardPosition.getPositionsFromSymbols(List('e5, 'f5, 'g5, 'h5))))
      assert(vectorBasedPositions(pos, columnVectors)
        == List(ChessBoardPosition.getPositionsFromSymbols(List('d6, 'd7, 'd8)),
        ChessBoardPosition.getPositionsFromSymbols(List('d4, 'd3, 'd2, 'd1))))
      assert(vectorBasedPositions(pos, diagonalVectors)
        == List(ChessBoardPosition.getPositionsFromSymbols(List('c6, 'b7, 'a8)),
        ChessBoardPosition.getPositionsFromSymbols(List('e6, 'f7, 'g8)),
        ChessBoardPosition.getPositionsFromSymbols(List('c4, 'b3, 'a2)),
        ChessBoardPosition.getPositionsFromSymbols(List('e4, 'f3, 'g2, 'h1))))
    }

  }

  describe("More chess board positions") {

    it("should be derived from a collection of strings") {
      assert(ChessBoardPosition.getPositionsFromStrings(Set("a1", "a2"))
        == Set(ChessBoardPosition((1, 2)), ChessBoardPosition(1, 1)))
      assert(ChessBoardPosition.getPositionsFromStrings(List("a1", "a2"))
        == List(ChessBoardPosition((1, 1)), ChessBoardPosition(1, 2)))
      assert(ChessBoardPosition.getPositionsFromStrings(List("a1", "a2"))
        != List(ChessBoardPosition((1, 2)), ChessBoardPosition(1, 1)))
    }

    it("should be derived from a collection of symbols") {
      assert(ChessBoardPosition.getPositionsFromSymbols(Set('a1, 'a2))
        == Set(ChessBoardPosition((1, 2)), ChessBoardPosition(1, 1)))
      assert(ChessBoardPosition.getPositionsFromSymbols(List('a1, 'a2))
        == List(ChessBoardPosition((1, 1)), ChessBoardPosition(1, 2)))
      assert(ChessBoardPosition.getPositionsFromSymbols(List('a1, 'a2))
        != List(ChessBoardPosition((1, 2)), ChessBoardPosition(1, 1)))
    }

    it("should be derived from a collection of numeric coordinates") {
      var positions: Set[BoardPosition] = Set.empty
      positions += ChessBoardPosition((1, 1))
      positions += ChessBoardPosition((1, 2))
      assert(ChessBoardPosition.getPositions(Set((1, 1), (1, 2))) == positions)
    }

  }

}