package it.unibo.chess.model.core

import it.unibo.chess.model.utilities.ChessUtilities.{BOARD_END_INDEX, BOARD_START_INDEX}
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ChessColorSpecs extends FlatSpec {

  val white: ChessColor = White
  val black: ChessColor = Black

  "Something White" should "have Black as opposite" in {
    assert(white == !black)
  }

  "Something White" should "start in the bottom row" in{
    assert(white.startingRow == BOARD_START_INDEX)
  }

  "Something Black" should "have White as opposite" in {
    assert(white == !black)
  }

  "Something Black" should "start in the top row" in{
    assert(black.startingRow == BOARD_END_INDEX)
  }

}
