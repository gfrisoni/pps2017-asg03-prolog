package it.unibo.chess.model.utilities

import org.scalatest.FlatSpec
import CharFormatUtilities._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CharFormatUtilitiesSpecs extends FlatSpec {

  "a number between 1 and " + ALPHABET_LENGTH  should "be converted" in {
    assert(intToLetter(1) == "a")
    assert(intToLetter(3) == "c")
    assert(intToLetter(9) == "i")
    assert(intToLetter(14) == "n")
    assert(intToLetter(26) == "z")
  }

  "a number outside the allowed range" should "throw an exception" in {
    assertThrows[IndexOutOfBoundsException](intToLetter(-2))
    assertThrows[IndexOutOfBoundsException](intToLetter(ALPHABET_LENGTH + 1))
    assertThrows[IndexOutOfBoundsException](intToLetter(ALPHABET_LENGTH + 20))
  }

}
