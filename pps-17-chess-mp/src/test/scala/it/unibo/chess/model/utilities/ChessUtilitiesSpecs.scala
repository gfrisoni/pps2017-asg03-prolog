package it.unibo.chess.model.utilities

import org.scalatest.FlatSpec
import ChessUtilities._
import it.unibo.chess.exceptions.Board.BoardException
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ChessUtilitiesSpecs extends FlatSpec {

  "the iterable" should "iterate within the allowed range" in {
    var i: Int = 0
    for (counter <- sideLengthIterable) {
      i = counter
      assert(i >= BOARD_START_INDEX)
      assert(i <= BOARD_END_INDEX)
    }
    assert(i == BOARD_END_INDEX)
  }

  "a symbol" should "be converted in a Tuple[Int,Int] coordinate" in {
    assert(coordinateToTuple('a1) == ((1, 1)))
    assert(coordinateToTuple('b2) == ((2, 2)))
    assert(coordinateToTuple('e8) == ((5, 8)))
    assert(coordinateToTuple('h4) == ((8, 4)))
  }

  "a symbol not matching a chess coordinate" should "throw an exception" in {
    assertThrows[BoardException](coordinateToTuple(null))
    assertThrows[BoardException](coordinateToTuple('b9))
    assertThrows[BoardException](coordinateToTuple('q1))
    assertThrows[BoardException](coordinateToTuple('a14))
    assertThrows[BoardException](coordinateToTuple('foo))
  }

}
