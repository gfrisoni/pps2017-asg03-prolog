package it.unibo.chess.model.pgn

import com.github.nscala_time.time.Imports.DateTime
import it.unibo.chess.model.pgn.PgnTagTypes._
import org.junit.runner.RunWith
import org.scalatest.{FunSpec, Matchers}
import org.scalatest.junit.JUnitRunner

import scalaz.Success

@RunWith(classOf[JUnitRunner])
class PgnTagsSpecs extends FunSpec with Matchers {

  describe("A collection of PGN tags") {

    val exampleTags = PgnTags(
      Seq(
        PgnTag(Site, "Chess Multiplayer"),
        PgnTag(Round, "-"),
        PgnTag(Date, "2018.09.10"),
        PgnTag(Black, "Leonardo Montini"),
        PgnTag(White, "Giacomo Frisoni"),
        PgnTag(Result, "1-0"),
        PgnTag(Event, "Casual Game")
      ).collect { case Success(tag) => tag }
    )

    describe("should be able to verify the existence of a PGN tag") {
      exampleTags.exists(Site) should be(true)
    }

    describe("should be able to create a new collection with another PGN tag") {

      it("replacing the old one if related to a tag with a type already present") {
        (exampleTags + PgnTag(Event, "New value").toOption.get) should be(PgnTags(
          Seq(
            PgnTag(Site, "Chess Multiplayer"),
            PgnTag(Round, "-"),
            PgnTag(Date, "2018.09.10"),
            PgnTag(Black, "Leonardo Montini"),
            PgnTag(White, "Giacomo Frisoni"),
            PgnTag(Result, "1-0"),
            PgnTag(Event, "New value")
          ).collect { case Success(tag) => tag })
        )
      }

      it("added to the existing ones if not already present") {
        (exampleTags + PgnTag(WhiteType, "human").toOption.get) should be(PgnTags(
          Seq(
            PgnTag(Site, "Chess Multiplayer"),
            PgnTag(Round, "-"),
            PgnTag(Date, "2018.09.10"),
            PgnTag(Black, "Leonardo Montini"),
            PgnTag(White, "Giacomo Frisoni"),
            PgnTag(Result, "1-0"),
            PgnTag(Event, "Casual Game"),
            PgnTag(WhiteType, "human")
          ).collect { case Success(tag) => tag })
        )
      }

    }

    describe("should be able to create a new collection with more PGN tags") {

      it("updating the tags with a type already present") {
        (exampleTags ++
          PgnTags(
            Seq(PgnTag(Site, "New value"), PgnTag(Event, "New value")).collect { case Success(tag) => tag }
          )) should be(PgnTags(
          Seq(
            PgnTag(Round, "-"),
            PgnTag(Date, "2018.09.10"),
            PgnTag(Black, "Leonardo Montini"),
            PgnTag(White, "Giacomo Frisoni"),
            PgnTag(Result, "1-0"),
            PgnTag(Site, "New value"),
            PgnTag(Event, "New value")
          ).collect { case Success(tag) => tag })
        )
      }

      it("added to the existing ones if not already present") {
        (exampleTags ++
          PgnTags(
            Seq(PgnTag(WhiteType, "human"), PgnTag(BlackType, "human")).collect { case Success(tag) => tag }
          )) should be(PgnTags(
          Seq(
            PgnTag(Site, "Chess Multiplayer"),
            PgnTag(Round, "-"),
            PgnTag(Date, "2018.09.10"),
            PgnTag(Black, "Leonardo Montini"),
            PgnTag(White, "Giacomo Frisoni"),
            PgnTag(Result, "1-0"),
            PgnTag(Event, "Casual Game"),
            PgnTag(WhiteType, "human"),
            PgnTag(BlackType, "human")
          ).collect { case Success(tag) => tag })
        )
      }

    }

    it("should be sorted by the Seven Tag Roster rules") {
      exampleTags.sorted.getTags.map(_.tagType) should be(Seq(
        Event,
        Site,
        Date,
        Round,
        White,
        Black,
        Result
      ))
      (exampleTags ++
        PgnTags(
          Seq(PgnTag(WhiteType, "human"), PgnTag(BlackType, "human")).collect { case Success(tag) => tag }
        )).sorted.getTags.map(_.tagType) should be(Seq(
        Event,
        Site,
        Date,
        Round,
        White,
        Black,
        Result,
        WhiteType,
        BlackType
      ))
    }

    describe("should be able to handle the date") {

      it("retrieving it if the associated tag is present") {
        val date: Option[DateTime] = exampleTags.date
        date.isDefined should be(true)
        date.get.getYear should be(2018)
        date.get.getMonthOfYear should be(9)
        date.get.getDayOfMonth should be(10)
      }

      it("retrieving an empty value if the associated tag is not present") {
        PgnTags(
          Seq(PgnTag(Event, "Casual Game"), PgnTag(Date, "29/09/2018")).collect { case Success(tag) => tag }
        ).date should be(None)
      }

    }

    describe("should be able to handle the year") {

      it("retrieving it if the date tag is present") {
        exampleTags.year should be(Some(2018))
      }

      it("retrieving an empty value if the date tag is not present") {
        PgnTags(
          Seq(PgnTag(Event, "Casual Game"), PgnTag(Date, "29/09/2018")).collect { case Success(tag) => tag }
        ).year should be(None)
      }

    }

    it("should be able to generate a properly string representation") {
      exampleTags.toString should be(
        """[Site "Chess Multiplayer"]
          |[Round "-"]
          |[Date "2018.09.10"]
          |[Black "Leonardo Montini"]
          |[White "Giacomo Frisoni"]
          |[Result "1-0"]
          |[Event "Casual Game"]""".stripMargin)
    }

  }

}
