package it.unibo.chess.model.pgn

import it.unibo.chess.model.core._
import it.unibo.chess.model.fen.FenParser
import it.unibo.chess.model.history.ChessActions._
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.ChessRoles.{Pawn, Queen}
import org.junit.runner.RunWith
import org.scalatest.{FunSpec, Matchers}
import org.scalatest.junit.JUnitRunner
import scalaz.{NonEmptyList, ValidationNel}

@RunWith(classOf[JUnitRunner])
class PgnMoveParserSpecs extends FunSpec with Matchers {

  describe("A PGN move parser") {

    describe("should be able to convert successfully") {

      describe("a legal move represented by SAN into the game resultant by its application") {

        describe("with a movement") {

          it("of a pawn") {
            val pawnMoveSan: String = "a3"
            val defaultGame: ChessGame = FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(pawnMoveSan, defaultGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(MoveAction(
                (defaultGame.board.pieceAt('a2).get, 'a2),
                AmbiguityTypes.None,
                'a3
              ))
            })
          }

          it("of a piece that is not a pawn") {
            val knightMoveSan: String = "Nc3"
            val defaultGame: ChessGame = FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(knightMoveSan, defaultGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(MoveAction(
                (defaultGame.board.pieceAt('b1).get, 'b1),
                AmbiguityTypes.None,
                'c3
              ))
            })
          }

          it("that is ambiguous") {
            val knightMoveSan: String = "Nce4"
            val ambiguousMoveGame: ChessGame = FenParser.fromFen("rnbqkbnr/ppp3pp/3ppp2/6N1/8/2N5/PPPPPPPP/R1BQKB1R w KQkq - 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(knightMoveSan, ambiguousMoveGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(MoveAction(
                (ambiguousMoveGame.board.pieceAt('c3).get, 'c3),
                AmbiguityTypes.CompleteAmbiguity,
                'e4
              ))
            })
          }

        }

        describe("with a capture") {

          it("of a pawn") {
            val pawnCaptureSan: String = "xd5"
            val pawnCaptureGame: ChessGame = FenParser.fromFen("rnbqkbnr/ppp1pppp/8/3p4/2P5/8/PP1PPPPP/RNBQKBNR w KQkq - 0 1")
            PgnMoveParser(pawnCaptureSan, pawnCaptureGame).map(nextGame => {
              nextGame.history.gameSteps.last.action should be(CaptureAction(
                (pawnCaptureGame.board.pieceAt('c4).get, 'c4),
                AmbiguityTypes.None,
                'd5,
                pawnCaptureGame.board.pieceAt('d5).get
              ))
            })
          }

          it("of a piece that is not a pawn") {
            val knightCaptureSan: String = "Nxd5"
            val knightCaptureGame: ChessGame = FenParser.fromFen("rnbqkbnr/ppp1pppp/8/3p4/8/2N5/PPPPPPPP/R1BQKBNR w KQkq - 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(knightCaptureSan, knightCaptureGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(CaptureAction(
                (knightCaptureGame.board.pieceAt('c3).get, 'c3),
                AmbiguityTypes.None,
                'd5,
                knightCaptureGame.board.pieceAt('d5).get
              ))
            })
          }

          it("that is ambiguous") {
            val pawnCaptureSan: String = "bxc5"
            val ambiguousCaptureGame: ChessGame = FenParser.fromFen("rnbqkbnr/pp2pppp/8/2pp4/1P1P4/8/P1P1PPPP/RNBQKBNR w KQkq - 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(pawnCaptureSan, ambiguousCaptureGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(CaptureAction(
                (ambiguousCaptureGame.board.pieceAt('b4).get, 'b4),
                AmbiguityTypes.SameRankAmbiguity,
                'c5,
                ambiguousCaptureGame.board.pieceAt('c5).get
              ))
            })
          }

        }

        describe("with an en passant") {

          it("that is not ambiguous") {
            val pawnEnPassantSan: String = "xc6"
            val enPassantGame: ChessGame = FenParser.fromFen("rnbqkbnr/pp1pp1pp/5p2/1Pp5/8/8/P1PPPPPP/RNBQKBNR w KQkq c6 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(pawnEnPassantSan, enPassantGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(EnPassantAction(
                (enPassantGame.board.pieceAt('b5).get, 'b5),
                AmbiguityTypes.None,
                'c6,
                ChessPiece(Black, Pawn(Black))
              ))
            })
          }

          it("that is ambiguous") {
            val pawnEnPassantSan: String = "dxe6"
            val enPassantGame: ChessGame = FenParser.fromFen("3k4/8/8/3PpP2/8/8/8/3K4 w KQkq e6 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(pawnEnPassantSan, enPassantGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(EnPassantAction(
                (enPassantGame.board.pieceAt('d5).get, 'd5),
                AmbiguityTypes.SameRankAmbiguity,
                'e6,
                ChessPiece(Black, Pawn(Black))
              ))
            })
          }

        }

        describe("with a promotion") {

          it("that is not ambiguous") {
            val promotionSan: String = "c8=Q"
            val promotionGame: ChessGame = FenParser.fromFen("rn1qkbnr/p1P1pppp/8/1p1p1b2/3P4/8/P1P1PPPP/RNBQKBNR w KQkq - 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(promotionSan, promotionGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(PromoteAction(
                (promotionGame.board.pieceAt('c7).get, 'c7),
                AmbiguityTypes.None,
                'c8,
                Queen
              ))
            })
          }

        }

        describe("with a capture and promotion") {

          it("that is not ambiguous") {
            val promotionSan: String = "xb8=Q"
            val promotionGame: ChessGame = FenParser.fromFen("rn1qkbnr/p1P1pppp/8/1p1p1b2/3P4/8/P1P1PPPP/RNBQKBNR w KQkq - 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(promotionSan, promotionGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(CapturePromoteAction(
                (promotionGame.board.pieceAt('c7).get, 'c7),
                AmbiguityTypes.None,
                'b8,
                promotionGame.board.pieceAt('b8).get,
                Queen
              ))
            })
          }

          describe("that is ambiguous") {

            it("on ranks") {
              val promotionSan: String = "axb8=Q"
              val promotionGame: ChessGame = FenParser.fromFen("rn1qkbnr/P1P1pppp/8/1p1p1b2/3P4/8/2P1PPPP/RNBQKBNR w KQkq - 0 1")
              val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(promotionSan, promotionGame)
              parserResult.isSuccess should be(true)
              parserResult.map(nextGame => {
                nextGame.history.gameSteps.last.action should be(CapturePromoteAction(
                  (promotionGame.board.pieceAt('a7).get, 'a7),
                  AmbiguityTypes.SameRankAmbiguity,
                  'b8,
                  promotionGame.board.pieceAt('b8).get,
                  Queen
                ))
              })
            }

            it("on files") {
              val captureSan: String = "N4xe5"
              val captureGame: ChessGame = FenParser.fromFen("rnbqkbnr/ppp1pppp/2N5/4p3/2N5/8/PPPPPPPP/R1BQKB1R w KQkq - 0 1")
              val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(captureSan, captureGame)
              parserResult.isSuccess should be(true)
              parserResult.map(nextGame => {
                nextGame.history.gameSteps.last.action should be(CaptureAction(
                  (captureGame.board.pieceAt('c4).get, 'c4),
                  AmbiguityTypes.SameFileAmbiguity,
                  'e5,
                  captureGame.board.pieceAt('e5).get
                ))
              })
            }

            it("on ranks and files") {
              val captureSan: String = "Nf6xe4"
              val knightCircleGame: ChessGame = FenParser.fromFen("3k4/8/3N1N2/2N3N1/4p3/2N3N1/3N1N2/4K3 w KQkq - 0 1")
              val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(captureSan, knightCircleGame)
              parserResult.isSuccess should be(true)
              parserResult.map(nextGame => {
                nextGame.history.gameSteps.last.action should be(CaptureAction(
                  (knightCircleGame.board.pieceAt('f6).get, 'f6),
                  AmbiguityTypes.CompleteAmbiguity,
                  'e4,
                  knightCircleGame.board.pieceAt('e4).get
                ))
              })
            }

          }

        }

        describe("with a castling") {

          describe("of the white player") {

            it("that is short") {
              val castlingSan: String = "0-0"
              val castlingGame: ChessGame = FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQK2R w KQkq - 0 1")
              val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(castlingSan, castlingGame)
              parserResult.isSuccess should be(true)
              parserResult.map(nextGame => {
                nextGame.history.gameSteps.last.action should be(CastlingAction(
                  isKingSide = true
                ))
              })
            }

            it("that is long") {
              val castlingSan: String = "0-0-0"
              val castlingGame: ChessGame = FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/R3KBNR w KQkq - 0 1")
              val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(castlingSan, castlingGame)
              parserResult.isSuccess should be(true)
              parserResult.map(nextGame => {
                nextGame.history.gameSteps.last.action should be(CastlingAction(
                  isKingSide = false
                ))
              })
            }
          }

        }

        describe("of the black player") {

          it("that is short") {
            val castlingSan: String = "0-0"
            val castlingGame: ChessGame = FenParser.fromFen("rnbqk2r/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b KQkq - 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(castlingSan, castlingGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(CastlingAction(
                isKingSide = true
              ))
            })
          }

          it("that is long") {
            val castlingSan: String = "0-0-0"
            val castlingGame: ChessGame = FenParser.fromFen("r3kbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b KQkq - 0 1")
            val parserResult: ValidationNel[String, ChessGame] = PgnMoveParser(castlingSan, castlingGame)
            parserResult.isSuccess should be(true)
            parserResult.map(nextGame => {
              nextGame.history.gameSteps.last.action should be(CastlingAction(
                isKingSide = false
              ))
            })
          }
        }

      }

    }

    describe("should fail") {

      describe("if the SAN string contains syntactical error") {

        val game: ChessGame = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))

        it("related to invalid moves") {
          val invalidSanCapture: String = "Nxb4xc3"
          val invalidSanCastling: String = "0-0-0-0"
          PgnMoveParser(invalidSanCapture, game) should be(scalaz.Failure(NonEmptyList(
            s"The SAN string $invalidSanCapture has an invalid syntax"
          )))
          PgnMoveParser(invalidSanCastling, game) should be(scalaz.Failure(NonEmptyList(
            s"The SAN string $invalidSanCastling has an invalid syntax"
          )))
        }

        it("related to an empty target position") {
          val invalidSanTarget: String = "Nex"
          val game: ChessGame = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))
          PgnMoveParser(invalidSanTarget, game) should be(scalaz.Failure(NonEmptyList(
            s"The SAN string $invalidSanTarget has an invalid syntax"
          )))
        }

        it("related to an invalid target position") {
          val invalidSanTarget: String = "xz9"
          val game: ChessGame = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))
          PgnMoveParser(invalidSanTarget, game) should be(scalaz.Failure(NonEmptyList(
            s"The SAN string $invalidSanTarget has an invalid syntax"
          )))
        }

      }

      describe("if the SAN string is syntactical correct but invalid because") {

        it("related to a sequence of two moves done by the same player") {
          val doubleSanMove: String = "b4"
          val game: ChessGame = FenParser.fromFen("rnbqkbnr/pppppppp/8/8/P7/8/1PPPPPPP/RNBQKBNR b KQkq - 0 1")
          PgnMoveParser(doubleSanMove, game) should be(scalaz.Failure(NonEmptyList(
            s"The move $doubleSanMove is illegal for the chess role Pawn(Black)"
          )))
        }

        it("related to an ambiguous move") {
          val ambiguousSanMove: String = "xb5"
          val game: ChessGame = FenParser.fromFen("rnbqkbnr/p1pppppp/8/1p6/P1P5/8/1P1PPPPP/RNBQKBNR w KQkq - 0 1")
          PgnMoveParser(ambiguousSanMove, game) should be(scalaz.Failure(NonEmptyList(
            "The move xb5 is ambiguous: multiple pieces can go into b5 square"
          )))
        }

        it("related to an invalid promotion") {
          val invalidSanPromotion: String = "Nxb8=Q"
          val game: ChessGame = FenParser.fromFen("1b2k3/8/2N5/8/8/8/8/4K3 w KQkq - 0 1")
          PgnMoveParser(invalidSanPromotion, game) should be(scalaz.Failure(NonEmptyList(
            "A piece with role Knight cannot promote"
          )))
        }

        it("related to an illegal move") {
          val illegalSanMove: String = "Ra6"
          val game: ChessGame = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))
          PgnMoveParser(illegalSanMove, game) should be(scalaz.Failure(NonEmptyList(
            s"The move $illegalSanMove is illegal for the chess role Rook"
          )))
        }

        describe("related to an inconsistency between the SAN and the game status for false") {

          describe("free") {

            val game: ChessGame = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))

            it("but represented as checks") {
              val inconsistentSanCheck: String = "a4+"
              PgnMoveParser(inconsistentSanCheck, game).isFailure should be(true)
            }

            it("but represented as checkmates") {
              val inconsistentSanCheck: String = "a4#"
              PgnMoveParser(inconsistentSanCheck, game).isFailure should be(true)
            }

          }

          describe("checks") {

            val game: ChessGame = FenParser.fromFen("rnbqkbnr/ppp2ppp/3pp3/8/8/4P3/PPPP1PPP/RNBQKBNR w KQkq - 0 1")

            it("but represented as free") {
              val inconsistentSanCheck: String = "Bb5"
              PgnMoveParser(inconsistentSanCheck, game).isFailure should be(true)
            }

            it("but represented as checkmates") {
              val inconsistentSanCheck: String = "Bb5#"
              PgnMoveParser(inconsistentSanCheck, game).isFailure should be(true)
            }

          }

          describe("captures") {

            val game: ChessGame = FenParser.fromFen("rnbqkbnr/ppp1pppp/8/3p4/4P3/2P5/PP1P1PPP/RNBQKBNR w KQkq - 0 1")

            it("represented but not real") {
              val inconsistentSanCheck: String = "xa4"
              PgnMoveParser(inconsistentSanCheck, game).isFailure should be(true)
            }

            it("real but not represented") {
              val inconsistentSanCheck: String = "d5"
              PgnMoveParser(inconsistentSanCheck, game).isFailure should be(true)
            }

          }

          it("promotions represented but not real") {
            val inconsistentSanCheck: String = "a4=Q"
            val game: ChessGame = FenParser.fromFen("r1bqkbnr/pPp1pppp/2n5/3p4/4p3/2P5/P2P1PPP/RNBQKBNR w KQkq - 0 1")
            PgnMoveParser(inconsistentSanCheck, game).isFailure should be(true)
          }

        }

      }

    }

    it("should not skip whitespaces") {
      val inconsistentSanCheck: String = "a 3"
      val game: ChessGame = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))
      PgnMoveParser(inconsistentSanCheck, game).isFailure should be(true)
    }

  }

}