package it.unibo.chess.model.history.notation

import it.unibo.chess.model.history.ChessActions._

/**
  * Models a serializer for the steps of a chess game.
  * It uses Template method pattern.
  */
abstract class ChessGameActionSerializer extends BoardGameActionSerializer[ChessAction] {

  override def serializeGameAction(action: ChessAction): String = {
    action match {
      case a: MoveAction => move(a)
      case a: CaptureAction => capture(a)
      case a: CapturePromoteAction => capturePromote(a)
      case a: EnPassantAction => enPassant(a)
      case a: PromoteAction => promote(a)
      case a: CastlingAction => castling(a)
      case a: DrawAction => draw(a)
      case a: LoseAction => lose(a)
    }
  }

  override def serializeGameActions(actions: ChessAction*): Seq[String] = actions.map(a => serializeGameAction(a))

  protected def move(action: MoveAction): String

  protected def capture(action: CaptureAction): String

  protected def capturePromote(action: CapturePromoteAction): String

  protected def enPassant(action: EnPassantAction): String

  protected def promote(action: PromoteAction): String

  protected def castling(action: CastlingAction): String

  protected def draw(action: DrawAction): String

  protected def lose(action: LoseAction): String

}