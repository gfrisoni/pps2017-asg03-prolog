package it.unibo.chess.model.history

import it.unibo.chess.model.core.AmbiguityTypes.AmbiguityType
import it.unibo.chess.model.core.ChessRoles.ChessRole
import it.unibo.chess.model.core.DrawReasons.DrawReason
import it.unibo.chess.model.core.KingStates.{Free, KingState}
import it.unibo.chess.model.core.LoseReasons.LoseReason
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.BoardPosition

/**
  * Handles all the actions of a chess game.
  */
object ChessActions {

  /**
    * A generic action inside a chess game.
    */
  sealed trait ChessAction extends BoardGameAction[ChessPiece]

  /**
    * A generic ending action for a chess game.
    */
  sealed trait ChessEndingAction extends ChessAction

  /**
    * A generic action that can be related to a king state.
    */
  sealed trait CheckableAction extends ChessAction {
    /**
      * @return the state of the king related to the action.
      */
    def kingState: KingState
  }

  /**
    * A generic move action inside a chess game.
    */
  sealed trait ChessMoveAction extends CheckableAction {
    /**
      * @return the moved piece and its starting position.
      */
    def from: (ChessPiece, BoardPosition)
    /**
      * @return the type of ambiguity related to the chess move.
      */
    def ambiguity: AmbiguityType
    /**
      * @return the arrival position.
      */
    def toPosition: BoardPosition
  }

  /**
    * A generic capture inside a chess game.
    */
  sealed trait ChessCaptureAction extends ChessMoveAction {
    /**
      * @return the captured chess piece.
      */
    def capturedPiece: ChessPiece
  }

  /**
    * A generic chess game action related to pawn promotion.
    */
  sealed trait ChessPromotionAction extends CheckableAction {
    /**
      * @return the promotion role.
      */
    def promoteRole: ChessRole
  }

  /**
    * A generic chess game action related to the castling.
    */
  sealed trait ChessCastlingAction extends CheckableAction {
    /**
      * @return {{{true}}} if the castling is short, {{{false}}} otherwise.
      */
    def isKingSide: Boolean
  }

  /**
    * A generic draw for a chess game.
    */
  sealed trait ChessDrawAction extends ChessEndingAction {
    /**
      * @return the reason of the draw ending.
      */
    def drawReason: DrawReason
  }

  /**
    * A generic lose for a chess game.
    */
  sealed trait ChessLoseAction extends ChessEndingAction {
    /**
      * @return the color of the loser player.
      */
    def loserColor: ChessColor
    /**
      * @return the reason of the lose ending.
      */
    def loseReason: LoseReason
  }

  /**
    * A move action of a chess game.
    *
    * @param from the moved piece and its starting position
    * @param ambiguity the type of ambiguity related to the chess move
    * @param toPosition the arrival position
    * @param kingState the state of the king related to the action
    */
  case class MoveAction(override val from: (ChessPiece, BoardPosition),
                        override val ambiguity: AmbiguityType = AmbiguityTypes.None,
                        override val toPosition: BoardPosition,
                        override val kingState: KingState = Free) extends ChessMoveAction

  /**
    * A capture action of a chess game.
    *
    * @param from the moved piece and its starting position
    * @param ambiguity the type of ambiguity related to the chess move
    * @param toPosition the arrival position
    * @param capturedPiece the captured piece
    * @param kingState the state of the king related to the action
    */
  case class CaptureAction(override val from: (ChessPiece, BoardPosition),
                           override val ambiguity: AmbiguityType = AmbiguityTypes.None,
                           override val toPosition: BoardPosition,
                           override val capturedPiece: ChessPiece,
                           override val kingState: KingState = Free) extends ChessCaptureAction

  /**
    * A promotion with capture action of a chess game.
    *
    * @param from the moved piece and its starting position
    * @param ambiguity the type of ambiguity related to the chess move
    * @param toPosition the arrival position
    * @param capturedPiece the captured piece
    * @param promoteRole the promotion role after the capture
    * @param kingState the state of the king related to the action
    */
  case class CapturePromoteAction(override val from: (ChessPiece, BoardPosition),
                                  override val ambiguity: AmbiguityType = AmbiguityTypes.None,
                                  override val toPosition: BoardPosition,
                                  override val capturedPiece: ChessPiece,
                                  override val promoteRole: ChessRole,
                                  override val kingState: KingState = Free) extends ChessCaptureAction with ChessPromotionAction

  /**
    * An en-passant action of a chess game.
    *
    * @param from the moved piece and its starting position
    * @param ambiguity the type of ambiguity related to the chess move
    * @param toPosition the arrival position
    * @param capturedPiece the captured pawn
    * @param kingState the state of the king related to the action
    */
  case class EnPassantAction(override val from: (ChessPiece, BoardPosition),
                             override val ambiguity: AmbiguityType = AmbiguityTypes.None,
                             override val toPosition: BoardPosition,
                             override val capturedPiece: ChessPiece,
                             override val kingState: KingState = Free) extends ChessCaptureAction

  /**
    * A promotion action of a chess game.
    *
    * @param from the moved piece and its starting position
    * @param ambiguity the type of ambiguity related to the chess move
    * @param toPosition the arrival position
    * @param promoteRole the promotion role
    * @param kingState the state of the king related to the action
    */
  case class PromoteAction(override val from: (ChessPiece, BoardPosition),
                           override val ambiguity: AmbiguityType = AmbiguityTypes.None,
                           override val toPosition: BoardPosition,
                           override val promoteRole: ChessRole,
                           override val kingState: KingState = Free) extends ChessMoveAction with ChessPromotionAction

  /**
    * A castling action of a chess game.
    *
    * @param isKingSide {{{true}}} if the castling is short, {{{false}}} otherwise
    * @param kingState the state of the king related to the action
    */
  case class CastlingAction(override val isKingSide: Boolean,
                            override val kingState: KingState = Free) extends ChessCastlingAction

  /**
    * A draw action of a chess game.
    *
    * @param drawReason the reason of the draw ending
    */
  case class DrawAction(override val drawReason: DrawReason) extends ChessDrawAction

  /**
    * A lose action of a chess game.
    *
    * @param loserColor the color of the loser player
    * @param loseReason the reason of the lose ending
    */
  case class LoseAction(override val loserColor: ChessColor,
                        override val loseReason: LoseReason) extends ChessLoseAction

}