package it.unibo.chess.model.history

import it.unibo.chess.model.core.{Board, Piece}

/**
  * Defines a step for a generic board game.
  * It associate an action with the related status of the board.
  *
  * @tparam P the specific type of game piece
  * @tparam A the specific type of game action
  * @tparam B the specific type of game board
  */
trait BoardGameStep[P <: Piece[_], A <: BoardGameAction[P], B <: Board[P]] {

  /**
    * @return the action related to the game step.
    */
  def action: A

  /**
    * @return the board related to the game step.
    */
  def board: B

}
