package it.unibo.chess.model.fen

import it.unibo.chess.model.utilities.ChessUtilities._
import it.unibo.chess.model.utilities.ChessUtilities.FenUtilities._
import it.unibo.chess.exceptions.Fen.BadFEN
import it.unibo.chess.exceptions.Fen.Types._
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.utilities.CharFormatUtilities

import scala.annotation.tailrec
import scala.util.Try

/**
  * Handles the utilities for FEN parser.
  */
object FenParser {

  /**
    * The player's map with association of a [[ChessColor]] with default names.
    */
  private val players: Map[ChessColor, ChessPlayer] = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2"))

  /**
    * Parses the specified input FEN string.
    *
    * @param fen the string providing all the necessary information to instantiate a game from a particular position
    * @return a chess game representing FEN board state.
    */
  def fromFen(fen: String): ChessGame = {

    require(isValidFen(fen), throw BadFEN(s"$fen is not a valid FEN string", InvalidSequence))

    val game: Array[String] = fen.trim.split(" ")
    game match {
      case Array(placement, turn, castling, enPassant, halfMove, fullMove) =>
        ChessGame(
          board = {
            val rowPlacements: Seq[String] = placement.split("/").reverse
            if (rowPlacements.lengthCompare(BOARD_END_INDEX) != 0) throw BadFEN(s"invalid placement, ${rowPlacements.length} rows", InvalidPlacements)
            ChessBoard(parsePlacement(rowPlacements, BOARD_START_INDEX, Map.empty[BoardPosition, ChessPiece]), List.empty)
          },
          players = players,
          currentTurn = turn match {
            case "w" => White
            case "b" => Black
            case _ => throw BadFEN(s"$turn is not a valid player", InvalidActivePlayer)
          },
          flags = ChessFlags(
            castlingAvailability = castling match {
              case "-" => ChessFlagsUtilities.castlingByBoolean(false)
              case ca => ca.foldRight(ChessFlagsUtilities.castlingByBoolean(false)) {
                case cas@(_, cm) => cm + {
                  val color: ChessColor =  if(cas._1.isUpper) White else Black
                  val castlingCol: Int = CharFormatUtilities.letterToInt(if(cas._1.toLower == 'k') CastlingConstants(color).kingCastlingDestination
                  else CastlingConstants(color).queenCastlingDestination)
                  (color, ChessBoardPosition(castlingCol, color.startingRow)) -> true
                }
                case _ => throw BadFEN(s"$castling is not a valid sequence for castling", InvalidCastlingAvailability)
              }
            },
            enPassantTarget = enPassant match {
              case "-" => None
              case sq => Option(ChessBoardPosition(Symbol(sq)))
            },
            halfMove = Try(halfMove.toInt)
              .filter(_ >= 0).getOrElse(throw BadFEN(s"$halfMove is not a valid half move number", InvalidHalfMove)),
            fullMove = Try(fullMove.toInt)
              .filter(_ >= 1).getOrElse(throw BadFEN(s"$fullMove is not a valid full move number", InvalidFullMove)
            )
          )
        )
    }
  }

  /**
    * Generate the FEN string related to the input chess game.
    *
    * @param game the chess game representing the game state to convert
    * @return the FEN string of the specific game.
    */
  def toFen(game: ChessGame): String = {
    val empty: Int = 0
    val pieces = (for (row <- sideLengthIterable.reverse; col <- sideLengthIterable) yield ChessBoardPosition(col,row)).foldLeft("", empty)(
      (acc, pos) => {
        val piece: Option[ChessPiece] = game.board.pieces.get(pos)
        val pieceString: (String, Int) = piece match {
          case Some(p) => if(acc._2 > 0) (acc._2 + FenUtilities.getPiece(p).toString, 0) else (FenUtilities.getPiece(p).toString, 0)
          case None => if(isLastCol(pos.col))((acc._2+1).toString, 0) else ("", empty + 1)
        }
        (acc._1 + pieceString._1 + (if(isLastCol(pos.col) && !isFirstRow(pos.row)) "/" else ""),
          if(isLastCol(pos.col) || pieceString._1 != "") 0 else acc._2 + pieceString._2)
      }
    )._1

    val castle = game.flags.castlingAvailability.map {
      case elem@((_, _), true) => {
        if (elem._1._2.col == BOARD_END_INDEX - 1) {
          if(elem._1._1 == White) "K" else "k"
        } else {
          if(elem._1._1 == White) "Q" else "q"
        }
      }
      case _ => ""
    }.fold("")(_ + _).sortWith(_ < _)

    s"$pieces ${getFenTurn(game.currentTurn)} " + s"$castle ${game.flags.enPassantTarget.getOrElse("-")} " +
      s"${game.flags.halfMove} ${game.flags.fullMove}"
  }

  /**
    * Parses recursively board rows from which generate the FEN string representing board status.
    *
    * @param rowPlacements a structure that collects all board rows
    * @param row the number of row
    * @param rowMap the game board in a specific state
    * @return the game board correctly populated.
    */
  @tailrec
  private def parsePlacement(rowPlacements: Seq[String], row: Int, rowMap : Map[BoardPosition, ChessPiece]): Map[BoardPosition, ChessPiece] = rowPlacements match {
    case h +: t =>
      require(isValidShortFen(h, row), throw BadFEN(s"FEN=$h", InvalidSequence))
      val (column, rowPlaced) = h.foldLeft(1, rowMap) {
        case ((col, rp), c) if c.isDigit =>
          if (((col -1) + c.asDigit) > BOARD_END_INDEX) throw BadFEN(s"invalid placement, incorrect qty in row $h", InvalidPlacements)
          else (col + c.asDigit, rp)
        case ((col, rp), c) =>
          (col+1, rp + (ChessBoardPosition(col, row) -> FenUtilities.getPiece(c)))
      }
      if (column < BOARD_END_INDEX) throw BadFEN(s"invalid placement, incorrect qty in row $h", InvalidPlacements)
      parsePlacement(t, row + 1, rowPlaced)
    case _ => rowMap
  }

}
