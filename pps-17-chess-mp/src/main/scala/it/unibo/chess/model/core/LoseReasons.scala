package it.unibo.chess.model.core

/**
  * Defines all the possible reasons for a chess lose ending.
  */
object LoseReasons {

  /**
    * Represent a lose reason.
    */
  trait LoseReason

  /**
    * A player is under Checkmate.
    */
  case object CheckmateLoss extends LoseReason

  /**
    * A player surrendered.
    */
  case object SurrenderLoss extends LoseReason

  /**
    * A player's time expired.
    */
  case object TimeOutLoss extends LoseReason

  /**
    * A player disconnected (Multiplayer only).
    */
  case object DisconnectLoss extends LoseReason

}
