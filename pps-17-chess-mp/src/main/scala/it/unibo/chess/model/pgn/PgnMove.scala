package it.unibo.chess.model.pgn

/**
  * Represents a PGN move.
  */
sealed trait PgnMove {

  /**
    * The move expressed in Standard Algebraic Notation.
    */
  val san: String

  /**
    * The comments associated to the move.
    */
  val comments: Seq[String]

}

/**
  * The companion object of [[PgnMove]].
  * It handles the factory for a PGN move.
  */
object PgnMove {

  /**
    * Creates a [[PgnMove]].
    *
    * @param san the move expressed in Standard Algebraic Notation
    * @param comments the comments associated to the move
    * @return the PGN move with {{{san}}} and {{{comments}}}.
    */
  def apply(san: String, comments: Seq[String]): PgnMove = PgnMoveImpl(san, comments)

  private case class PgnMoveImpl(override val san: String, override val comments: Seq[String]) extends PgnMove

}
