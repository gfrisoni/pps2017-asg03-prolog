package it.unibo.chess.model.core

import it.unibo.chess.model.utilities.ChessUtilities.{BOARD_END_INDEX, BOARD_START_INDEX}

/**
  * Represents a chess color.
  */
sealed trait ChessColor {

  /**
    * Returns the opposite of the given color, White or Black.
    */
  val unary_! : ChessColor

  /**
    * The starting row.
    */
  val startingRow: Int

  /**
    * The pawn starting line.
    */
  val pawnLine: Int

  /**
    * The pawn final/promotion line.
    */
  val promotionLine: Int

  /**
    * The pawn en-passant line.
    */
  val enPassantFrom: Int

  /**
    * The castling constants container.
    */
  val castling: Castling

}

case object White extends ChessColor {
  override val unary_! : ChessColor = Black
  override val startingRow: Int = BOARD_START_INDEX
  override val pawnLine: Int = BOARD_START_INDEX + 1
  override val promotionLine: Int = BOARD_END_INDEX
  override val enPassantFrom: Int = BOARD_END_INDEX - 3
  override val castling: Castling = CastlingConstants(this)
}

case object Black extends ChessColor {
  override val unary_! : ChessColor = White
  override val startingRow: Int = BOARD_END_INDEX
  override val pawnLine: Int = BOARD_END_INDEX - 1
  override val promotionLine: Int = BOARD_START_INDEX
  override val enPassantFrom: Int = BOARD_START_INDEX + 3
  override val castling: Castling = CastlingConstants(this)
}