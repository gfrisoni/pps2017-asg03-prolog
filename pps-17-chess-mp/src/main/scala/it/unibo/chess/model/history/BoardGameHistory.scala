package it.unibo.chess.model.history

import it.unibo.chess.model.core.Piece
import it.unibo.chess.model.history.notation.BoardGameActionSerializer
import net.liftweb.json.Extraction.decompose
import net.liftweb.json.{DefaultFormats, compactRender}

/**
  * Represents the history for a generic board game.
  *
  * @tparam P the specific type of game piece
  * @tparam A the specific type of game action
  * @tparam G the specific type of board game step
  * @tparam S the specific type of serializer to use
  */
trait BoardGameHistory[P <: Piece[_], A <: BoardGameAction[P], G <: BoardGameStep[P, _ <: A, _], S <: BoardGameActionSerializer[A]] {

  implicit val formats: DefaultFormats.type = net.liftweb.json.DefaultFormats

  /**
    * @return the game step serializer to use.
    */
  def serializer: S

  /**
    * @return the sequence of steps performed since the beginning of the game.
    */
  def gameSteps: Seq[G]

  /**
    * @return the json representation of the serialized game steps.
    */
  def toJson: String = compactRender(decompose(gameSteps.map(g => serializer.serializeGameAction(g.action))))

}