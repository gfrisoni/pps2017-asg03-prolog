package it.unibo.chess.model.history

import it.unibo.chess.model.core.{ChessBoard, ChessPiece}
import it.unibo.chess.model.history.ChessActions.ChessAction

/**
  * Represents a step for a chess game.
  *
  * @tparam A the specific type of chess game action
  */
sealed trait ChessGameStep[A <: ChessAction] extends BoardGameStep[ChessPiece, A, ChessBoard]

/**
  * The companion object of [[ChessGameStep]].
  * It handles factories for chess game steps.
  */
object ChessGameStep {

  /**
    * Creates a new step of a chess game.
    *
    * @param action the chess action that has been accomplished
    * @param board the chess board related to the game step
    * @tparam A the specific type of chess game action
    * @return the new chess game step.
    */
  def apply[A <: ChessAction](action: A, board: ChessBoard): ChessGameStep[A] = ChessGameStepImpl(action, board)

  private case class ChessGameStepImpl[A <: ChessAction](override val action: A,
                                                         override val board: ChessBoard) extends ChessGameStep[A]

}