package it.unibo.chess.model.core

/**
  * Defines the types of ambiguity that can occur for a move action in a chess game.
  */
object AmbiguityTypes {

  /**
    * Represents a type of ambiguity.
    */
  sealed trait AmbiguityType

  /**
    * Absence of ambiguity.
    */
  case object None extends AmbiguityType

  /**
    * The ambiguity that occur when more pieces on the same file can perform the action.
    */
  case object SameFileAmbiguity extends AmbiguityType

  /**
    * The ambiguity that occur when more pieces on the same rank can perform the action.
    */
  case object SameRankAmbiguity extends AmbiguityType

  /**
    * The ambiguity that occur when more pieces on different files and ranks can perform the action.
    */
  case object CompleteAmbiguity extends AmbiguityType

}
