package it.unibo.chess.model.logic

/**
  * The possible scenario of a subsequent position analysis based on a piece moving toward a direction.
  */
sealed trait PathControl

/**
  * The selected position will not be included, no more positions on that direction will be evaluated.
  */
case object Stop extends PathControl

/**
  * The selected position will be included, however no more positions on that direction will be evaluated.
  */
case object IncludeAndStop extends PathControl

/**
  * The selected position will be included and the evaluation will continue on that direction.
  */
case object Continue extends PathControl

