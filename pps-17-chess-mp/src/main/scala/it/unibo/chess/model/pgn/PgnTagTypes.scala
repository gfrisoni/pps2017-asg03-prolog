package it.unibo.chess.model.pgn

import it.unibo.chess.model.utilities.StringUtilities._
import it.unibo.chess.model.utilities.RegexUtilities._
import it.unibo.chess.model.utilities.ChessUtilities.FenUtilities._

import scala.util.matching.Regex

/**
  * Handles the supported types of PGN tags.
  * @see [[http://www.saremba.de/chessgml/standards/pgn/pgn-complete.htm]] for the PGN guide assumed as reference.
  */
object PgnTagTypes {

  /**
    * Represents the type of a PGN tag.
    */
  sealed trait PgnTagType {

    /**
      * The name of the PGN tag's type.
      */
    lazy val name: String = toString

    /**
      * Whether or not the PGN tag's type belongs to the list managed by the application.
      */
    val isUnknown: Boolean = false

    /**
      * The optional format validator to apply on the values related to the PGN tag's type.
      */
    val format: Option[String => Boolean] = None

  }

  /**
    * The name of the tournament or match event.
    */
  case object Event extends PgnTagType

  /**
    * The location of the event.
    */
  case object Site extends PgnTagType

  /**
    * The starting date of the game.
    */
  case object Date extends PgnTagType {
    override val format: Option[String => Boolean] = Some(date => date.isValidDateTime(datePattern))
  }

  /**
    * The starting time of the game.
    */
  case object Time extends PgnTagType {
    override val format: Option[String => Boolean] = Some(time => time.isValidDateTime(timePattern))
  }

  /**
    * The playing round ordinal of the game.
    */
  case object Round extends PgnTagType {
    override val format: Option[String => Boolean] = Some(round => roundRegex matches round)
  }

  /**
    * The player of the white pieces.
    */
  case object White extends PgnTagType

  /**
    * The player of the black pieces.
    */
  case object Black extends PgnTagType

  /**
    * The white player type.
    */
  case object WhiteType extends PgnTagType {
    override val format: Option[String => Boolean] = Some(playerType => playerTypeRegex matches playerType)
  }

  /**
    * The black player type.
    */
  case object BlackType extends PgnTagType {
    override val format: Option[String => Boolean] = Some(playerType => playerTypeRegex matches playerType)
  }

  /**
    * The result of the game.
    */
  case object Result extends PgnTagType {
    override val format: Option[String => Boolean] = Some(result => resultRegex matches result)
  }

  /**
    * The reason for the conclusion of the game.
    */
  case object Termination extends PgnTagType

  /**
    * The Forsyth-Edwards Notation for the starting position used in the game.
    */
  case object Fen extends PgnTagType {
    override val format: Option[String => Boolean] = Some(fen => isValidFen(fen))
  }

  /**
    * An unknown PGN tag's type.
    *
    * @param n the name of the unknown type
    */
  case class Unknown(n: String) extends PgnTagType {
    override def toString: String = n
    override val isUnknown = true
  }

  private val datePattern: String = "yyyy.MM.dd"
  private val timePattern: String = "HH:mm:ss"
  private val roundRegex: Regex = """-|\d(\.\d)*""".r
  private val playerTypeRegex: Regex = "human|program".r
  private val resultRegex: Regex = """\*|1/2-1/2|½-½|0-1|1-0""".r

  /**
    * All the supported PGN tag types.
    */
  val pgnTagTypes: Set[PgnTagType] = Set(Event, Site, Date, Time, Round, White, Black,
    WhiteType, BlackType, Result, Termination, Fen)

  /**
    * The PGN tag types that compose the Seven Tag Roster.
    */
  val sevenTagRoster: Seq[PgnTagType] = Seq(Event, Site, Date, Round, White, Black, Result)

  /**
    * The PGN tag types of the Seven Tag Roster with their order number.
    */
  val sevenTagRosterIndexes: Map[PgnTagType, Int] = sevenTagRoster.zipWithIndex.toMap

  /**
    * Searches a supported PGN tag type with the specified name.
    *
    * @param name the name of the PGN tag type to research
    * @return the specific PGN tag type if supported, or an unknown type otherwise.
    */
  def getPgnTagTypeFromName(name: String): PgnTagType = pgnTagTypes
    .find(t => t.name.toLowerCase() == name.toLowerCase())
    .getOrElse(Unknown(name))

}