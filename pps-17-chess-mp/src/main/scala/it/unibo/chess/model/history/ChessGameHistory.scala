package it.unibo.chess.model.history

import it.unibo.chess.model.core.ChessPiece
import it.unibo.chess.model.history.ChessActions.{ChessAction, ChessEndingAction}
import it.unibo.chess.model.history.notation.{AlgebraicNotationGameActionSerializer, ChessGameActionSerializer}
import it.unibo.chess.model.utilities.ChessUtilities.AnUtilities._

/**
  * Represents the history for a chess game.
  */
sealed trait ChessGameHistory extends BoardGameHistory[ChessPiece, ChessAction, ChessGameStep[_ <: ChessAction], ChessGameActionSerializer] {

  /**
    * @return the number of chess moves inside the history.
    */
  def movesNumber: Int

  /**
    * @return the chess game result registered inside the history, if present.
    */
  def gameResult: Option[_ <: ChessEndingAction]

}

/**
  * The companion object of [[ChessGameHistory]].
  * It handles factories for chess board histories.
  */
object ChessGameHistory {

  /**
    * Creates a new [[ChessGameHistory]].
    *
    * @param gameSteps the sequence of steps performed since the beginning of the chess game
    * @param serializer the chess game step serializer to use
    * @return the new [[ChessGameHistory]].
    */
  def apply(gameSteps: Seq[ChessGameStep[_ <: ChessAction]] = Seq.empty,
            serializer: ChessGameActionSerializer = AlgebraicNotationGameActionSerializer(
              STANDARD_AN_RULES
            )): ChessGameHistory = ChessGameHistoryImpl(gameSteps, serializer)

  /**
    * Creates a new [[ChessGameHistory]] from an existing one.
    *
    * @param history the original history to copy
    * @param newGameSteps the game steps of the new history
    * @param newSerializer the game step serializer of the new history
    * @return the new [[ChessGameHistory]].
    */
  def copy(history: ChessGameHistory)
          (newGameSteps: Seq[ChessGameStep[_ <: ChessAction]] = history.gameSteps,
           newSerializer: ChessGameActionSerializer = history.serializer): ChessGameHistory = {
    ChessGameHistoryImpl(newGameSteps, newSerializer)
  }

  private case class ChessGameHistoryImpl(override val gameSteps: Seq[ChessGameStep[_ <: ChessAction]],
                                          override val serializer: ChessGameActionSerializer) extends ChessGameHistory {

    override def movesNumber: Int = gameSteps.filterNot(_.action.isInstanceOf[ChessEndingAction]).size

    override def gameResult: Option[_ <: ChessEndingAction] = {
      gameSteps.map(_.action).find(_.isInstanceOf[ChessEndingAction]).map(_.asInstanceOf[ChessEndingAction])
    }

  }

}