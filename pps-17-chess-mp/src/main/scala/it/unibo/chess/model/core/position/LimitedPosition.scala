package it.unibo.chess.model.core.position

import it.unibo.chess.exceptions.Board.BoardException
import it.unibo.chess.exceptions.Board.Types.OutOfBoard

import scala.util.{Failure, Success, Try}

/**
  * A limited position.
  * It excludes some movements and return a new limited position after each one,
  * in order to realize a chain of validations.
  *
  * @tparam P the specific type of limited position to work with
  */
trait LimitedPosition[P <: Position[P] with LimitedPosition[P]] extends Position[P] {

  /**
    * The validator to use in order to set movement limits.
    *
    * @param col the column coordinate
    * @param row the row coordinate
    * @return {{{true}}} if the coordinates are valid, {{{false}}} otherwise.
    */
  def validate(col: Int, row: Int): Boolean

  require(this validate(this.col, this.row), throw BoardException(s"Cannot instantiate a position ($col, $row) outside the board", OutOfBoard))

  abstract override def ^(n: Int): Option[P] = Try(super.^(n)) match { case Success(s) => s; case Failure(f) => None }
  abstract override def >(n: Int): Option[P] = Try(super.>(n)) match { case Success(s) => s; case Failure(f) => None }
  abstract override def v(n: Int): Option[P] = this ^ (n * -1)
  abstract override def <(n: Int): Option[P] = this > (n * -1)

}
