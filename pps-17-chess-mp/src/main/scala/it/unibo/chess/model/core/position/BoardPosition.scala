package it.unibo.chess.model.core.position

import it.unibo.chess.model.utilities.CharFormatUtilities

/**
  * Handles a position inside a limited square board.
  *
  * @param col the x-coordinate
  * @param row the y-coordinate
  * @param validValues the inclusive range for the board's coordinates
  */
class BoardPosition(override val col: Int, override val row: Int)(val validValues: Range.Inclusive)
  extends GridPosition[BoardPosition] with LimitedPosition[BoardPosition] {

  override def fac(x: Int, y: Int): BoardPosition = new BoardPosition(x, y)(validValues)

  override def validate(col: Int, row: Int): Boolean = { validValues.contains(col) && validValues.contains(row) }

  def canEqual(other: Any): Boolean = other.isInstanceOf[BoardPosition]

  override def equals(other: Any): Boolean = other match {
    case that: BoardPosition =>
      (that canEqual this) &&
        col == that.col &&
        row == that.row &&
        validValues == that.validValues
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(col, row, validValues)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  override def toString: String = CharFormatUtilities.intToLetter(col) + row

}