package it.unibo.chess.model.core

import it.unibo.chess.exceptions.Castling.InvalidCastlingException
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.utilities.CharFormatUtilities

/**
  * Represents the utilities related to chess castling.
  */
sealed trait Castling {

  /**
    * Value for strategy pattern, the row where castling is possible.
    */
  val row: Int

  /**
    * The position where the queen side castling is allowed.
    */
  val queenCastlingDestination: String

  /**
    * The position where the king side castling is allowed.
    */
  val kingCastlingDestination: String

  /**
    * King starting position.
    */
  val kingPosition: BoardPosition

  /**
    * King's possible destinations on castling.
    */
  val kingDestinations: Set[List[BoardPosition]]

  /**
    * Position of the left rook before the castling.
    */
  val leftRookPosition: BoardPosition

  /**
    * Position of the right rook before the castling.
    */
  val rightRookPosition: BoardPosition

  /**
    * Destination of the left rook after the castling.
    */
  val leftRookDestination: BoardPosition

  /**
    * Destination of the right rook after the castling.
    */
  val rightRookDestination: BoardPosition

  /**
    * Utility method to see which side the king is attempting the castling.
    *
    * @param kingTargetCol King's desired destination
    * @return {{{true}}} if on king side, {{{false}}} otherwise.
    */
  def isKingCastling(kingTargetCol: Int): Boolean

}

/**
  * Handles the utilities related to chess castling of the player with the specified color.
  *
  * @param color the [[ChessColor]] to consider
  */
case class CastlingConstants(color: ChessColor) extends Castling {

  override val row: Int = color.startingRow
  override val queenCastlingDestination: String = "c"
  override val kingCastlingDestination: String = "g"

  override val kingPosition: BoardPosition = ChessBoardPosition("e" + row)
  override val kingDestinations: Set[List[BoardPosition]] = Set(
    List(ChessBoardPosition(queenCastlingDestination + row)),
    List(ChessBoardPosition(kingCastlingDestination + row))
  )
  override val leftRookPosition: BoardPosition = ChessBoardPosition("a" + row)
  override val rightRookPosition: BoardPosition = ChessBoardPosition("h" + row)
  override val leftRookDestination: BoardPosition = ChessBoardPosition("d" + row)
  override val rightRookDestination: BoardPosition = ChessBoardPosition("f" + row)

  override def isKingCastling(kingTargetCol: Int): Boolean = CharFormatUtilities.intToLetter(kingTargetCol) match {
    case `queenCastlingDestination` => false
    case `kingCastlingDestination` => true
    case _ => throw InvalidCastlingException("Castling not possible in the specified column")
  }

}
