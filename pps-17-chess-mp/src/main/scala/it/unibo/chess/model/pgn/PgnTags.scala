package it.unibo.chess.model.pgn

import it.unibo.chess.model.pgn.PgnTagTypes._
import it.unibo.chess.model.utilities.StringUtilities._

import scala.util.matching.Regex
import com.github.nscala_time.time.Imports._

/**
  * Represents a collection of PGN tags.
  */
sealed trait PgnTags {

  /**
    * @return all the registered PGN tags.
    */
  def getTags: Seq[PgnTag]

  /**
    * Searches the value for the specified PGN tag's type inside the collection.
    *
    * @param tagType the PGN tag's type to research
    * @return the value if a PGN tag with {{{tagType}}} is present, {{{None}}} otherwise.
    */
  def getTagValue(tagType: PgnTagType): Option[String]

  /**
    * Checks if a PGN tag with a certain type exists inside the collection.
    *
    * @param tagType the PGN tag's type to research
    * @return {{{true}}} if the PGN tag is present, {{{false}}} otherwise.
    */
  def exists(tagType: PgnTagType): Boolean

  /**
    * Adds a new PGN tag to the collection.
    *
    * @param tag the PGN tag to insert
    * @return the new collection after {{{tag}}} insertion.
    */
  def +(tag: PgnTag): PgnTags

  /**
    * Adds new PGN tags to the collection.
    *
    * @param tags the PGN tags to insert
    * @return the new collection after {{{tag}}} insertion.
    */
  def ++(tags: PgnTags): PgnTags

  /**
    * @return {{{true}}} if the PGN tags of the seven roster are present, {{{false}}} otherwise.
    */
  def containsSevenTagRoster: Boolean

  /**
    * @return the collection with the PGN tags of the seven roster ordered as desired by the standard.
    * @see http://www.saremba.de/chessgml/standards/pgn/pgn-complete.htm#c8.1.1
    */
  def sorted: PgnTags

  /**
    * @return the date time of the game if the Date PGN tag is present, {{{None}}} otherwise.
    */
  def date: Option[DateTime]

  /**
    * @return the year of the game if the Date PGN tag is present, {{{None}}} otherwise.
    */
  def year: Option[Int]

}

/**
  * Handles the utilities for a collection of PGN tags.
  * It offers factories and some useful constants.
  */
object PgnTags {

  /**
    * Creates a new collection of [[PgnTag]].
    *
    * @param tags the PGN tags of the collection
    * @return the collection of tags.
    */
  def apply(tags: Seq[PgnTag] = Seq.empty): PgnTags = PgnTagsImpl(tags)

  private case class PgnTagsImpl(tags: Seq[PgnTag] = Seq.empty) extends PgnTags {

    override def getTags: Seq[PgnTag] = tags

    override def getTagValue(tagType: PgnTagType): Option[String] = {
      tags.find(_.tagType == tagType).map(_.value)
    }

    override def exists(tagType: PgnTagType): Boolean = tags.exists(_.tagType == tagType)

    override def +(tag: PgnTag): PgnTags = PgnTags(tags.filterNot(_.tagType == tag.tagType) :+ tag)

    override def ++(tags: PgnTags): PgnTags = tags.getTags.foldLeft(PgnTags(this.getTags))(_ + _)

    override def containsSevenTagRoster: Boolean = sevenTagRoster.toSet subsetOf tags.map(_.tagType).toSet

    override def sorted: PgnTags = copy(
      tags = tags.sortBy(tag =>
        PgnTagTypes.sevenTagRosterIndexes.getOrElse(tag.tagType, PgnTagTypes.sevenTagRosterIndexes.values.max)
      )
    )

    override def date: Option[DateTime] = getTagValue(Date).fold[Option[DateTime]](None)(d =>
      Some(DateTime.parse(d, DateTimeFormat.forPattern("yyyy.MM.dd")))
    )

    override def year: Option[Int] = getTagValue(Date) flatMap {
      case PgnTags.DateRegex(y, _, _) => y.toIntOpt
      case _ => None
    }

    override def toString: String = tags.mkString(sys.props("line.separator"))

  }

  private val DateRegex: Regex = """(\d{4}|\?{4})\.(\d\d|\?.\?)\.(\d\d|\?\?)""".r

}