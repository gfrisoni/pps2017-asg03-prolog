package it.unibo.chess.model.utilities

import scala.util.matching.Regex

/**
  * Handles the utilities for regex handling.
  */
object RegexUtilities {

  /**
    * Handy matcher implicit for strings.
    *
    * @param regex the resulting regex.
    */
  implicit class RichRegex(val regex: Regex) extends AnyVal {
    def matches(s: String): Boolean = regex.pattern.matcher(s).matches
  }

}
