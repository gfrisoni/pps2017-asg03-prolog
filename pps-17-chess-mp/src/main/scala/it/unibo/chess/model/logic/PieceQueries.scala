package it.unibo.chess.model.logic

import it.unibo.chess.exceptions.Role.InvalidRoleAssigned
import it.unibo.chess.model.core.ChessRoles.Pawn
import it.unibo.chess.model.core.KingStates.Free
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}

import scala.language.reflectiveCalls

/**
  * Queries to determine the PathControl.
  */
object PieceQueries {

  /**
    * A Chess PieceQuery interface.
    */
  sealed trait ChessPieceQuery {

    /**
      * The piece taken into account.
      */
    val piece: ChessPiece

    /**
      * Method used to apply the query based on the game and the given position.
      *
      * @param game the environment of the move
      * @param target the analyzed position
      * @return the PathControl that determines if the position will be considered.
      */
    def applyQuery(game: ChessGame, target: BoardPosition): PathControl
  }

  /**
    * Abstract class with targets and evaluation color-dependant.
    */
  abstract class PathByTargetWithColorQuery extends ChessPieceQuery {

    def sameColor: () => PathControl
    def differentColor: () => PathControl
    def free: (ChessGame, BoardPosition) => PathControl

    override def applyQuery(game: ChessGame, target: BoardPosition): PathControl = {
      game.board.pieces
        .get(target)
        .map(_.color)
        .map(col => if (col == piece.color) sameColor() else differentColor())
        .getOrElse(free(game, target))
    }

  }

  /**
    * Abstract class with targets but no color dependencies.
    */
  abstract class PathByTargetQuery extends PathByTargetWithColorQuery {

    def occupied: () => PathControl

    override def sameColor: () => PathControl = occupied
    override def differentColor: () => PathControl = occupied

  }

  /**
    * A query for a generic piece, not a pawn.
    *
    * @param piece the piece taken into account
    */
  case class GenericPieceLimitQuery(override val piece: ChessPiece) extends PathByTargetWithColorQuery {

    require(!piece.role.isInstanceOf[Pawn], throw InvalidRoleAssigned("Cannot assign a generic query to a Pawn"))

    override def sameColor: () => PathControl = () => Stop
    override def differentColor: () => PathControl = () => IncludeAndStop
    override def free: (ChessGame, BoardPosition) => PathControl = (_,_) => Continue

  }

  /**
    * A query for a forward moving Pawn.
    *
    * @param piece the Pawn moving
    */
  case class PawnForwardLimitQuery(override val piece: ChessPiece) extends PathByTargetQuery {

    require(piece.role.isInstanceOf[Pawn], throw InvalidRoleAssigned("Cannot assign a pawn query to any other Role"))

    override def occupied: () => PathControl = () => Stop
    override def free: (ChessGame, BoardPosition) => PathControl = (_,_) => Continue

  }

  /**
    * A query for a diagonal moving Pawn.
    *
    * @param piece the Pawn moving
    */
  case class PawnDiagonalLimitQuery(override val piece: ChessPiece) extends PathByTargetWithColorQuery {

    require(piece.role.isInstanceOf[Pawn], throw InvalidRoleAssigned("Cannot assign a pawn query to any other Role"))

    override def sameColor: () => PathControl = () => Stop
    override def differentColor: () => PathControl = () => IncludeAndStop
    override def free: (ChessGame, BoardPosition) => PathControl = (game, target) => {
      if((game.board pieceAt ChessBoardPosition(target.col, piece.color.enPassantFrom)).exists(_.color == !piece.color) &&
      game.flags.enPassantTarget.fold(false)(_ == target)
      ) IncludeAndStop else Stop
    }

  }

  /**
    * Query for a King attempting a castling.
    *
    * @param piece a King
    */
  case class CastleQuery(override val piece: ChessPiece) extends ChessPieceQuery {

    override def applyQuery(game: ChessGame, target: BoardPosition): PathControl = {
      val kingPosition = piece.color.castling.kingPosition
      val rookPosition = if (target.col < kingPosition.col) piece.color.castling.leftRookPosition else piece.color.castling.rightRookPosition
      if (!game.flags.canCastle(piece.color,target)) return Stop
      for (col <- Math.min(kingPosition.col, rookPosition.col) until Math.max(kingPosition.col, rookPosition.col)
           if col != Math.min(kingPosition.col, rookPosition.col)) {
        if (game.board.pieces.contains(ChessBoardPosition(col, kingPosition.row)) || (
          ChessGame(
            game.board move kingPosition to ChessBoardPosition(col, kingPosition.row),
            game.players,
            game.currentTurn,
            game.history
          ).kingStateOf(piece.color) != Free)) return Stop
      }
      IncludeAndStop
    }

  }

}