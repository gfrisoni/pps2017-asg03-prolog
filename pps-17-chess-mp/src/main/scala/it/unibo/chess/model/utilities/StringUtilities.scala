package it.unibo.chess.model.utilities

import org.joda.time.{DateTime, DateTimeZone}
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}

import scala.util.{Failure, Success, Try}

/**
  * The utilities for string handling.
  */
object StringUtilities {

  /**
    * Adding more functionalities to our strings.
    *
    * @param s the RichString with more methods
    */
  implicit class RichString(val s: String) {

    /**
      * Converts a string to an Option[Int] if valid.
      *
      * @return the optional number.
      */
    def toIntOpt: Option[Int] = {
      try {
        Some(s.toInt)
      } catch {
        case _: NumberFormatException => None
      }
    }

    /**
      * Concatenates two strings if the condition is valid.
      *
      * @param other the string to concatenate
      * @param condition the condition to be evaluated
      * @return the string resulting from the concatenation of {{{other}}}.
      */
    def concatIf(other: String)(condition: Boolean): String = {
      if (condition) {
        s concat other
      } else {
        s
      }
    }

    /**
      * Checks whether a date in a string format is properly composed.
      *
      * @param pattern the requested datetime pattern
      * @return {{{true}}} if the string is valid for {{{pattern}}}.
      */
    def isValidDateTime(pattern: String): Boolean = {
      val formatter: DateTimeFormatter = DateTimeFormat forPattern pattern withZone DateTimeZone.UTC
      Try[DateTime](formatter parseDateTime s) match {
        case Success(_) => true
        case Failure(_) => false
      }
    }

  }

}
