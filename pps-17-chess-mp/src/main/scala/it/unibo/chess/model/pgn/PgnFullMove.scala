package it.unibo.chess.model.pgn

/**
  * Represents a chess game full move inside a PGN.
  */
sealed trait PgnFullMove {

  /**
    * The SAN representation of the white move.
    */
  val whiteSanMove: String

  /**
    * The SAN representation of the black move.
    */
  val blackSanMove: Option[String]

}

/**
  * The companion object of [[PgnFullMove]].
  * It handles the factory for a PGN full move.
  */
object PgnFullMove {

  /**
    * Creates a new [[PgnFullMove]].
    *
    * @param whiteSanMove the SAN representation of the white move
    * @param blackSanMove the SAN representation of the black move
    * @return the new [[PgnFullMove]].
    */
  def apply(whiteSanMove: String,
            blackSanMove: Option[String] = None): PgnFullMove = PgnFullMoveImpl(whiteSanMove, blackSanMove)

  private case class PgnFullMoveImpl(override val whiteSanMove: String,
                                     override val blackSanMove: Option[String]) extends PgnFullMove

}