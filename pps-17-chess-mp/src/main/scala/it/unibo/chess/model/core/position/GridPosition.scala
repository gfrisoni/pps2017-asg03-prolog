package it.unibo.chess.model.core.position

/**
  * This abstract class defines the behaviour of a generic grid position.
  * It requires the definition of a factory function in order to instantiate a specific type of grid position.
  *
  * @tparam P the specific type of grid position to work with
  */
abstract class GridPosition[P <: GridPosition[P]] extends Position[P] {

  /**
    * The factory to use for the instance of a [[GridPosition]].
    *
    * @param x the x-coordinate
    * @param y the y-coordinate
    * @return the [[GridPosition]] related to {{{x}}} and {{{y}}}.
    */
  def fac(x: Int, y: Int): P

  override def ^(n: Int): Option[P] = Some(fac(col, row + n))
  override def >(n: Int): Option[P] = Some(fac(col + n, row))
  override def v(n: Int): Option[P] = this ^ (n * -1)
  override def <(n: Int): Option[P] = this > (n * -1)

  private[this] def expand(i: Int, accumulator: List[Option[P]], direction: Option[P] => Option[P]): List[Option[P]] = {
    if (i > 0 && accumulator.head.isDefined) expand(i-1, direction(accumulator.head) :: accumulator, direction) else accumulator
  }

  override def ^^(n: Int): List[P] = expand(n, List(Some(fac(col, row))), p => p.flatMap(_ ^ 1)).filter(_.isDefined).map(_.get).reverse
  override def >>(n: Int): List[P] = expand(n, List(Some(fac(col, row))), p => p.flatMap(_ > 1)).filter(_.isDefined).map(_.get).reverse
  override def vv(n: Int): List[P] = expand(n, List(Some(fac(col, row))), p => p.flatMap(_ v 1)).filter(_.isDefined).map(_.get).reverse
  override def <<(n: Int): List[P] = expand(n, List(Some(fac(col, row))), p => p.flatMap(_ < 1)).filter(_.isDefined).map(_.get).reverse

}

/**
  * A position on an unlimited grid.
  *
  * @param col the x-coordinate
  * @param row the y-coordinate
  */
case class GridPositionImpl(override val col: Int, override val row: Int) extends GridPosition[GridPositionImpl] {

  override def fac(x: Int, y: Int) = GridPositionImpl(x, y)

}