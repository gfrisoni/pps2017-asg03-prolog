package it.unibo.chess.model.pgn

import it.unibo.chess.model.core.ChessColor

/**
  * Represents an index for a PGN history.
  */
sealed trait PgnMoveIndex {

  /**
    * The full-turn index.
    */
  val turnIndex: Int

  /**
    * The player index.
    */
  val playerIndex: ChessColor

}

/**
  * The companion object of [[PgnMoveIndex]].
  * It handles the factory for a PGN history index.
  */
object PgnMoveIndex {

  /**
    * Creates a new [[PgnMoveIndex]].
    *
    * @param turnIndex the full-turn index
    * @param playerIndex the player index
    * @return the new [[PgnMoveIndex]].
    */
  def apply(turnIndex: Int,
            playerIndex: ChessColor): PgnMoveIndex = PgnMoveIndexImpl(turnIndex, playerIndex)

  private case class PgnMoveIndexImpl(override val turnIndex: Int,
                                      override val playerIndex: ChessColor) extends PgnMoveIndex

}