package it.unibo.chess.model.modes

import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Handles the supported chess modes.
  */
object ChessModes {

  /**
    * Represents a generic chess mode.
    */
  trait ChessMode {

    /**
      * The name of the game mode to be displayed.
      */
    val name: String

    /**
      * Timer behaviour at the current color tick.
      *
      * @return the updated time.
      */
    def currentColorTick(): Option[FiniteDuration]

    /**
      * Timer behaviour at the opposite color tick.
      *
      * @return the updated time.
      */
    def oppositeColorTick(): Option[FiniteDuration]

    /**
      * Timer behaviour at the current color switch.
      *
      * @return the updated time.
      */
    def currentColorSwitch(): Option[FiniteDuration]

    /**
      * Timer behaviour at the opposite color switch.
      *
      * @return the updated time.
      */
    def oppositeColorSwitch(): Option[FiniteDuration]

    /**
      * Our toString with the custom name.
      *
      * @return the name of the chess mode.
      */
    override def toString: String = name
  }

  case object SuddenDeath extends ChessMode {

    override val name: String = "Sudden Death"

    override def currentColorTick(): Option[FiniteDuration] = Some(-1.seconds)
    override def oppositeColorTick(): Option[FiniteDuration] = None
    override def currentColorSwitch(): Option[FiniteDuration] = None
    override def oppositeColorSwitch(): Option[FiniteDuration] = None

  }

  case object Hourglass extends ChessMode {

    override val name: String = "Hourglass"

    override def currentColorTick(): Option[FiniteDuration] = Some(-1.seconds)
    override def oppositeColorTick(): Option[FiniteDuration] = Some(1.seconds)
    override def currentColorSwitch(): Option[FiniteDuration] = None
    override def oppositeColorSwitch(): Option[FiniteDuration] = None

  }

  case class Compensation(bonus: FiniteDuration) extends ChessMode {

    override val name: String = s"Compensation (+${bonus.toSeconds})"

    override def currentColorTick(): Option[FiniteDuration] = Some(-1.seconds)
    override def oppositeColorTick(): Option[FiniteDuration] = None
    override def currentColorSwitch(): Option[FiniteDuration] = Some(bonus)
    override def oppositeColorSwitch(): Option[FiniteDuration] = None

  }

  val chessModes: Seq[ChessMode] = Seq(SuddenDeath, Hourglass, Compensation(30 seconds))

}
