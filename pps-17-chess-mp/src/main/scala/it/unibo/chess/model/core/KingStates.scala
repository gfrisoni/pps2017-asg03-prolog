package it.unibo.chess.model.core

/**
  * Defines all the possible king states during a chess game.
  */
object KingStates {

  /**
    * The states that describe a King's situation.
    */
  sealed trait KingState

  /**
    * King is free to move.
    */
  case object Free extends KingState

  /**
    * King is under check, game is not over yet but something has to be done to get back to free.
    */
  case object Check extends KingState

  /**
    * King is under checkmate, game is over and.
    */
  case object Checkmate extends KingState

  /**
    * King is under Stalemate, game is draw.
    */
  case object Stalemate extends KingState

}
