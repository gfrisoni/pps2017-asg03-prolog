package it.unibo.chess.model.core

import ChessRoles._
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.logic.SideEffects._
import it.unibo.chess.model.logic.PieceQueries._

/**
  * Represents a generic piece.
  *
  * @tparam R the type of the role played by the piece
  */
sealed trait Piece[R <: BoardRole] {

  /**
    * The role of the piece
    */
  val role: R

  /**
    * The color of the piece.
    */
  val color: ChessColor

}

/**
  * Represents a chess piece.
  * It consists of a specific piece with a chess role.
  */
sealed trait ChessPiece extends Piece[ChessRole] {

  /**
    * Calculates all the possible directions reachable by a piece from a given position, on an empty board.
    *
    * @param position the starting position
    * @return a map with each direction and the related possible actions.
    */
  def validatedDirectionsFrom(position: BoardPosition): Map[List[BoardPosition], (ChessPieceQuery, Option[ChessPieceSideEffect])]

}

/**
  * Handles a chess piece, providing factories and utilities.
  */
object ChessPiece {

  /**
    * Creates a new chess piece.
    *
    * @param color the color of the piece
    * @param role the role of the piece
    * @return the new chess piece.
    */
  def apply(color: ChessColor, role: ChessRole): ChessPiece = ChessPieceImpl(color, role)

  /**
    * Divides a chess piece in its components.
    *
    * @param arg the chess piece to unapply
    * @return the components of the chess piece.
    */
  def unapply(arg: ChessPiece): Option[(ChessColor, ChessRole)] = Some(arg.color, arg.role)

  private case class ChessPieceImpl(
                             override val color: ChessColor,
                             override val role: ChessRole
                           ) extends ChessPiece {

    /**
      * Basic structure of the item where the moves will be collected.
      */
    val resultSeed = Map.empty[List[BoardPosition], (ChessPieceQuery, Option[ChessPieceSideEffect])]

    override def validatedDirectionsFrom(position: BoardPosition): Map[List[BoardPosition], (ChessPieceQuery, Option[ChessPieceSideEffect])] = role match {
      case Pawn(_) => (role directionsFrom position).foldLeft(resultSeed) {(acc, listPos) => listPos.head.col match {
        case position.col => acc + (listPos -> (PawnForwardLimitQuery(this), None))
        case _ => acc + (listPos -> (PawnDiagonalLimitQuery(this), Some(EnPassantEffect(this))))
      }
      }
      case King(_) => (role directionsFrom position).foldLeft(resultSeed) {(acc, listPos) => listPos.head.col match {
        case _ if (listPos.head.col == position.col - 2) || (listPos.head.col == position.col + 2) => acc + (listPos -> (CastleQuery(this), Some(CastlingEffect(this))))
        case _ => acc + (listPos -> (GenericPieceLimitQuery(this), None))
      }}
      case _ => (role directionsFrom position).foldLeft(resultSeed) {(acc, listPos) => acc + (listPos -> (GenericPieceLimitQuery(this), None))}
    }

  }

}