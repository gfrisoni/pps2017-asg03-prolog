package it.unibo.chess.model.utilities

import scala.languageFeature.implicitConversions
import it.unibo.chess.exceptions.Board.BoardException
import it.unibo.chess.exceptions.Board.Types.OutOfBoard
import it.unibo.chess.exceptions.Fen.BadFEN
import it.unibo.chess.exceptions.Fen.Types.{InvalidActivePlayer, UnknownPiece}
import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.history._
import it.unibo.chess.model.history.notation._

import scala.util.matching.Regex

/**
  * Handles the utilities for the handling of a chess game.
  */
object ChessUtilities {

  /**
    * The first index of a chess board.
    */
  val BOARD_START_INDEX: Int = 1

  /**
    * The final index of a chess board.
    */
  val BOARD_END_INDEX: Int = 8

  private val COORDINATE_REGEX_PATTERN: Regex = new Regex("^[a-h][1-8]$")

  /**
    * Assuming that the board is a square, returns an iterable for its length.
    *
    * @return {{{BOARD_START_INDEX}}} to {{{BOARD_END_INDEX}}} iterable.
    */
  def sideLengthIterable: Range.Inclusive = BOARD_START_INDEX to BOARD_END_INDEX

  /**
    * Converts a coordinate expressed as symbol into a tuple of two integer coordinates.
    *
    * @param s the symbol to convert
    * @return the tuple coordinate related to {{{s}}}.
    */
  def coordinateToTuple(s: Symbol): (Int,Int) = {
    if (s == null || ChessUtilities.COORDINATE_REGEX_PATTERN.findFirstIn(s.name).isEmpty) throw BoardException("Invalid coordinate: " + s, OutOfBoard)
    (s.name.charAt(0).asDigit - CharFormatUtilities.DIGITS, s.name.charAt(1).asDigit)
  }

  /**
    * Calculates the principal row pieces arrangement for the specified color.
    *
    * @param color the player color
    * @return the principal row pieces arrangement.
    */
  def defaultChessPattern(color: ChessColor): List[ChessRole] = List(Rook, Knight, Bishop, Queen, King(color), Bishop, Knight, Rook)

  /**
    * Handles the utilities related to an en-passant action for a chess game.
    */
  object EnPassantUtilities {

    /**
      * Calculates the position associated to the square with the opponent pawn,
      * according to the color of the player and the target destination of the move.
      *
      * @param color the color of the player
      * @param target the en-passant target position
      * @return the position related to the square that should have the opponent pawn.
      */
    def getOpponentPosition(color: ChessColor, target: BoardPosition): BoardPosition = {
      color match {
        case White => ChessBoardPosition(target.col, target.row - 1)
        case Black => ChessBoardPosition(target.col, target.row + 1)
      }
    }

  }

  /**
    * Handles the utilities for PGN (Portable Game Notation).
    */
  object PgnUtilities {

    /**
      * The number of moves in a PGN full turn.
      */
    val PGN_PERIOD_SIZE: Int = 2

    /**
      * The separator to use between PGN full turns.
      */
    val PGN_PERIOD_SEPARATOR: String = " "

    /**
      * The separator to use between the moves of a PGN full turn.
      */
    val PGN_MOVE_SEPARATOR: String = " "

    /**
      * The separator to use between a full turn index and its moves.
      */
    val PGN_INDEX_SEPARATOR: String = ". "

  }

  /**
    * Handles the utilities for AN (Algebraic Notation).
    */
  object AnUtilities {

    /**
      * The standard figurines supported by Unicode Miscellaneous Symbols and required by FAN.
      */
    def figurine(color: ChessColor, role: ChessRole): String = role match {
      case King(_) => if (color == White) "♔" else "♚"
      case Queen => if (color == White) "♕" else "♛"
      case Rook => if (color == White) "♖" else "♜"
      case Bishop => if (color == White) "♗" else "♝"
      case Knight => if (color == White) "♘" else "♞"
      case Pawn(_) => if (color == White) "♙" else "♟"
    }

    /**
      * The Standard Algebraic Notation (SAN) rules required by FIDE.
      */
    val STANDARD_AN_RULES: AlgebraicNotationRules = AlgebraicNotationRules(
      longNotation = false,
      lowerCaseFiles = true,
      figurine = false,
      CaptureNotations.XCaptureNotation,
      CastlingNotations.ZeroesCastlingNotation,
      PromotionNotations.EqualsPromoteNotation,
      CheckNotations.PlusCheckNotation,
      CheckmateNotations.HashtagCheckmateNotation
    )

    /**
      * Calculates the chess role associated to a role expressed as char and a chess color.
      *
      * @param c the char related to the role
      * @param color the chess color related to the role
      * @return the chess role representation of {{{c}}} and {{{color}}}.
      */
    def getRole(c: Char, color: ChessColor): Option[ChessRole] = {
      c.toLower match {
        case 'r' => Some(Rook)
        case 'n' => Some(Knight)
        case 'b' => Some(Bishop)
        case 'q' => Some(Queen)
        case 'k' => Some(King(color))
        case 'p' => Some(Pawn(color))
        case _ => None
      }
    }

  }

  /**
    * Handles the FEN parser utilities.
    */
  object FenUtilities {

    /**
      * Retrieves the correct chess piece from a character.
      *
      * @param c the character representing chess piece
      * @return the chess piece related to the input char.
      */
    def getPiece(c: Char): ChessPiece = {
      val color: ChessColor = if(c.isUpper) White else Black
      AnUtilities.getRole(c, color).fold(throw BadFEN(s"Piece=$c", UnknownPiece))(ChessPiece(color, _))
    }

    /**
      * Retrieves the related char associated with a specific chess piece.
      *
      * @param p the chess piece to convert
      * @return a char representing {{{p}}}.
      */
    def getPiece(p: ChessPiece): Char = {
      val c = p.role match {
        case Rook => 'r'
        case Knight => 'n'
        case Bishop => 'b'
        case Queen => 'q'
        case King(_) => 'k'
        case Pawn(_) => 'p'
        case _ => throw BadFEN(s"Piece=${p.role}", UnknownPiece)
      }
      if(p.color == White) c.toUpper else c
    }

    /**
      * Retrieves the char representing current game turn.
      *
      * @param currentTurn the current turn in the chess game
      * @return a char related to current turn.
      */
    def getFenTurn(currentTurn: ChessColor): Char = {
      currentTurn match{
        case White => 'w'
        case Black => 'b'
        case _ => throw BadFEN(s"Turn $currentTurn is not a valid", InvalidActivePlayer)
      }
    }

    /**
      * Checks if the input field is the last col of the board.
      *
      * @param col int value representing board column
      * @return {{{true}}} if the input field is the last col, {{{false}}} otherwise.
      */
    def isLastCol(col: Int): Boolean = col == BOARD_END_INDEX

    /**
      * Checks if the input field is the last row of the board.
      *
      * @param row the int value representing the board row.
      * @return {{{true}}} if the input field is the last row, {{{false}}} otherwise.
      */
    def isFirstRow(row: Int): Boolean = row == BOARD_START_INDEX

    /**
      * Checks if the input string represents a valid FEN string.
      *
      * @param fen the fen string to verify.
      * @return {{{true}}} if the input string is a valid FEN, {{{false}}} otherwise.
      */
    def isValidFen(fen: String): Boolean =
      fen.trim.matches(
        """\s*[rnbqkpRNBQKP\/\d]+\s+[wb]\s+([KQkq]{1,4}|\-)\s*[\-abcdefgh12345678]{1,2}\s*[\d]{1,2}\s*[\d]{1,2}\s*""")

    /**
      * Checks if the input string represents a valid short FEN string.
      *
      * @param fen the fen string to verify.
      * @param row the row related to the placement.
      * @return {{{true}}} if the input string is a valid short FEN, {{{false}}} otherwise.
      */
    def isValidShortFen(fen: String, row: Int): Boolean =
      if(row == BOARD_START_INDEX || row == BOARD_END_INDEX) fen.count(_ == (if(row == BOARD_START_INDEX) 'P' else 'p')) == 0
      else fen.trim.matches("""\s*[rnbqp+kRNBQKP+\/\d]+\s*""")

  }

}
