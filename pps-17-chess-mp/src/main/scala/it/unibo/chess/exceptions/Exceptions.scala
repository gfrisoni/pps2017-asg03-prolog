package it.unibo.chess.exceptions

import it.unibo.chess.exceptions.Actor.Types.ActorExceptionType
import it.unibo.chess.exceptions.Board.Types.BoardExceptionType
import it.unibo.chess.exceptions.Database.Types.DatabaseExceptionType
import it.unibo.chess.exceptions.Fen.Types.FenExceptionType
import it.unibo.chess.exceptions.Game.Types.GameExceptionType
import it.unibo.chess.exceptions.Promotion.Types.PromotionExceptionType
import it.unibo.chess.exceptions.Timer.Types.TimerExceptionType

/**
  * The root class of our custom Exceptions.
  *
  * @param message a nice string message to display.
  */
class CustomException(val message: String) extends Exception(message)

/**
  * Simple exception with no subtypes.
  *
  * @param message a nice string message to display.
  */
class UntypedException(override val message: String) extends CustomException(message)

/**
  * Interface for the subtypes of our custom exceptions.
  */
sealed trait CustomExceptionType

/**
  * Composite exception with subtypes.
  *
  * @param message a nice string message to display.
  * @param exceptionType one of the allowed subtypes for this exception.
  */
class TypedException(override val message: String, val exceptionType: CustomExceptionType) extends CustomException(message)

/**
  * Exceptions thrown upon the promotion process.
  */
object Promotion {
  object Types {
    sealed trait PromotionExceptionType extends CustomExceptionType
    case object NotAPawn extends PromotionExceptionType
    case object NotOnPromotionLine extends PromotionExceptionType
    case object EmptyPromotionLine extends PromotionExceptionType
    case object NoPawnOnPosition extends PromotionExceptionType
    case object WrongRole extends PromotionExceptionType
  }
  case class PromotionException(override val message: String, override val exceptionType: PromotionExceptionType) extends TypedException(message, exceptionType)
}

/**
  * Exceptions thrown upon a board process.
  */
object Board {
  object Types {
    sealed trait BoardExceptionType extends CustomExceptionType
    case object OccupiedPosition extends BoardExceptionType
    case object OutOfBoard extends BoardExceptionType
    case object NoPiece extends BoardExceptionType
  }
  case class BoardException(override val message: String, override val exceptionType: BoardExceptionType) extends TypedException(message, exceptionType)
}

/**
  * Exceptions thrown upon the castling process.
  */
object Castling {
  case class InvalidCastlingException(override val message: String) extends UntypedException(message)
}

/**
  * Exceptions thrown upon the game processes.
  */
object Game {
  object Types {
    sealed trait GameExceptionType extends CustomExceptionType
    case object PendingPromotion extends GameExceptionType
    case object WrongTurn extends GameExceptionType
    case object IllegalMove extends GameExceptionType
    case object InvalidPlayersSetup extends GameExceptionType
    case object GameOver extends GameExceptionType
  }
  case class GameException(override val message: String, override val exceptionType: GameExceptionType) extends TypedException(message, exceptionType)
}

/**
  * Exceptions thrown upon the actor actions.
  */
object Actor {
  object Types {
    sealed trait ActorExceptionType extends CustomExceptionType
    case object IllegalMove extends ActorExceptionType
  }
  case class ActorException(override val message: String, override val exceptionType: ActorExceptionType) extends TypedException(message, exceptionType)
}

/**
  * Exceptions thrown upon the Role processes.
  */
object Role {
  case class InvalidRoleAssigned(override val message: String) extends UntypedException(message)
}

/**
  * Exceptions thrown upon the FEN parsing process.
  */
object Fen {
  object Types {
    sealed trait FenExceptionType extends CustomExceptionType
    case object InvalidSequence extends FenExceptionType
    case object InvalidPlacements extends FenExceptionType
    case object InvalidActivePlayer extends FenExceptionType
    case object InvalidCastlingAvailability extends FenExceptionType
    case object InvalidHalfMove extends FenExceptionType
    case object InvalidFullMove extends FenExceptionType
    case object UnknownPiece extends FenExceptionType
  }
  case class BadFEN(override val message: String, override val exceptionType: FenExceptionType) extends TypedException(message, exceptionType)
}

/**
  * Exceptions thrown upon the times processes.
  */
object Timer {
  object Types {
    sealed trait TimerExceptionType extends CustomExceptionType
    case object SwitchOnOwnTurn extends TimerExceptionType
    case object WhiteSwitchedFirst extends TimerExceptionType
    case class TimerException(override val message: String, override val exceptionType: TimerExceptionType) extends TypedException(message, exceptionType)
  }
}

/**
  * Exceptions thrown upon the database processes.
  */
object Database {
  object Types {
    sealed trait DatabaseExceptionType extends CustomExceptionType
    case object UnfinishedGameSavingAttempt extends DatabaseExceptionType
  }
  case class DatabaseException(override val message: String, override val exceptionType: DatabaseExceptionType) extends TypedException(message, exceptionType)
}

/**
  * Exceptions thrown upon the history navigation.
  */
object History {
  object Types {
    sealed trait HistoryExceptionType extends CustomExceptionType
    case object UnavailablePreviousStep extends HistoryExceptionType
    case object UnavailableNextStep extends HistoryExceptionType
    case class HistoryException(override val message: String, override val exceptionType: HistoryExceptionType) extends TypedException(message, exceptionType)
  }
}