package it.unibo.chess

import javafx.application.{Application, Platform}
import javafx.stage.Stage

import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.Intent
import it.unibo.chess.view.{FXWindow, GenericWindow}

class KnightTour extends Application {

  override def start(primaryStage: Stage): Unit = {
      Platform.runLater(() => {
        // Creates the manager for the window
        val view: GenericWindow = FXWindow(primaryStage)
        view.setTitle("Chess MP - Knight Tour")
        view.setScene(new Intent(SceneType.KnightTour))
        view.showView()
    })
  }

}

object KnightTour extends App {
  Application.launch(classOf[KnightTour], args: _*)
}