package it.unibo.chess.controller.traits.scenes

import it.unibo.chess.controller.traits.GenericController
import it.unibo.chess.view.general.observers.TvSceneObserver
import it.unibo.chess.view.general.scenes.TvScene

trait TvSceneController extends TvSceneObserver with GenericController[TvScene]