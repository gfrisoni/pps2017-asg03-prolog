package it.unibo.chess.controller.controllers.game

import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.util.concurrent.atomic.AtomicBoolean

import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.controller.traits.scenes.game.GameSceneController
import it.unibo.chess.controller.utilities.SchedulingUtilities.AdvancedScheduler
import it.unibo.chess.exceptions.History.Types.{HistoryException, UnavailableNextStep, UnavailablePreviousStep}
import it.unibo.chess.model.core.DrawReasons.MutualDraw
import it.unibo.chess.model.core.KingStates._
import it.unibo.chess.model.core.LoseReasons.{DisconnectLoss, SurrenderLoss, TimeOutLoss}
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.fen.FenParser
import it.unibo.chess.model.history.ChessActions._
import it.unibo.chess.model.history.ChessGameStep
import it.unibo.chess.model.pgn.{PgnConverter, PgnMoveIndex}
import it.unibo.chess.model.utilities.ChessUtilities
import it.unibo.chess.view.MessageTypes
import it.unibo.chess.view.general.scenes.GameScene

import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

/**
  * Class that represents a generic controller for a GameScene.
  *
  * @param game the game that will be managed by this controller
  */
abstract class AbstractGameSceneController(override var game: ChessGame) extends GameSceneController {

  // Sets flags
  var gameStepIndex: Option[Int] = None
  private val nextIndexStrategy: Option[Int] => Option[Int] = _.fold(Some(0))(index => Some(index + 1))
  private val isPlaying: AtomicBoolean = new AtomicBoolean(false)

  protected def setGameEnded(endingAction: ChessEndingAction): Unit = {
    // Updates the game step index and the associated view components
    endingAction match {
      case DrawAction(MutualDraw) | LoseAction(_, SurrenderLoss | TimeOutLoss | DisconnectLoss) =>
      case _ => updateGameStepIndex(nextIndexStrategy)
    }
    // Tells the game over status and the history to draw with the current SAN move
    view.foreach(_.setGameEnded(endingAction))
  }

  private def calculateLastMoveFromAction(chessAction: CheckableAction): (BoardPosition, BoardPosition) = {
    chessAction match {
      case move: ChessMoveAction => (move.from._2, move.toPosition)
      case castling: CastlingAction =>
        val castlingUtil: Castling = CastlingConstants(game.currentTurn)
        if (castling.isKingSide) {
          (castlingUtil.kingPosition, ChessBoardPosition(castlingUtil.kingCastlingDestination + castlingUtil.row))
        } else {
          (castlingUtil.kingPosition, ChessBoardPosition(castlingUtil.queenCastlingDestination + castlingUtil.row))
        }
    }
  }

  private def checkHistoryNavigation(): Unit = {
    gameStepIndex match {
      // If there are not previous game steps
      case None =>
        view.foreach(view => {
          view.setFirstGameStepEnabled(false)
          view.setPreviousGameStepEnabled(false)
          // But there are others later
          if (game.history.movesNumber > 0) {
            view.setPlayEnabled(true)
            view.setNextGameStepEnabled(true)
            view.setLastGameStepEnabled(true)
          }
        })
      // If the current move index is not related to the first game step
      case Some(index) =>
        index match {
          // If there are not subsequent game steps
          case i if i >= game.history.movesNumber - 1 =>
            view.foreach(view => {
              view.setPlayEnabled(false)
              view.setNextGameStepEnabled(false)
              view.setLastGameStepEnabled(false)
              if (game.history.movesNumber > 0) {
                view.setFirstGameStepEnabled(true)
                view.setPreviousGameStepEnabled(true)
              }
            })
          // If there are subsequent game steps
          case _ =>
            view.foreach(view => {
              view.setFirstGameStepEnabled(true)
              view.setPreviousGameStepEnabled(true)
              view.setPlayEnabled(true)
              view.setNextGameStepEnabled(true)
              view.setLastGameStepEnabled(true)
            })
        }
    }
  }

  private def moveIndexFromGameStepIndex(gameStepIndex: Option[Int]): Option[PgnMoveIndex] = {
    gameStepIndex.map[PgnMoveIndex](index => {
      val turnModule: Int = (index + 1) % 2
      PgnMoveIndex(
        ((index + 1) / 2).floor.toInt + turnModule,
        if(turnModule == 0) Black else White)
    })
  }

  private def updateGameStepIndex(updateStrategy: Option[Int] => Option[Int],
                                  updateView: Boolean = true): Unit = {
    val originalGameStepIndex: Option[Int] = gameStepIndex
    gameStepIndex = updateStrategy(gameStepIndex)
    if (!isPlaying.get) {
      checkHistoryNavigation()
    }
    if (updateView) {
      gameStepIndex.fold(view.foreach(view => {
        view.drawBoard(ChessGame.copy(game)().board.reset().pieces)
        view.updateTakenPieces(White, Seq.empty)
        view.updateTakenPieces(Black, Seq.empty)
      }))(index => {
        val gameStep: ChessGameStep[_] = game.history.gameSteps(index)
        // Checks board enabling according to current position inside history
        if (gameStep == game.history.gameSteps.last) {
          view.foreach(_.enableBoard())
        } else {
          view.foreach(_.disableBoard())
        }
        // Evaluates the last action
        gameStep.action match {
          case checkableAction: CheckableAction =>
            val currentTurn: ChessColor = !moveIndexFromGameStepIndex(gameStepIndex).get.playerIndex
            view.foreach(view => {
              // Draws the board with the last move for the action evidenced
              view.drawBoard(gameStep.board.pieces, Some(calculateLastMoveFromAction(checkableAction)))
              // Checks for check drawing
              checkableAction.kingState match {
                case Check | Checkmate => view.setKingCheck(gameStep.board.kingPos(currentTurn))
                case _ =>
              }
            })
            // Draws taken pieces at action moment only if an action between the starting one and the arrival one is a capture
            val fromIndex: Int = Math.min(index, originalGameStepIndex.getOrElse(0))
            val toIndex: Int = Math.max(index, originalGameStepIndex.getOrElse(0)) + 1
            // Searches the colors that have taken pieces in the selected range of movement
            val colorsToUpdate: Seq[ChessColor] = game.history.gameSteps
              .slice(fromIndex, toIndex)
              .filter(_.action.isInstanceOf[ChessCaptureAction])
              .map(_.action.asInstanceOf[ChessCaptureAction].from._1.color)
            // Updates the taken pieces only for the colors with a new capture in the range, if present
            colorsToUpdate.foreach(player => {
              view.foreach(_.updateTakenPieces(
                player,
                gameStep.board.taken.filter(_.color == !player)
              ))
            })
          case _ =>
        }
      })
      view.foreach(view => {
        view.setHistory(PgnConverter.historyToInteractivePgn(game.history))
        view.setCurrentMoveInHistory(moveIndexFromGameStepIndex(gameStepIndex))
      })
    }
  }

  protected def performCheckableAction(action: ChessGame => ChessGame)
                                      (implicit doOnSuccess: () => Unit, doOnFail: String => Unit): Unit = {
    // Does the game action
    Try {
      game = action(game)
    } match {
      case Success(_) =>
        // Evaluates the history action resulting from the move
        game.history.gameSteps.last.action match {
          // If the performed move has caused the end of the game, handles game over
          case endingAction: ChessEndingAction =>
            view.foreach(view => view.drawBoard(game.board.pieces, None))
            setGameEnded(endingAction)
          // If the performed move has not caused the end of the game
          case nonEndingAction =>
            nonEndingAction match {
              case checkableAction: CheckableAction =>
                // Calculates the last move's coordinates according to the action
                val lastMove: (BoardPosition, BoardPosition) = calculateLastMoveFromAction(checkableAction)
                // Checks for taken pieces
                checkableAction match {
                  case _: ChessCaptureAction => view.foreach(_.updateTakenPieces(
                    !game.currentTurn,
                    game.board.taken.filter(_.color == game.currentTurn)
                  ))
                  case _ =>
                }
                // Checks if a promotion is possible after the move
                if (game.flags.promotionAvailability) {
                  view.foreach(view => {
                    // Draws the board with the pawn to promote
                    view.drawBoard(game.board.pieces, Some(lastMove))
                    // Asks for promotion role selection (the turn must be the same)
                    view.disableBoard()
                    view.enablePromotionSelection(lastMove._2, game.currentTurn, ChessRoles.promotableRoles)
                  })
                } else {
                  // Checks if the last action is a promotion completion
                  checkableAction match {
                    case _: ChessPromotionAction => view.foreach(view => view.enableBoard())
                    case _ =>
                  }
                  // Updates the chess clock
                  !game.currentTurn match {
                    case White => gameClock.foreach(_.whitePressure())
                    case Black => gameClock.foreach(_.blackPressure())
                  }
                  // Notifies the next turn
                  view.foreach(view => {
                    view.setTurn(game.currentTurn)
                  })
                  /*
                  Updates the PGN move index.
                  Considers the drawing of the new board and the related effects only if the move index is in last position.
                   */
                  if (gameStepIndex.isEmpty || gameStepIndex.get == game.history.movesNumber - 2) {
                    updateGameStepIndex(nextIndexStrategy)
                  } else {
                    updateGameStepIndex(nextIndexStrategy, updateView = false)
                  }
                }
              case _ =>
            }
        }
        doOnSuccess()
      case Failure(exception) =>
        view.foreach(view => view.windowManager.showMessage(exception.getMessage, MessageTypes.Error))
        doOnFail(exception.getMessage)
    }
  }

  override def onFirstGameStep(): Unit = {
    require(gameStepIndex.isDefined,
      throw HistoryException("Incorrect input: unavailable previous step", UnavailablePreviousStep))
    updateGameStepIndex(_ => None)
  }

  override def onPreviousGameStep(): Unit = {
    require(gameStepIndex.isDefined,
      throw HistoryException("Incorrect input: unavailable previous step", UnavailablePreviousStep))
    updateGameStepIndex(_.flatMap(i => if (i > 0) Some(i - 1) else None))
  }

  override def play(): Unit = {
    val scheduler = AdvancedScheduler()
    isPlaying.set(true)
    scheduler.doWhile(
      onNextGameStep(),
      gameStepIndex.fold(true)(_ < game.history.movesNumber - 1 && isPlaying.get()),
      Some(() => {
        isPlaying.set(false)
        view.foreach(_.setStopEnabled(false))
        checkHistoryNavigation()
      }))(1.seconds)
    view.foreach(view => {
      view.setFirstGameStepEnabled(false)
      view.setPreviousGameStepEnabled(false)
      view.setPlayEnabled(false)
      view.setStopEnabled(true)
      view.setNextGameStepEnabled(false)
      view.setLastGameStepEnabled(false)
    })
  }

  override def stop(): Unit = {
    isPlaying.set(false)
  }

  override def onNextGameStep(): Unit = {
    require((gameStepIndex.isEmpty && game.history.movesNumber > 0) || (gameStepIndex.isDefined && gameStepIndex.get < game.history.movesNumber - 1),
      throw HistoryException("Incorrect input: unavailable next step", UnavailableNextStep))
    updateGameStepIndex(nextIndexStrategy)
  }

  override def onLastGameStep(): Unit = {
    require((gameStepIndex.isEmpty && game.history.movesNumber > 0) || (gameStepIndex.isDefined && gameStepIndex.get < game.history.movesNumber - 1),
      throw HistoryException("Incorrect input: unavailable next step", UnavailableNextStep))
    updateGameStepIndex(_ => Some(game.history.movesNumber - 1))
  }

  override def onDownloadRequest(): Unit = {
    view.foreach(view => view.showDownloadInfo(FenParser.toFen(game), PgnConverter.gameToPgn(game), game.dateTime))
  }

  override def onPgnSaving(pgn: String, file: File): Unit = {
    Files.write(file.toPath, pgn.getBytes(StandardCharsets.UTF_8))
  }

  override def setView(view: GameScene): Unit = {
    this.view = Some(view)
    this.view.get.init(game.players, ChessUtilities.BOARD_END_INDEX)
    this.view.get.drawBoard(game.board.pieces)
    this.view.get.setTurn(game.currentTurn)

    if (game.history.movesNumber > 0) {
      this.view.get.setHistory(PgnConverter.historyToInteractivePgn(game.history))
      checkHistoryNavigation()
      onLastGameStep()
    }
  }

  override def setGameClock(clock: ChessClock): Unit = {
    gameClock = Some(clock)
  }

  override def onConnect(): Unit = {}

}
