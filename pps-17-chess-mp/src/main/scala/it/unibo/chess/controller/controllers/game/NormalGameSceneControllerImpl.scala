package it.unibo.chess.controller.controllers.game

import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.controller.traits.scenes.game.NormalGameSceneController
import it.unibo.chess.controller.traits.scheduling.ChessClockObserver
import it.unibo.chess.exceptions.Game.GameException
import it.unibo.chess.exceptions.Game.Types.GameOver
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.history.ChessActions.ChessEndingAction
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.Intent
import it.unibo.chess.view.general.views.DrawRequestMode

import scala.concurrent.duration.FiniteDuration

/**
  * Implementation of the AbstractGameSceneController, when the game needs to be interactive, with timers.
  *
  * @param chessGame the game to manage
  */
case class NormalGameSceneControllerImpl(chessGame: ChessGame)
  extends AbstractGameSceneController(chessGame) with NormalGameSceneController with ChessClockObserver {

  override var gameOver: Boolean = false
  var drawApplicant: Option[ChessColor] = None

  gameClock.foreach(_.addObserver(this))

  def setClock(chessClock: ChessClock): Unit = {
    gameClock = Some(chessClock)
    gameClock.foreach(_.addObserver(this))
  }

  override def setGameEnded(endingAction: ChessEndingAction): Unit = {
    // Sets the flag
    gameOver = true
    // Stops the chess clock
    gameClock.foreach(_.stop())

    super.setGameEnded(endingAction)
  }

  override def performCheckableAction(action: ChessGame => ChessGame)(implicit doOnSuccess: () => Unit, doOnFail: String => Unit): Unit = {
    // Cannot perform action after game over
    if (gameOver) throw GameException("Game is over", GameOver)
    // The execution of an action determines the rejection of a draw request
    if (drawApplicant.isDefined) drawApplicant = None

    super.performCheckableAction(action)
  }

  override def onTick(timers: Map[ChessColor, FiniteDuration]): Unit = {
    view.foreach(view => view.updateClock(timers))
  }

  override def onTimeout(loser: ChessColor): Unit = {
    game = game.lose(loser, LoseReasons.TimeOutLoss)
    setGameEnded(game.history.gameResult.get)
  }

  override def onDrawApplication(color: ChessColor, status: Boolean): Unit = {
    if (drawApplicant.isDefined) {
      if (status) {
        game = game.draw(DrawReasons.MutualDraw)
        setGameEnded(game.history.gameResult.get)
      } else {
        view.foreach(_.setDrawRequestMode(DrawRequestMode.Normal, color))
        drawApplicant = None
      }
    } else {
      view.foreach(_.setDrawRequestMode(DrawRequestMode.PendingRequest, color))
      drawApplicant = Some(color)
    }
  }

  override def onSquareSelected(boardPosition: BoardPosition): Unit = {
    // Shows the highlight only if the selected square is occupied by a piece of the current turn
    val turn = game.currentTurn
    game.board.pieceAt(boardPosition) match {
      case Some(ChessPiece(`turn`, _)) => view.foreach(view => {
        view.setSquareSelected(boardPosition)
        view.setSquaresToHighlighted(game.movesFrom(boardPosition))
      })
      case _ =>
    }
  }

  override def onPieceMove(from: BoardPosition, to: BoardPosition)
                          (implicit doOnSuccess: () => Unit, doOnFail: String => Unit): Unit = {
    performCheckableAction(game => game.move(from, to))(doOnSuccess, doOnFail)
  }

  override def onPromotionSelected(boardPosition: BoardPosition, role: ChessRoles.ChessRole)
                                  (implicit doOnSuccess: () => Unit, doOnFail: String => Unit): Unit = {
    performCheckableAction(game => game.promote(boardPosition, role))(doOnSuccess, doOnFail)
  }

  override def onExitGame(isSavingEnabled: Boolean): Unit = {
    view.foreach(view => view.windowManager.setScene(new Intent(SceneType.MainScene)))
  }

  override def setGameClock(clock: ChessClock): Unit = {
    gameClock = Some(clock)
    gameClock.get.addObserver(this)
  }

  override def onViewLoadCompletion(): Unit = {
    gameClock.foreach(_.blackPressure())
    view.get.disablePlayerWaiting()
    view.get.setPlayable(gameClock.get.startingTimers, gameClock.get.timerStrategy.chessMode)
  }

}
