package it.unibo.chess.controller.controllers.game

import akka.actor.{ActorRef, PoisonPill}
import it.unibo.chess.controller.controllers.actors.messages.ControllerMessages._
import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.model.core.ChessRoles.ChessRole
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.core._
import it.unibo.chess.view.MessageTypes
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.Intent

/**
  * Extension of the NormalGameSceneController for multiplayer purposes
  * @param chessGame Game to manage
  */
class MultiplayerGameSceneControllerImpl(chessGame: ChessGame) extends NormalGameSceneControllerImpl(chessGame) {

  private var actor: ActorRef = _
  private var color: ChessColor = _

  private var address: String = _
  private var port: Int = _

  private def updateView(): Unit = {
    if (this.view.isDefined && color != null) {
      color match {
        case White =>
          // So black is missing, I'm the creator
          view.get.setIP(address)
          view.get.setPort(port.toString)
        case Black =>
        // So white is missing, I'm the challenger
      }
    }
  }

  private def returnToMainMenu(): Unit = {
    actor ! PoisonPill
    view.foreach(_.windowManager.setScene(new Intent(SceneType.MainScene)))
  }

  /**
    * First init of a multiplayer game controller.
    *
    * @param actorRef the reference of the own actor
    * @param actorColor the color of the own actor/player
    */
  def localInit(actorRef: ActorRef, actorColor: ChessColor): Unit = {
    actor = actorRef
    color = actorColor

    updateView()
    view.get.enablePlayerWaiting(!color)
  }

  def setActorAddressAndPort(address: String, port: Int): Unit = {
    this.address = address
    this.port = port
    updateView()
  }

  def onGameReady(player: ChessColor, name: String): Unit = {
    view.get.disablePlayerWaiting()
    //view.get.setPlayerName(player, name)
    view.get.setPlayable(gameClock.get.startingTimers, gameClock.get.timerStrategy.chessMode, Some(player, name))
    gameClock.foreach(_.blackPressure())
  }

  override def onPieceMove(from: BoardPosition, to: BoardPosition)
                          (implicit doOnSuccess: () => Unit = () => {}, doOnFail: String => Unit = _ => {}): Unit = {
    if (game.currentTurn == color) {
      super.onPieceMove(from, to)(() => {
        actor ! SetMove(from, to)
      })
    }
  }

  /**
    * Moves performed by the remote actor.
    *
    * @param from position from the movement
    * @param to position to the movement
    * @param doOnSuccess method executed after super's success
    * @param doOnFail method executed after super's failure
    */
  def onOpponentPieceMove(from: BoardPosition, to: BoardPosition)(doOnSuccess: () => Unit = () => {}, doOnFail: String => Unit = _ => {}): Unit = {
    super.onPieceMove(from, to)(doOnSuccess, doOnFail)
  }

  override def onPromotionSelected(boardPosition: BoardPosition, role: ChessRole)
                                  (implicit doOnSuccess: () => Unit = () => {}, doOnFail: String => Unit = _ => {}): Unit = {
    if (game.currentTurn == color) {
      super.onPromotionSelected(boardPosition, role)(() => {
        actor ! SetPromote(boardPosition, role)
      })
    }
  }

  /**
    * Promotion performed by the remote actor.
    *
    * @param boardPosition the position where to promote
    * @param role the role to promote
    * @param doOnSuccess method executed after super's success
    * @param doOnFail method executed after super's failure
    */
  def onOpponentPromotionSelected(boardPosition: BoardPosition, role: ChessRole)
                                 (implicit doOnSuccess: () => Unit = () => {}, doOnFail: String => Unit = _ => {}): Unit = {
    super.onPromotionSelected(boardPosition, role)(doOnSuccess, doOnFail)
  }

  override def onDrawApplication(color: ChessColor, status: Boolean): Unit = {
    super.onDrawApplication(color, status)
    actor ! SetDraw(status)
  }

  /**
    * Draw offered/rejected by the remote actor.
    *
    * @param color the color of the actor
    * @param status accepted or rejected
    */
  def onOpponentDrawApplication(color: ChessColor, status: Boolean): Unit = {
    super.onDrawApplication(color, status)
  }

  override def setGameClock(clock: ChessClock): Unit = {
    gameClock = Some(clock)
    gameClock.get.addObserver(this)
  }

  override def onViewLoadCompletion(): Unit = {}

  override def onConnect(): Unit = {
    // If I'm here, I'm the BLACK asking a WHITE to join his game
    val address: String = view.get.getIP
    val port: Int = view.get.getPort

    actor ! SetLaunchChallenge(address, port)
  }

  override def onExitGame(isSavingEnabled: Boolean): Unit = {
    actor ! SetUploadAgreement(isSavingEnabled)
    view.get.setOnWaitingForSave()
  }

  def onUploadCompleted(isSavingEnabled: Boolean,message: String = ""): Unit = {
    if (isSavingEnabled)
      view.foreach(_.windowManager.showMessage("Saving status", message, MessageTypes.Info))

    returnToMainMenu()
  }

  def onActorDead(): Unit = {
    view.foreach(_.windowManager.showMessage(
      "Connection lost",
      "There was a problem with communication between you and your opponent: the game cannot proceed. Sorry!",
      MessageTypes.Info)
    )

    returnToMainMenu()
  }

  override def onTimeout(loser: ChessColor): Unit = {
    super.onTimeout(loser)
    actor ! SetCheckGameOver
  }
}
