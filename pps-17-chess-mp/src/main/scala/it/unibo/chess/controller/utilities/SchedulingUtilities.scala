package it.unibo.chess.controller.utilities

import akka.actor.{ActorSystem, Cancellable, Scheduler}

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration.FiniteDuration

/**
  * Handles a short-term task scheduling library.
  */
object SchedulingUtilities {

  /**
    * The actor system from which select the scheduler to use.
    */
  val actorSystem: ActorSystem = ActorSystem("SchedulingSystem")

  implicit val executor: ExecutionContextExecutor = actorSystem.dispatcher

  /**
    * Handles an advanced scheduler with custom features.
    *
    * @param scheduler the scheduler to use
    */
  case class AdvancedScheduler(scheduler: Scheduler = actorSystem.scheduler) {

    /**
      * Executes a task after a certain amount of time.
      *
      * @param task the task to execute
      * @param interval the interval length
      * @return the cancellable scheduled operation.
      */
    def doOnce(task: => Unit)(interval: FiniteDuration): Cancellable = scheduler.scheduleOnce(interval)(task)

    /**
      * Executes a task periodically.
      *
      * @param task the task to execute
      * @param interval the interval length
      * @return the cancellable scheduled operation.
      */
    def doRegularly(task: => Unit)(interval: FiniteDuration): Cancellable = scheduler.schedule(interval, interval)(task)

    /**
      * Executes a task periodically while a condition returns true.
      *
      * @param task the task to execute
      * @param whileFn the while function
      * @param endTask the optional task to execute at the end
      * @param interval the interval length
      */
    def doWhile(task: => Unit, whileFn: => Boolean, endTask: Option[() => Unit] = None)(interval: FiniteDuration): Unit = {
      if (whileFn) {
        task
        doOnce(doWhile(task, whileFn, endTask)(interval))(interval)
      } else {
        endTask.fold((): Unit)(task => task())
      }
    }

  }

}
