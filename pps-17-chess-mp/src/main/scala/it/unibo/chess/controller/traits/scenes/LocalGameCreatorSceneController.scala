package it.unibo.chess.controller.traits.scenes

import it.unibo.chess.controller.traits.GenericController
import it.unibo.chess.view.general.observers.gameCreator.LocalGameCreatorSceneObserver
import it.unibo.chess.view.general.scenes.gameCreator.LocalGameCreatorScene

trait LocalGameCreatorSceneController extends LocalGameCreatorSceneObserver with GenericController[LocalGameCreatorScene]
