package it.unibo.chess.controller.controllers

import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.controller.traits.scenes.LocalGameCreatorSceneController
import it.unibo.chess.model.core._
import it.unibo.chess.model.modes.ChessModes
import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.{GameSceneIntent, Intent}
import it.unibo.chess.view.general.scenes.gameCreator.LocalGameCreatorScene

import scala.concurrent.duration.FiniteDuration

case class LocalGameCreatorSceneControllerImpl() extends LocalGameCreatorSceneController {

  override def onBack(): Unit = {
    require(view.isDefined)
    view.get.windowManager.setScene(new Intent(SceneType.MainScene))
  }

  override def onStart(): Unit = {
    require(view.isDefined)
    val players: Map[ChessColor, ChessPlayer] = Map(White -> ChessPlayer(view.get.getWhitePlayerName), Black -> ChessPlayer(view.get.getBlackPlayerName))
    val gameMode: ChessMode = view.get.getSelectedGameMode
    val time: FiniteDuration = view.get.getSelectedTime

    val game: ChessGame = ChessGame(players = players)
    val clock: ChessClock = ChessClock(gameMode, Map(White -> time, Black -> time))

    view.get.windowManager.setScene(new GameSceneIntent(SceneType.GameScene, game, Some(clock)))
  }

  override def setView(view: LocalGameCreatorScene): Unit = {
    super.setView(view)
    this.view.get.setSelectableGameModes(ChessModes.chessModes)
  }

}