package it.unibo.chess.controller.database

import it.unibo.chess.controller.settings.AppSettings
import org.mongodb.scala.{MongoClient, MongoClientSettings, MongoCredential}
import org.mongodb.scala.connection.NettyStreamFactoryFactory

import scala.collection.JavaConverters._

/**
  * Calculates the representation of the MongoDB client inside the Atlas cluster
  * used for the application, via mongo settings.
  * Note: the driver does not attempt to connect to the database until it is needed.
  * The connection is so lazily established.
  */
object MongoConnector {

  private val appSettings: AppSettings = AppSettings()

  // Specifies authentication credentials
  private val credential: MongoCredential = MongoCredential.createCredential(
    appSettings.mongoSettings.username,
    appSettings.mongoSettings.authSource,
    appSettings.mongoSettings.password.toCharArray)

  // Builds final settings and enables SSL (using Netty)
  private val dbSettings: MongoClientSettings = MongoClientSettings
    .builder()
    .applyToClusterSettings(b => b.hosts(appSettings.mongoSettings.cluster.asJava))
    .credential(credential)
    .applyToSslSettings(b => b.enabled(true))
    .streamFactoryFactory(NettyStreamFactoryFactory())
    .build()

  /**
    * The client-side representation of the MongoDB cluster for the chess game application.
    */
  val mongoDbClient: MongoClient = MongoClient(dbSettings)

}
