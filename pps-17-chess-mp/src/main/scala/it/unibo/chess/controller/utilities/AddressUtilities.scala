package it.unibo.chess.controller.utilities

import akka.actor.{ActorSystem, Address, ExtendedActorSystem, Extension, ExtensionId}

/**
  * Utilities to retrieve Address and Port from an existing ActorSystem.
  */
object AddressUtilities {

  private[AddressUtilities] class AddressExtension(system: ExtendedActorSystem) extends Extension {
    val address: Address = system.provider.getDefaultAddress
  }

  private[AddressUtilities] object AddressExtension extends ExtensionId[AddressExtension] {
    def createExtension(system: ExtendedActorSystem): AddressExtension = new AddressExtension(system)
  }

  /**
    * Method to retrieve the address of an existing ActorSystem.
    *
    * @param system the system to analyze.
    * @return the system's address.
    */
  def addressOf(system: ActorSystem): String = AddressExtension(system).address.host.getOrElse("")

  /**
    * Method to retrieve the port of an existing ActorSystem.
    *
    * @param system the system to analyze.
    * @return the system's port.
    */
  def portOf(system: ActorSystem): Int = AddressExtension(system).address.port.getOrElse(0)

}