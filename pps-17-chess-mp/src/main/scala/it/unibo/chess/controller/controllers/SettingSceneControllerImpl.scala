package it.unibo.chess.controller.controllers

import it.unibo.chess.controller.preferences.PreferencesHandler
import it.unibo.chess.controller.traits.scenes.SettingsSceneController
import it.unibo.chess.view.MessageTypes
import it.unibo.chess.view.fx.utilities.SettingPreferences
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.Intent

case class SettingSceneControllerImpl() extends SettingsSceneController {

  override def init(): Unit = {
    // Notifies current preferences to the view
    view.foreach(view => view.showCurrentPreferences(SettingPreferences(
      PreferencesHandler.getLegalMovesPreference,
      PreferencesHandler.getLastMovePreference,
      PreferencesHandler.getStylePreference)
    ))
  }

  override def onBack(): Unit = {
    require(view.isDefined)
    view.get.windowManager.setScene(new Intent(SceneType.MainScene))
  }

  override def onApply(settingPreferences: SettingPreferences): Unit = {
    require(view.isDefined)

    PreferencesHandler.setLegalMovesPreference(settingPreferences.isLegalMovesHighlightingEnabled)
    PreferencesHandler.setLastMovePreference(settingPreferences.isLastMoveHighlightingEnabled)
    PreferencesHandler.setStylePreference(settingPreferences.boardStyle)

    view.get.windowManager.showMessage("Settings saved successfully", MessageTypes.Info)
  }
}