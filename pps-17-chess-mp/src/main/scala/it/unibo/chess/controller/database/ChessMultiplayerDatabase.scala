package it.unibo.chess.controller.database

import com.sfxcode.nosql.mongo.MongoDAO
import com.sfxcode.nosql.mongo.database.DatabaseProvider
import it.unibo.chess.controller.settings.AppSettings
import it.unibo.chess.model.core.ChessPlayer
import org.bson.codecs.configuration.CodecRegistries._
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoClient
import org.mongodb.scala.bson.codecs.Macros._

/**
  * Models the database and the DAO objects for the chess multiplayer application.
  *
  * @param mongoClient the mongo client from which retrieve the database and the collections
  */
case class ChessMultiplayerDatabase(mongoClient: MongoClient = MongoConnector.mongoDbClient) {

  private val appSettings: AppSettings = AppSettings()

  // Defines a codec that can convert ChessGameMongo to and from BSON
  private val codecRegistry: CodecRegistry = fromProviders(classOf[ChessGameMongo], classOf[ChessPlayer])

  /**
    * The database inside the specified client, with supported codecs.
    */
  val database: DatabaseProvider = DatabaseProvider(appSettings.mongoSettings.dbName, codecRegistry, mongoClient)

  /**
    * The DAO for games handling (typed find and CRUD operations).
    */
  object ChessGameDAO extends MongoDAO[ChessGameMongo](database, appSettings.mongoSettings.gamesCollectionName)

}
