package it.unibo.chess.controller.controllers.actors.messages

import it.unibo.chess.model.core.ChessRoles.ChessRole
import it.unibo.chess.model.core.position.BoardPosition

object ControllerMessages {

  final case class SetUploadAgreement(agreement: Boolean)

  final case object GetAddressAndPort

  final case class SetMove(fromPos: BoardPosition, toPos: BoardPosition)

  final case class SetLaunchChallenge(creatorAddress: String, creatorPort: Int)

  final case class SetPromote(position: BoardPosition, newRole: ChessRole)

  final case class SetDraw(status: Boolean)

  final case object SetCheckGameOver

}
