package it.unibo.chess.controller.traits

trait GenericController[V] {

  protected var view: Option[V] = None

  def setView(view: V): Unit = this.view = Some(view)

}
