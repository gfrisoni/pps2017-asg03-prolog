package it.unibo.chess.controller.traits.scenes.game

trait NormalGameSceneController extends GameSceneController {
  var gameOver: Boolean
}
