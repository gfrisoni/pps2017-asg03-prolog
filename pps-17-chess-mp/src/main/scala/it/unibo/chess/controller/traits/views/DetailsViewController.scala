package it.unibo.chess.controller.traits.views

import it.unibo.chess.controller.traits.GenericController
import it.unibo.chess.view.general.observers.DetailsViewObserver
import it.unibo.chess.view.general.views.DetailsView

trait DetailsViewController extends DetailsViewObserver with GenericController[DetailsView]
