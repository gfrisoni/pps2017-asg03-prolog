package it.unibo.chess.controller.controllers

import it.unibo.chess.controller.traits.scenes.MainSceneController
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.Intent

case class MainSceneControllerImpl() extends MainSceneController {

  override def onStartLocalGame(): Unit = {
    require(view.isDefined)
    view.get.windowManager.setScene(new Intent(SceneType.LocalGameCreatorScene))
  }

  override def onStartMultiPlayerGame(): Unit = {
    require(view.isDefined)
    view.get.windowManager.setScene(new Intent(SceneType.MultiPlayerGameCreatorScene))
  }

  override def onTvMode(): Unit = {
    require(view.isDefined)
    view.get.windowManager.setScene(new Intent(SceneType.TvScene))
  }

  override def onSettings(): Unit = {
    require(view.isDefined)
    view.get.windowManager.setScene(new Intent(SceneType.SettingsScene))
  }

}
