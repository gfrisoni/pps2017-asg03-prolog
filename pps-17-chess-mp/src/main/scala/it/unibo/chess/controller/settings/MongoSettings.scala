package it.unibo.chess.controller.settings

import com.typesafe.config.{Config, ConfigFactory}
import org.mongodb.scala.ServerAddress
import scala.collection.JavaConverters._

/**
  * Represents the settings for the connection to a mongo database.
  */
sealed trait MongoSettings {

  /**
    * The configuration from which retrieve data.
    */
  val config: Config

  /**
    * The username associated to the MongoDB role.
    */
  val username: String

  /**
    * The password associated to the MongoDB role.
    */
  val password: String

  /**
    * The server addresses of the cluster.
    */
  val cluster: List[ServerAddress]

  /**
    * The database name associated with the user’s credentials.
    */
  val authSource: String

  /**
    * The name of the mongo database.
    */
  val dbName: String

  /**
    * The name of the collection related to the chess games.
    */
  val gamesCollectionName: String

}

/**
  * Handles the settings implementation for the connection to a mongo database
  * and provides factories.
  */
object MongoSettings {

  /**
    * Constructs the mongo settings from the specified config.
    *
    * @param config the configuration to use
    * @return the mongo settings constructed from the specified config.
    */
  def apply(config: Config): MongoSettings = MongoSettingsImpl(config)

  /**
    * @return the mongo settings constructed from the standard default configuration.
    */
  def apply(): MongoSettings = MongoSettingsImpl(ConfigFactory.load())

  // It considers a field for each setting and throws an exception if any settings are missing
  private case class MongoSettingsImpl(override val config: Config) extends MongoSettings {

    // Verifies that the config has a mongo path
    config.checkValid(ConfigFactory.defaultReference(), "mongo")

    // Gets fields and throws exceptions at construct time if does not find them
    override val username: String = config.getString("mongo.username")
    override val password: String = config.getString("mongo.password")
    override val cluster: List[ServerAddress] =
      config
        .getConfigList("mongo.cluster")
        .asScala
        .map(c => ServerAddress(c.getString("host"), c.getInt("port")))
        .toList
    override val authSource: String = config.getString("mongo.authSource")
    override val dbName: String = config.getString("mongo.dbName")
    override val gamesCollectionName: String = config.getString("mongo.gamesCollectionName")

    override def toString: String =
      s"""username = $username
         |password = <hidden>
         |cluster = {${cluster.map(s => s.toString).mkString(";")}}
         |authSource = $authSource
         |databaseName = $dbName
         |gamesCollectionName = $gamesCollectionName""".stripMargin

  }

}