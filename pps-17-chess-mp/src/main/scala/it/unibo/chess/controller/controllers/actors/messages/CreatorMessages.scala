package it.unibo.chess.controller.controllers.actors.messages

import it.unibo.chess.model.modes.ChessModes.ChessMode

object CreatorMessages {

  final case class ChallengeAccepted(opponentName: String, chessMode: ChessMode, time: Long)

  final case class UpdateStatus(isSavingEnabled: Boolean, message: String = "")

}
