package it.unibo.chess.controller.scheduling

import scala.concurrent.duration._
import scala.language.implicitConversions

/**
  * Handles the time related to a chess game.
  */
object TimeHandler {

  // Number of time updates in a Second. MUST be a divisor of 1000 (number of milliseconds in a second)
  private val clockRate: Long = 20

  require(FiniteDuration(1L, SECONDS).toMillis % clockRate == 0, "Cannot use a clock rate that isn't a perfect divisor of 1000 due to the millisecond conversion")

  /**
    * Starting from a real time duration converts it on a time unit #clockRate times smaller.
    * Once repeated #clockRate times in a second it will return to the real time value.
    *
    * @param abs the real time duration
    */
  case class RelativeDuration(abs: FiniteDuration) {
    def rel: FiniteDuration = {FiniteDuration(abs.toMillis / clockRate, MILLISECONDS)}
  }

  /**
    * Implicit to enable .rel on FiniteDuration.
    */
  object RelTime {
    implicit def toRelTime(finiteDuration: FiniteDuration): RelativeDuration = RelativeDuration(finiteDuration)
  }

}
