package it.unibo.chess.controller.traits.scenes

import it.unibo.chess.controller.traits.GenericController
import it.unibo.chess.view.general.observers.SettingsSceneObserver
import it.unibo.chess.view.general.scenes.SettingsScene

trait SettingsSceneController extends SettingsSceneObserver with GenericController[SettingsScene] {

  def init(): Unit

}
