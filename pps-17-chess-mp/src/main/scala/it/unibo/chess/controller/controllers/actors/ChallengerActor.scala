package it.unibo.chess.controller.controllers.actors

import akka.actor.Terminated
import it.unibo.chess.controller.controllers.actors.messages.ChallengerMessages.{DiscoveryCompleted, LaunchChallenge, UploadAgreement}
import it.unibo.chess.controller.controllers.actors.messages.ControllerMessages.{SetLaunchChallenge, SetUploadAgreement}
import it.unibo.chess.controller.controllers.actors.messages.CreatorMessages.{ChallengeAccepted, UpdateStatus}
import it.unibo.chess.controller.controllers.game.MultiplayerGameSceneControllerImpl
import it.unibo.chess.controller.database.ChessGameDatabaseFunctions
import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.model.core.{Black, ChessColor, White}

import scala.concurrent.duration._


/**
  * The implementation of the Challenger actor, who opens the game as second.
  *
  * @param controller its controller
  * @param dbFunctions its database functions
  */
class ChallengerActor(override val controller: MultiplayerGameSceneControllerImpl,
                      override val dbFunctions: ChessGameDatabaseFunctions) extends AbstractMultiplayerActor(controller, dbFunctions) {

  val color: ChessColor = Black

  override def initialBehaviour: Receive = {
    case SetLaunchChallenge(address, port) => launchChallenge(address, port)
    case ChallengeAccepted(opponentName, chessMode, time) =>
      controller.setClock(ChessClock(chessMode, Map(White -> time.minutes, Black -> time.minutes)))
      startGame(opponentName)
      opponentRef.get ! DiscoveryCompleted(controller.game.players(color).name)
      context.become(gameBehaviour)
    case Terminated(deadActor) if deadActor == opponentRef.get => controller.onActorDead()
  }

  override def finalBehaviour: Receive = {
    case UpdateStatus(isSavingEnabled, message) => controller.onUploadCompleted(isSavingEnabled, message)
    case SetUploadAgreement(agreement) => uploadAgreement(color, agreement)
    case Terminated(deadActor) if deadActor == opponentRef.get => controller.onActorDead()
  }

  override def uploadAgreement(color: ChessColor, agreement: Boolean): Unit = {
    opponentRef.get ! UploadAgreement(agreement)
  }

  private def launchChallenge(creatorAddress: String, creatorPort: Int): Unit = {
    context.actorSelection(s"akka.tcp://MultiplayerActor@$creatorAddress:$creatorPort/user/creator") ! LaunchChallenge
  }

}
