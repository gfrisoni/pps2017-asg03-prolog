package it.unibo.chess.controller.controllers.actors.messages

object ChallengerMessages {

  final case object LaunchChallenge

  final case class DiscoveryCompleted(opponentName: String)

  final case class UploadAgreement(agreement: Boolean)

}
