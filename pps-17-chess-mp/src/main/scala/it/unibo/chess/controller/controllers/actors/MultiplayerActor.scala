package it.unibo.chess.controller.controllers.actors

import akka.actor.{Actor, ActorRef, Terminated}
import it.unibo.chess.controller.controllers.actors.messages.ChallengerMessages.UploadAgreement
import it.unibo.chess.controller.controllers.actors.messages.CommonMessages.{Draw, Move, MoveFeedback, Promote}
import it.unibo.chess.controller.controllers.actors.messages.ControllerMessages.{SetCheckGameOver, SetDraw, SetMove, SetPromote}
import it.unibo.chess.controller.controllers.game.MultiplayerGameSceneControllerImpl
import it.unibo.chess.controller.database.ChessGameDatabaseFunctions
import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.controller.traits.scenes.game.GameSceneController
import it.unibo.chess.model.core.LoseReasons.DisconnectLoss
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter.toPosition
import it.unibo.chess.model.history.ChessActions
import it.unibo.chess.model.history.ChessActions.LoseAction

import scala.concurrent.duration._
import scala.language.implicitConversions

sealed trait MultiplayerActor extends Actor {

  val controller: GameSceneController
  val dbFunctions: ChessGameDatabaseFunctions

  /**
    * The player can decide if he want his game uploaded, there are 3 possible cases:
    * - 2 false: game is not uploaded.
    * - 1 true 1 false: the false player's name is anonymized.
    * - 2 true: game is uploaded with both players' names.
    *
    * @param agreement if the player wants to upload
    */
  def uploadAgreement(color: ChessColor, agreement: Boolean)

  /**
    * The initial context, different in creator and opponent for the discovery
    *
    * @return the allowed messages on this phase.
    */
  def initialBehaviour: Receive

  /**
    * The game moves behaviours, both actors will use the same before switching.
    *
    * @return the allowed messages on this phase.
    */
  def gameBehaviour: Receive

  /**
    * The final context, different in creator and opponent for uploading the game.
    *
    * @return the allowed messages on this phase.
    */
  def finalBehaviour: Receive
}

/**
  * The basic actor behaviours and methods.
  *
  * @param controller its controller
  * @param dbFunctions its database functions
  */
abstract class AbstractMultiplayerActor(controller: MultiplayerGameSceneControllerImpl,
                                        dbFunctions: ChessGameDatabaseFunctions) extends MultiplayerActor {

  var opponentRef: Option[ActorRef] = None
  val color: ChessColor

  override def receive: Receive = initialBehaviour

  override def initialBehaviour: Receive

  override def gameBehaviour: Receive = {
    case SetMove(fromPos, toPos) =>
      val timers = controller.getGameClock.get.getTimerValues
      opponentRef.get ! Move(Symbol(fromPos.toString), Symbol(toPos.toString), timers(color).toMillis, timers(!color).toMillis)
      checkGameOver()
    case Move(fromPos, toPos, oppTime, ownTime) =>
      if (controller.game.currentTurn == color) {
        opponentRef.get ! MoveFeedback(Some("Cannot move a piece when it isn't your turn!"))
      } else {
        import SyncableTimer.toSyncable
        controller.getGameClock.get.sync(ownTime, oppTime)
        controller.onOpponentPieceMove(fromPos, toPos)(() => doOnSuccessfulAction(), (exception: String) => doOnFailedAction(exception))
        checkGameOver()
      }
    case SetPromote(position, newRole) => opponentRef.get ! Promote(Symbol(position.toString), newRole)
    case Promote(position, newRole) =>
      controller.onOpponentPromotionSelected(position, newRole)(() => doOnSuccessfulAction(), (exception: String) => doOnFailedAction(exception))
    case SetDraw(status) =>
      opponentRef.get ! Draw(status)
      checkGameOver()
    case Draw(status) =>
      controller.onOpponentDrawApplication(!color, status)
      checkGameOver()
    case SetCheckGameOver => checkGameOver()
    case MoveFeedback(err)
      if err.isDefined =>
      throw new IllegalStateException(err.get)
      controller.onActorDead()
    case Terminated(deadActor) if deadActor == opponentRef.get => controller.onActorDead()

  }

  def startGame(opponentName: String): Unit = {
    opponentRef = Some(sender())
    context.watch(opponentRef.get)
    controller.game = ChessGame.copy(controller.game)(newPlayers = controller.game.players + (!color -> ChessPlayer(opponentName)))
    context.become(gameBehaviour)
    controller.onGameReady(!color, opponentName)
  }

  private def doOnSuccessfulAction(): Unit = {
    opponentRef.get ! MoveFeedback(None)
  }

  private def doOnFailedAction(exception: String): Unit = {
    opponentRef.get ! MoveFeedback(Some(exception))
  }

  private def checkGameOver(): Unit = {
    if (controller.gameOver) {
      context.become(finalBehaviour)
    }
  }

  case class SyncableTimer(chessClock: ChessClock) {
    def sync(ownTime: Long, oppTime: Long): Unit = {
      controller.getGameClock.get.setTimerValues(Map(color -> ownTime.milliseconds, !color -> oppTime.milliseconds))
    }
  }

  private object SyncableTimer {
    implicit def toSyncable(gameTimer: ChessClock): SyncableTimer = SyncableTimer(gameTimer.asInstanceOf[ChessClock])
  }

}