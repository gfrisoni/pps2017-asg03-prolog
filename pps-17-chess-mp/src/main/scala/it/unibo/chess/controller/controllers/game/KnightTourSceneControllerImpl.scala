package it.unibo.chess.controller.controllers.game

import alice.tuprolog._
import it.unibo.chess.controller.traits.scenes.game.KnightTourSceneController
import it.unibo.chess.controller.utilities.SchedulingUtilities.AdvancedScheduler
import it.unibo.chess.model.core.ChessRoles.Knight
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.core.{ChessPiece, White}
import it.unibo.chess.model.utilities.{CharFormatUtilities, ChessUtilities}
import it.unibo.chess.view.MessageTypes

import scala.concurrent.duration._

case class KnightTourSceneControllerImpl() extends KnightTourSceneController {

  val knightPiece: ChessPiece = ChessPiece(White, Knight)
  var startingPosition: Option[BoardPosition] = None

  val engine: Prolog = new Prolog()
  val theory: Theory = new Theory(
    """range(Low, High, Values) :-
      |	integer(Low),
      |	integer(High),
      |	Low =< High,
      |	range_(Low, High, Values).
      |range_(Low, Low, Values) :-
      |	!,
      |	Values = [Low].
      |range_(Low, High, [Low|Values]) :- NewLow is Low + 1, range_(NewLow, High, Values).
      |
      |boardPos(W, H, X, Y) :-
      |	range(1, W, WidthRange),
      |	range(1, H, HeightRange),
      |	member(X, WidthRange),
      |	member(Y, HeightRange).
      |
      |knightMoves(W, H, X, Y, NewX, NewY) :-
      |	boardPos(W, H, X, Y),
      |	( NewX is X - 1, NewY is Y - 2;
      |		NewX is X - 1, NewY is Y + 2;
      |		NewX is X + 1, NewY is Y - 2;
      |		NewX is X + 1, NewY is Y + 2;
      |		NewX is X - 2, NewY is Y - 1;
      |		NewX is X - 2, NewY is Y + 1;
      |		NewX is X + 2, NewY is Y - 1;
      |		NewX is X + 2, NewY is Y + 1 ),
      |	boardPos(W, H, NewX, NewY).
      |
      |unvisitedKnightMoves(W, H, Visits, X, Y, NewX, NewY) :-
      |	is_list(Visits),
      |	knightMoves(W, H, X, Y, NewX, NewY),
      |	not(member((NewX, NewY), Visits)).
      |
      |onwardKnightMovesCount(W, H, Visits, X, Y, NewX, NewY, Count) :-
      |	findall(_, unvisitedKnightMoves(W, H, Visits, X, Y, NewX, NewY), Moves),
      |	length(Moves, Count).
      |
      |warnsdorfEvaluation(W, H, Visits, X, Y, NewX, NewY, Score) :-
      |	unvisitedKnightMoves(W, H, Visits, X, Y, NewX, NewY),
      |	onwardKnightMovesCount(W, H, [(NewX, NewY) | Visits], NewX, NewY, _, _, Score).
      |
      |warnsdorfMinScores([], Mins, Mins).
      |warnsdorfMinScores([(X, Y, NewX, NewY, Score)|T], [(_, _, _, _, CurScore)|_], Mins) :-
      |	Score < CurScore,
      |	!,
      |	warnsdorfMinScores(T, [(X, Y, NewX, NewY, Score)], Mins).
      |warnsdorfMinScores([(X, Y, NewX, NewY, Score)|T], [(CurX, CurY, CurNewX, CurNewY, CurScore)|CurT], Mins) :-
      |	Score =:= CurScore,
      |	!,
      |	warnsdorfMinScores(T, [(X, Y, NewX, NewY, Score),(CurX, CurY, CurNewX, CurNewY, CurScore)|CurT], Mins).
      |warnsdorfMinScores([_|T], X, Mins) :-
      |	warnsdorfMinScores(T, X, Mins).
      |
      |warnsdorfBestMoves(W, H, Visits, X, Y, BestMoves) :-
      |	findall((X, Y, NewX, NewY, Score), warnsdorfEvaluation(W, H, Visits, X, Y, NewX, NewY, Score), [HE|T]),
      |	warnsdorfMinScores(T, [HE], BestMoves).
      |
      |
      |getByIndex([H|_], 0, H) :- !.
      |getByIndex([_|T], I, E) :- NewIndex is I-1, getByIndex(T, NewIndex, E).
      |
      |randomMember(List, X) :-
      |	list(List),
      |	length(List, Len),
      |	Len > 0,
      |	rand_int(Len, N),
      |	getByIndex(List, N, X).
      |
      |knightTour(W, H, X, Y, Path) :-
      |	knightTour_(W, H, [], X, Y, Path).
      |knightTour_(W, H, Visits, X, Y, Path) :-
      |	length(Visits, L),
      |	NewVisits = [(X, Y) | Visits],
      |	(	L =:= W * H - 1 ->	reverse(NewVisits, Path);
      |                       warnsdorfBestMoves(W, H, Visits, X, Y, BestMoves),
      |												randomMember(BestMoves, (_, _, NewX, NewY, _)),
      |												knightTour_(W, H, NewVisits, NewX, NewY, Path)	).
    """.stripMargin
  )
  engine.setTheory(theory)

  /**
    * Action performed when a square is selected.
    *
    * @param boardPosition position of the selected square
    */
  override def onSquareSelected(boardPosition: BoardPosition): Unit = {
    if (startingPosition.isEmpty) {
      val info: SolveInfo = engine.solve("knightTour(" +
        s"${ChessUtilities.BOARD_END_INDEX}, " +
        s"${ChessUtilities.BOARD_END_INDEX}, " +
        s"${boardPosition.row}, ${boardPosition.col}, Path).")
      if (info.isSuccess) {
        startingPosition = Some(boardPosition)
        val term: Term = info.getTerm("Path")
        var path: Seq[BoardPosition] = Seq.empty
        term match {
          case s: Struct =>
            s.listIterator().forEachRemaining {
              case sMove: Struct =>
                path = path :+ ChessBoardPosition(CharFormatUtilities.intToLetter(sMove.getArg(1).toString.toInt) + sMove.getArg(0))
              case _ => view.foreach(_.windowManager.showMessage("Error", "Invalid format for the result calculated by the Prolog engine", MessageTypes.Error))
            }
            var pathIndex = 0
            AdvancedScheduler().doWhile(
              {
                view.foreach(view => {
                  view.drawBoard(Map(path(pathIndex) -> knightPiece))
                  view.clearSquaresText()
                  view.setSquaresText(path.take(pathIndex).zipWithIndex.map{ case (pos, index) => (pos, (index + 1).toString) }.toMap)
                })
                pathIndex += 1
              },
              pathIndex < path.length,
              Some(() => {
                view.foreach(view => {
                  view.drawBoard(Map.empty)
                  view.clearSquaresText()
                  view.setSquaresText(path.take(pathIndex).zipWithIndex.map{ case (pos, index) => (pos, (index + 1).toString) }.toMap)
                })
              })
            )(700.milliseconds)
          case _ => view.foreach(_.windowManager.showMessage("Error", "Invalid format for the result calculated by the Prolog engine", MessageTypes.Error))
        }
      } else {
        view.foreach(_.windowManager.showMessage("Error", "Unsuccessful query resolution", MessageTypes.Error))
      }
    }
  }

  /**
    * Action performed when there is a request to move a piece from a position to another.
    *
    * @param from        starting position of the piece moving
    * @param to          final position of the piece moving
    * @param doOnSuccess action to perform when moving is successful
    * @param doOnFail    action to perform when moving fails
    */
  override def onPieceMove(from: BoardPosition, to: BoardPosition)(implicit doOnSuccess: () => Unit, doOnFail: String => Unit): Unit = {}

}
