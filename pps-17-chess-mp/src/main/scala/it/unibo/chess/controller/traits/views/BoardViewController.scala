package it.unibo.chess.controller.traits.views

import it.unibo.chess.controller.traits.GenericController
import it.unibo.chess.view.general.observers.SquareViewObserver
import it.unibo.chess.view.general.views.BoardView

trait BoardViewController extends SquareViewObserver with GenericController[BoardView]
