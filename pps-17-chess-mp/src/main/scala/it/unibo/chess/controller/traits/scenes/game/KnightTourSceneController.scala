package it.unibo.chess.controller.traits.scenes.game

import it.unibo.chess.controller.traits.GenericController
import it.unibo.chess.view.general.observers.KnightTourSceneObserver
import it.unibo.chess.view.general.scenes.KnightTourScene

trait KnightTourSceneController extends KnightTourSceneObserver with GenericController[KnightTourScene]
