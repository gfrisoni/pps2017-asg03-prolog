package it.unibo.chess.controller.database

import com.mongodb.client.result.UpdateResult
import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.model.core.ChessGame
import org.mongodb.scala.{Completed, FindObservable, Observable}
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.bson.ObjectId

/**
  * Represents the custom operations to execute on a ChessGameDao object.
  */
trait ChessGameDatabaseFunctions {

  /**
    * Saves the specified chess game into the database.
    *
    * @param game the chess game to save
    * @param gameClock the chess clock related to the game
    * @return the storing completion status.
    */
  def saveGame(game: ChessGame, gameClock: ChessClock): Observable[Completed]

  /**
    * @return the number of chess games stored into the database.
    */
  def gamesSize: Observable[Long]

  /**
    * Gets all the chess games saved into the database.
    *
    * @return all the saved chess games.
    */
  def findAllGames(): FindObservable[ChessGameMongo]

  /**
    * Increments the number of views of a saved chess game.
    *
    * @param _id the chess game id to update
    * @return the updating completion status.
    */
  def incrementViews(_id: ObjectId): Observable[UpdateResult]

}

/**
  * The companion object of ChessGameDatabaseFunctions.
  * It provides factories.
  */
object ChessGameDatabaseFunctions {

  /**
    * Creates the chess game database functions for the specified [[ChessMultiplayerDatabase]].
    *
    * @param database the [[ChessMultiplayerDatabase]] with the ChessGameDAO to consider
    * @return the new chess game database functions.
    */
  def apply(database: ChessMultiplayerDatabase): ChessGameDatabaseFunctions = ChessGameDatabaseFunctionsImpl(database)

  private case class ChessGameDatabaseFunctionsImpl(database: ChessMultiplayerDatabase) extends ChessGameDatabaseFunctions {

    override def saveGame(game: ChessGame, gameClock: ChessClock): Observable[Completed] = database.ChessGameDAO.insertOne(ChessGameMongo(game, gameClock))

    override def gamesSize: Observable[Long] = database.ChessGameDAO.count()

    override def findAllGames(): FindObservable[ChessGameMongo] = database.ChessGameDAO.find().sort(descending("dateTime"))

    override def incrementViews(_id: ObjectId): Observable[UpdateResult] = database.ChessGameDAO.updateOne(equal("_id", _id), inc("views", 1))

  }

}