package it.unibo.chess.controller.preferences

import java.util.prefs.Preferences

import it.unibo.chess.view.fx.utilities.BoardStyles
import net.liftweb.json.Extraction._
import net.liftweb.json._
import net.liftweb.json.ext.EnumSerializer

/**
  * Handles the user settings preferences for the application.
  */
object PreferencesHandler {

  // The preferences' identifiers
  private val LEGAL_MOVES_PREF: String = "legalMoves"
  private val LAST_MOVE_PREF: String = "lastMove"
  private val STYLE_PREF: String = "style"

  // Defines the node in which the data is stored
  private val prefs: Preferences = Preferences.userNodeForPackage(this.getClass)

  implicit val formats: Formats = net.liftweb.json.DefaultFormats + new EnumSerializer(BoardStyles)

  /**
    * Sets the user preference for legal moves highlighting.
    *
    * @param legalMovesPref true if the user wants legal moves highlighting on, false otherwise
    */
  def setLegalMovesPreference(legalMovesPref: Boolean): Unit = {
    prefs.putBoolean(LEGAL_MOVES_PREF, legalMovesPref)
  }

  /**
    * @return the current user legal moves highlighting preference (true if the value is not set yet).
    */
  def getLegalMovesPreference: Boolean = {
    prefs.getBoolean(LEGAL_MOVES_PREF, true)
  }

  /**
    * Sets the user preference for last move highlighting.
    *
    * @param lastMovePref true if the user wants last move highlighting on, false otherwise
    */
  def setLastMovePreference(lastMovePref: Boolean): Unit = {
    prefs.putBoolean(LAST_MOVE_PREF, lastMovePref)
  }

  /**
    * @return the current user last move highlighting preference (true if the value is not set yet).
    */
  def getLastMovePreference: Boolean = {
    prefs.getBoolean(LAST_MOVE_PREF, true)
  }

  /**
    * Sets the user preference for the board style.
    *
    * @param style the board style to use
    */
  def setStylePreference(style: BoardStyles.Value): Unit = {
    prefs.put(STYLE_PREF, compactRender(decompose(style)))
  }

  /**
    * @return the current user style preference (classic if the value is not set yet).
    */
  def getStylePreference: BoardStyles.Value = {
    parse(prefs.get(STYLE_PREF, compactRender(decompose(BoardStyles.Default)))).extract[BoardStyles.Value]
  }

  /**
    * Resets all user preferences.
    */
  def reset(): Unit = {
    prefs.remove(LEGAL_MOVES_PREF)
    prefs.remove(LAST_MOVE_PREF)
    prefs.remove(STYLE_PREF)
  }

}
