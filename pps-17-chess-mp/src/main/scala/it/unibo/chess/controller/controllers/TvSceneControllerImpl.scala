package it.unibo.chess.controller.controllers

import it.unibo.chess.controller.database.{ChessGameDatabaseFunctions, ChessGameMongo, ChessMultiplayerDatabase}
import it.unibo.chess.controller.traits.scenes.TvSceneController
import it.unibo.chess.model.pgn.PgnConverter
import it.unibo.chess.view.MessageTypes
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.{GameSceneIntent, Intent}
import it.unibo.chess.view.general.observers.TvGameSortingType
import it.unibo.chess.view.general.scenes.TvScene
import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.result.UpdateResult

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

case class TvSceneControllerImpl() extends TvSceneController {

  private var games: List[ChessGameMongo] = List.empty
  private var nMoves: Long = 0
  private var duration: Long = 0
  private val functions = ChessGameDatabaseFunctions(ChessMultiplayerDatabase())

   private def retrieveAllGames(): Unit = {
    functions.findAllGames().subscribe(
      (result: ChessGameMongo) => {
        games = games :+ result
        view.foreach(view => {
         view.showSavedGames(games)
          nMoves = updateAverageValue(nMoves, result.nMoves)
          duration = updateAverageValue(duration, result.duration)
          view.showStatistics(games.size, nMoves, duration)
        })
      },
      (e: Throwable) => {
        view.foreach(view => {
          view.disableLoading()
          view.windowManager.showMessage("An error occurred while downloading game list", e.getMessage, MessageTypes.Error)
        })
      },
      () => {
        view.foreach(_.disableLoading())
      }
    )
  }

  private def updateAverageValue(average: Long, newValue: Long): Long = {
    ((average * (games.size - 1)) + newValue) / games.size
  }

  override def onSortingSelected(tvGameSortingType: TvGameSortingType.Value): Unit = {
    if (games.nonEmpty)
      tvGameSortingType match {
        case TvGameSortingType.Date =>
          games = games.sortWith(_.dateTime > _.dateTime)
          view.foreach(_.showSavedGames(games))

        case TvGameSortingType.Views =>
          games = games.sortWith(_.views > _.views)
          view.foreach(_.showSavedGames(games))
      }
  }

  override def onGameSelected(_id: ObjectId): Unit = {
    playGame(_id)
    incrementViews(_id)
  }

  private def incrementViews(_id: ObjectId): Unit = {
    functions.incrementViews(_id).subscribe(
      (_: UpdateResult) => {},
      (e: Throwable) => view.get.windowManager.showMessage("An error occurred during updating game views", e.getMessage, MessageTypes.Error),
      () => {}
    )
  }

  private def playGame(_id: ObjectId): Unit = {
    require(view.isDefined)
    PgnConverter.fromPgn(games.find(_._id == _id).get.pgn, view) onComplete {
      case Success(game) => view.get.windowManager.setScene(new GameSceneIntent(SceneType.GameScene, game, None))
      case Failure(exception) =>
        view.get.disableLoading()
        view.get.windowManager.showMessage("An error occurred during PGN parsing", exception.getMessage, MessageTypes.Error)
    }
  }

  override def setView(view: TvScene): Unit = {
    super.setView(view)
    this.view.get.init()
  }

  override def onViewLoaded(): Unit = {
    require(view.isDefined)
    this.view.get.enableLoading()
    retrieveAllGames()
  }

  override def onBack(): Unit = {
    require(view.isDefined)
    view.get.windowManager.setScene(new Intent(SceneType.MainScene))
  }

}