package it.unibo.chess.controller.traits.scheduling

import it.unibo.chess.model.core.ChessColor

import scala.concurrent.duration.FiniteDuration

trait ChessClockObserver {

  def onTick(timers: Map[ChessColor, FiniteDuration]): Unit

  def onTimeout(loser: ChessColor): Unit

}