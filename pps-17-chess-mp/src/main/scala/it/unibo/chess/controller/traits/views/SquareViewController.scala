package it.unibo.chess.controller.traits.views

import it.unibo.chess.controller.traits.GenericController
import it.unibo.chess.view.general.observers.SquareViewObserver
import it.unibo.chess.view.general.views.SquareView

trait SquareViewController extends SquareViewObserver with GenericController[SquareView]
