package it.unibo.chess.controller.traits.scenes

import it.unibo.chess.controller.traits.GenericController
import it.unibo.chess.view.general.observers.gameCreator.MultiPlayerGameCreatorSceneObserver
import it.unibo.chess.view.general.scenes.gameCreator.MultiPlayerGameCreatorScene

trait MultiPlayerGameCreatorSceneController extends MultiPlayerGameCreatorSceneObserver with GenericController[MultiPlayerGameCreatorScene]
