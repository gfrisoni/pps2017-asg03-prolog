package it.unibo.chess.controller.scheduling

import it.unibo.chess.model.core.ChessColor
import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.controller.scheduling.TimeHandler.RelTime.toRelTime

import scala.concurrent.duration._
import scala.language.implicitConversions

/**
  * Represents an updating strategy for a chess clock.
  */
sealed trait ChessClockStrategy {

  /**
    * The chess mode which the clock strategy refers.
    */
  val chessMode: ChessMode

  /**
    * The timers update strategy to perform at every tick.
    *
    * @param timers the chess clock timers to update
    * @param currentTurn the current turn of the chess game
    * @return the updated timers of the chess clock.
    */
  def updateTimersOnTick(timers: Map[ChessColor, FiniteDuration], currentTurn: ChessColor): Map[ChessColor, FiniteDuration]

  /**
    * The timers update strategy to perform at every switch.
    *
    * @param timers the chess clock timers to update
    * @param currentTurn the current turn of the chess game
    * @return the updated timers of the chess clock.
    */
  def updateTimersOnSwitch(timers: Map[ChessColor, FiniteDuration], currentTurn: ChessColor): Map[ChessColor, FiniteDuration]

}

/**
  * The companion object of ChessClockStrategy.
  * Provides factories for chess clock updating strategies and utilities.
  */
object ChessClockStrategy {

  /**
    * Creates a clock updating strategy based on the specified chess mode.
    *
    * @param chessMode the chess mode to consider
    * @return the clock updating strategy related to {{{chessMode}}}.
    */
  def apply(chessMode: ChessMode): ChessClockStrategy = ChessClockStrategyImpl(chessMode)

  case class DeltaExtractor(option: Option[FiniteDuration]) {
    private def extract: FiniteDuration = option.getOrElse(0.second)
    def getDelta(rel: Boolean): FiniteDuration = if (rel) extract.rel else extract
  }

  object DeltaExtractor {
    implicit def toExtractor(option: Option[FiniteDuration]): DeltaExtractor = DeltaExtractor(option)
  }

  /**
    * Handles the timer updating strategies of a chess clock.
    *
    * @param chessMode the chess mode to consider
    */
  private case class ChessClockStrategyImpl(override val chessMode: ChessMode) extends ChessClockStrategy {

    import DeltaExtractor.toExtractor

    private def updateTimers(timers: Map[ChessColor, FiniteDuration], currentTurn: ChessColor)
                            (currentStrategy: Option[FiniteDuration])(oppositeStrategy: => Option[FiniteDuration])(rel: Boolean): Map[ChessColor, FiniteDuration] =
      timers + (currentTurn -> (timers(currentTurn) + currentStrategy.getDelta(rel))) + (!currentTurn -> (timers(!currentTurn) + oppositeStrategy.getDelta(rel)))

    override def updateTimersOnTick(timers: Map[ChessColor, FiniteDuration], currentTurn: ChessColor): Map[ChessColor, FiniteDuration] =
      updateTimers(timers, currentTurn)(chessMode.currentColorTick())(chessMode.oppositeColorTick())(rel = true)

    override def updateTimersOnSwitch(timers: Map[ChessColor, FiniteDuration], currentTurn: ChessColor): Map[ChessColor, FiniteDuration] =
      updateTimers(timers, currentTurn)(chessMode.currentColorSwitch())(chessMode.oppositeColorSwitch())(rel = false)

  }

}