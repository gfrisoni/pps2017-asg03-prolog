package it.unibo.chess.view.general.views

import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.core.{ChessColor, ChessPiece}
import it.unibo.chess.view.general.ObservableView
import it.unibo.chess.view.general.observers.SquareViewObserver

/**
  * Represents the square of a big Chess Board.
  */
trait SquareView extends ObservableView[SquareViewObserver] {
  /**
    * Position of the square.
    */
  val boardPosition: BoardPosition

  /**
    * Color of the square.
    */
  val color: ChessColor

  /**
    * Get the [[ChessPiece]] that is eventually inside this square.
    * @return An optional, representing eventually the piece inside this square
    */
  def getPiece: Option[ChessPiece]

  /**
    * Remove the piece from this square, if there is any.
    */
  def removePiece(): Unit

  /**
    * Put a piece inside this square.
    * @param piece the piece to put inside this square
    */
  def setPiece(piece: ChessPiece)

  /**
    * Set this square on highlight mode, to suggest valid movements.
    * @param isHighlighted true if the square must be highlighted
    */
  def setOnHighlighted(isHighlighted: Boolean): Unit

  /**
    * Set this square on last moved mode, to show the last movement occurred on the board.
    * @param isLastMoved true if the square must be on last moved mode
    */
  def setOnLastMoved(isLastMoved: Boolean): Unit

  /**
    * Set this square selected, to show currently selected square (for example, the square from will start a move).
    * @param isSelected true if the square must be on selected mode
    */
  def setSelected(isSelected: Boolean): Unit

  /**
    * Set this square on king check mode, to show that the king inside it is in danger.
    * @param isKingCheck true if the square must be on king check mode
    */
  def setOnKingCheck(isKingCheck: Boolean): Unit

  /**
    * Set the text to show on the square.
    * @param text the text to display
    */
  def setText(text: String): Unit

  /**
    * Remove the text from the square.
    */
  def clearText(): Unit

  /**
    * Check if the square is on highlight mode.
    * @return true if the square is highlighted
    */
  def isHighlightEnabled: Boolean

  /**
    * Check if the square is on last moved mode.
    * @return true if the square is on last moved mode
    */
  def isLastMoveEnabled: Boolean

  /**
    * Check of the square is on the king check mode.
    * @return true if the square is on the king check mode
    */
  def isOnKingCheck: Boolean

}