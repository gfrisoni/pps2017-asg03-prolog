package it.unibo.chess.view.fx.scenes

import java.io.File
import javafx.fxml.FXML
import javafx.geometry.Insets
import javafx.scene.control.{Button, Label, TextArea, TextField}
import javafx.scene.layout.{BorderPane, HBox, Priority, VBox}
import javafx.stage.FileChooser

import it.unibo.chess.model.core.ChessRoles.ChessRole
import it.unibo.chess.model.core.DrawReasons._
import it.unibo.chess.model.core.LoseReasons._
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.history.ChessActions.{ChessEndingAction, DrawAction, LoseAction}
import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.model.pgn.{PgnFullMove, PgnMoveIndex}
import it.unibo.chess.view.fx.GenericFXView
import it.unibo.chess.view.fx.utilities.SettingPreferences
import it.unibo.chess.view.fx.views.{FXBoardView, FXDetailsView}
import it.unibo.chess.view.general.observers.{BoardViewObserver, DetailsViewObserver}
import it.unibo.chess.view.general.scenes.GameScene
import it.unibo.chess.view.general.views.DrawRequestMode
import it.unibo.chess.view.{FXWindow, MessageTypes}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.concurrent.duration.FiniteDuration

case class FXGameScene(override val windowManager: FXWindow, settingPreferences: SettingPreferences) extends GenericFXView(Some("GameScene.fxml")) with GameScene {

  @FXML protected var boardView: FXBoardView = _
  @FXML protected var detailsView: FXDetailsView = _

  private var players: Map[ChessColor, ChessPlayer] = _
  private var playableForPlayer: Option[ChessColor] = None

  override def init(players: Map[ChessColor, ChessPlayer], sideSize: Int): Unit = {
    this.players = players
    boardView.initBoard(sideSize, settingPreferences)
    detailsView.initDetails(players)

    // By default, disable board and wait for controller signal that all's ok
    boardView.disableBoard()

    observers.foreach(observer => {
      boardView.addObserver(new BoardViewObserver {
        override def onSquareSelected(boardPosition: BoardPosition): Unit = observer.onSquareSelected(boardPosition)

        override def onPieceMove(from: BoardPosition,
                                 to: BoardPosition)
                                (implicit doOnSuccess: () => Unit,
                                 doOnFail: String => Unit): Unit = observer.onPieceMove(from, to)()
      })

      detailsView.addObserver(new DetailsViewObserver {
        override def onFirstGameStep(): Unit = observer.onFirstGameStep()

        override def onPreviousGameStep(): Unit = observer.onPreviousGameStep()

        override def play(): Unit = observer.play()

        override def stop(): Unit = observer.stop()

        override def onNextGameStep(): Unit = observer.onNextGameStep()

        override def onLastGameStep(): Unit = observer.onLastGameStep()

        override def onDownloadRequest(): Unit = observer.onDownloadRequest()

        override def onExitGame(isSavingEnabled: Boolean): Unit = observer.onExitGame(isSavingEnabled)

        override def onPromotionSelected(boardPosition: BoardPosition,
                                         role: ChessRoles.ChessRole)
                                        (implicit doOnSuccess: () => Unit,
                                         doOnFail: String => Unit): Unit = observer.onPromotionSelected(boardPosition, role)

        override def onDrawApplication(color: ChessColor, status: Boolean): Unit = observer.onDrawApplication(color, status)

        override def onConnect(): Unit = observer.onConnect()

      })
      observer.onViewLoadCompletion()
    })
  }

  override def setPlayable(clock: Map[ChessColor, FiniteDuration],
                           gameMode: ChessMode,
                           missingPlayer: Option[(ChessColor, String)]): Unit = {

    if (missingPlayer.isEmpty) {
      // Local game
      playableForPlayer = None
    } else {
      // Multiplayer game -> if missing was BLACK this board is for WHITE only
      playableForPlayer = Some(!missingPlayer.get._1)

      // Update names, since one of them was "unknown"
      players = Map(
        !missingPlayer.get._1 -> ChessPlayer(players(!missingPlayer.get._1).name),
        missingPlayer.get._1 -> ChessPlayer(missingPlayer.get._2))
      detailsView.setPlayerName(White, players(White).name)
      detailsView.setPlayerName(Black, players(Black).name)
    }
    boardView.setPlayable(playableForPlayer)
    detailsView.setPlayable(clock, gameMode, missingPlayer)

  }

  override def updateClock(clock: Map[ChessColor, FiniteDuration]): Unit =
    detailsView.updateClock(clock)

  override def updateTakenPieces(chessColor: ChessColor, pieces: Seq[ChessPiece]): Unit =
    detailsView.updateTakenPieces(chessColor, pieces)

  override def setTurn(chessColor: ChessColor): Unit =
    detailsView.setTurn(chessColor)

  override def setHistory(history: (Map[Int, PgnFullMove], Option[String])): Unit =
    detailsView.setHistory(history)

  override def setCurrentMoveInHistory(moveIndex: Option[PgnMoveIndex]): Unit =
    detailsView.setCurrentMoveInHistory(moveIndex)

  override def enablePromotionSelection(boardPosition: BoardPosition, chessColor: ChessColor, promotableRoles: Seq[ChessRole]): Unit =
    detailsView.enablePromotionSelection(boardPosition, chessColor, promotableRoles)

  override def setPlayModeEnabled(isEnabled: Boolean): Unit =
    detailsView.setPlayModeEnabled(isEnabled)

  override def setFirstGameStepEnabled(isEnabled: Boolean): Unit =
    detailsView.setFirstGameStepEnabled(isEnabled)

  override def setPreviousGameStepEnabled(isEnabled: Boolean): Unit =
    detailsView.setPreviousGameStepEnabled(isEnabled)

  override def setPlayEnabled(isEnabled: Boolean): Unit =
    detailsView.setPlayEnabled(isEnabled)

  override def setStopEnabled(isEnabled: Boolean): Unit =
    detailsView.setStopEnabled(isEnabled)

  override def setNextGameStepEnabled(isEnabled: Boolean): Unit =
    detailsView.setNextGameStepEnabled(isEnabled)

  override def setLastGameStepEnabled(isEnabled: Boolean): Unit =
    detailsView.setLastGameStepEnabled(isEnabled)

  override def showDownloadInfo(fen: String, pgn: String, dateTime: DateTime): Unit = {
    val content: BorderPane = new BorderPane()
    val insets: Insets = new Insets(10)

    content.setTop(
      new VBox(
        new Label("FEN"),
        new TextField() { setEditable(false); setText(fen) }
      ){ BorderPane.setMargin(this, insets) }
    )

    content.setCenter(
      new VBox(
        new Label("PGN"),
        new TextArea() { setEditable(false); setWrapText(true); setText(pgn) }
      ){ BorderPane.setMargin(this, insets) }
    )

    content.setBottom(
      new HBox(
        new Button("Save PGN as file") {
          setOnMouseClicked(_ => {
            val fileChooser: FileChooser = new FileChooser()
            // Sets extension filter for pgn files
            val extFilter = new FileChooser.ExtensionFilter("PGN files (*.pgn)", "*.pgn")
            fileChooser.getExtensionFilters.add(extFilter)
            // Sets default file name
            fileChooser.setInitialFileName("chessmp_" + DateTimeFormat.forPattern("yyyyMMdd").print(dateTime))
            // Shows the save file dialog
            val file: File = fileChooser.showSaveDialog(windowManager.stage)
            if (file != null) {
              observers.foreach(observer => observer.onPgnSaving(pgn, file))
            }
          })

          setMaxWidth(Double.MaxValue)
          HBox.setHgrow(this, Priority.ALWAYS)
        }
      ) {
        BorderPane.setMargin(this, insets)
      }
    )

    windowManager.showCustomMessage(content, MessageTypes.Info)
  }

  override def setWhiteDrawRequestEnabled(isEnabled: Boolean): Unit =
    detailsView.setWhiteDrawRequestEnabled(isEnabled)

  override def setBlackDrawRequestEnabled(isEnabled: Boolean): Unit =
    detailsView.setBlackDrawRequestEnabled(isEnabled)

  override def setDrawRequestMode(drawRequestMode: DrawRequestMode.Value, color: ChessColor): Unit =
    detailsView.setDrawRequestMode(drawRequestMode, color)

  override def setIP(ip: String): Unit =
    detailsView.setIP(ip)

  override def setPort(port: String): Unit =
    detailsView.setPort(port)

  override def getIP: String =
    detailsView.getIP

  override def getPort: Int =
    detailsView.getPort

  override def setOnWaitingForSave(): Unit =
    detailsView.setOnWaitingForSave()

  override def drawBoard(pieces: Map[BoardPosition, ChessPiece], lastMove: Option[(BoardPosition, BoardPosition)]): Unit =
    boardView.drawBoard(pieces, lastMove)

  override def setSquaresToHighlighted(boardPositions: Set[BoardPosition]): Unit =
    boardView.setSquaresToHighlighted(boardPositions)

  override def setSquaresText(squaresText: Map[BoardPosition, String]): Unit =
    boardView.setSquaresText(squaresText)

  override def clearSquaresText(): Unit =
    boardView.clearSquaresText()

  override def setSquareSelected(boardPosition: BoardPosition): Unit =
    boardView.setSquareSelected(boardPosition)

  override def enableBoard(): Unit =
    boardView.enableBoard()

  override def disableBoard(): Unit =
    boardView.disableBoard()

  override def setKingCheck(kingPosition: BoardPosition): Unit =
    boardView.setKingCheck(kingPosition)

  override def setGameEnded(endingAction: ChessEndingAction): Unit = {
    endingAction match {
      case LoseAction(loserColor, loseReason) =>
        val description: String = loseReason match {
          case CheckmateLoss => s"${players(!loserColor).name} has won for chess mate."
          case SurrenderLoss => s"${players(!loserColor).name} has won for the opponent's withdrawal."
          case TimeOutLoss => s"${players(!loserColor).name} has won for timeout."
          case DisconnectLoss => s"${players(!loserColor).name} has won for the opponent's disconnection."
        }
        this.windowManager.showMessage(
          "The " + (if (!loserColor == White) "White" else "Black") + " has won",
          description,
          MessageTypes.Info
        )
      case DrawAction(drawReason) =>
        val description: String = drawReason match {
          case MutualDraw => s"${players(White).name} and ${players(Black).name} have tied."
          case FiftyMovesDraw => s"${players(White).name} and ${players(Black).name} have tied according to the 50-moves rule."
          case StalemateDraw => s"${players(White).name} and ${players(Black).name} have tied for stalemate."
        }
        this.windowManager.showMessage(
          "Draw",
          description,
          MessageTypes.Info
        )
    }
    boardView.disableBoard()
    detailsView.setGameEnded(endingAction)
  }

  override def enablePlayerWaiting(chessColor: ChessColor): Unit = {
    boardView.disableBoard()
    detailsView.enablePlayerWaiting(chessColor)
  }

  override def disablePlayerWaiting(): Unit = {
    boardView.enableBoard()
    detailsView.disablePlayerWaiting()
  }
}
