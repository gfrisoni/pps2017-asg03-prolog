package it.unibo.chess.view.general.observers

/**
  * Represents the observer for the [[it.unibo.chess.view.general.scenes.MainScene]]
  */
trait MainSceneObserver extends ViewObserver {

  /**
    * Action performed when a user wants to start a local game
    */
  def onStartLocalGame(): Unit

  /**
    * Action performed when a user wants to start a multiplayer game
    */
  def onStartMultiPlayerGame(): Unit

  /**
    * Action performed when a user wants to start athe TvMode
    */
  def onTvMode(): Unit

  /**
    * Action performed when a user wants to open settings
    */
  def onSettings(): Unit

}