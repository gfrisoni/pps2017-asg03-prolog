package it.unibo.chess.view.fx.utilities

/**
  * Represent the preferences a user can specify
  * @param isLegalMovesHighlightingEnabled if true, enable the highlight
  * @param isLastMoveHighlightingEnabled if true, enable the selection of the last move
  * @param boardStyle represents the style of the board
  */
case class SettingPreferences(isLegalMovesHighlightingEnabled: Boolean,
                              isLastMoveHighlightingEnabled: Boolean,
                              boardStyle: BoardStyles.Value)
