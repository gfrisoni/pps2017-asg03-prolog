package it.unibo.chess.view.fx.views

import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.view.fx.GenericFXView
import it.unibo.chess.view.fx.utilities.{PseudoClasses, SettingPreferences}
import it.unibo.chess.view.general.views.SquareView
import javafx.application.Platform
import javafx.fxml.FXML
import javafx.scene.image.{Image, ImageView}
import javafx.scene.paint.Color
import javafx.scene.shape.{Circle, Rectangle}
import javafx.scene.text.Text

/**
  * Represents the fx view for the [[SquareView]]
  * @param boardPosition position on the board of this square
  * @param color color of this square
  * @param chessImages set of images that the square can use to display pieces
  * @param settingPreferences preferences of the user
  */
class FXSquareView(override val boardPosition: BoardPosition,
                   override val color: ChessColor,
                   chessImages: Map[ChessColor, Map[ChessRole, Image]],
                   settingPreferences: SettingPreferences) extends GenericFXView(Some("SquareView.fxml")) with SquareView {

  @FXML protected var pieceImg: ImageView = _
  @FXML protected var highlightCircle: Circle = _
  @FXML protected var squareRect: Rectangle = _
  @FXML protected var squareText: Text = _

  private var piece: Option[ChessPiece] = None
  private var isHighlightActive = false
  private var isLastMoveActive = false
  private var isKingCheckActive = false

  color match {
    case White => this.setId("lightSquare")
    case Black => this.setId("darkSquare")
  }

  // Set an event listener when clicked
  this.setOnMouseClicked(_ => {
    observers.foreach(observer => observer.onSquareSelected(this))
  })

  override def getPiece: Option[ChessPiece] = piece

  override def setPiece(piece: ChessPiece): Unit = {
    this.piece = Some(piece)
    Platform.runLater(() => pieceImg.setImage(chessImages(piece.color)(piece.role)))
  }

  override def removePiece(): Unit = {
    piece = None
    Platform.runLater(() => pieceImg.setImage(null))
  }

  override def setOnHighlighted(isEnabled: Boolean): Unit = {
    isHighlightActive = isEnabled
    Platform.runLater(() => {
      highlightCircle.setVisible(isHighlightEnabled && settingPreferences.isLegalMovesHighlightingEnabled)
      if (piece.nonEmpty) {
        highlightCircle.setFill(Color.ORANGE)
      } else {
        highlightCircle.setFill(Color.DIMGRAY)
      }
    })
  }

  override def setOnLastMoved(isEnabled: Boolean): Unit = {
    isLastMoveActive = isEnabled
    Platform.runLater(() => this.pseudoClassStateChanged(PseudoClasses.lastMoveClass, isLastMoveEnabled))
  }

  override def setSelected(isEnabled: Boolean): Unit = {
    Platform.runLater(() => this.pseudoClassStateChanged(PseudoClasses.selectedClass, isEnabled))
  }

  override def setOnKingCheck(isKingCheck: Boolean): Unit = {
    isKingCheckActive = isKingCheck
    Platform.runLater(() => this.pseudoClassStateChanged(PseudoClasses.kingCheck, isKingCheckActive))
  }

  override def setText(text: String): Unit = {
    squareRect.setVisible(true)
    squareText.setText(text)
    squareText.setVisible(true)
  }

  override def clearText(): Unit = {
    squareRect.setVisible(false)
    squareText.setText("")
    squareText.setVisible(false)
  }

  override def isHighlightEnabled: Boolean = isHighlightActive
  override def isLastMoveEnabled: Boolean = isLastMoveActive
  override def isOnKingCheck: Boolean = isKingCheckActive

  def canEqual(other: Any): Boolean = other.isInstanceOf[FXSquareView]

  override def equals(other: Any): Boolean = other match {
    case that: FXSquareView =>
      (that canEqual this) &&
        boardPosition == that.boardPosition
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(boardPosition)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

}
