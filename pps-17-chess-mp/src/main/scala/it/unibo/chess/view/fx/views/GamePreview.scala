package it.unibo.chess.view.fx.views

import it.unibo.chess.controller.database.ChessGameMongo
import it.unibo.chess.model.core.White
import it.unibo.chess.model.pgn.PgnTagTypes.Black
import it.unibo.chess.view.general.ObservableView
import it.unibo.chess.view.general.observers.GamePreviewObserver
import javafx.application.Platform
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout._
import javafx.scene.text.TextAlignment

/**
  * Represent a view that display a preview of a game, saved in a remote location, ready to show
  * @param game game to display
  */
class GamePreview(game: ChessGameMongo) extends GridPane with ObservableView[GamePreviewObserver] {

  // Game labels
  private val whitePlayerLbl, resultLbl, blackPlayerLbl: Label = new Label() {
    HBox.setHgrow(this, Priority.ALWAYS)
    setId("bigText")
    setAlignment(Pos.CENTER)
    setTextAlignment(TextAlignment.CENTER)
    GridPane.setRowIndex(this, 0)
  }

  // Details labels
  private val viewsLbl, movesLbl, dateLbl: Label = new Label()

  // Game labels containers
  private val leftBox, middleBox, rightBox, detailsBox = new VBox() {
    HBox.setHgrow(this, Priority.ALWAYS)
  }

  // Details containers
  private val viewsContainer, movesContainer, dateContainer = new HBox(){
    setSpacing(5)
    setAlignment(Pos.CENTER_RIGHT)
  }

  Platform.runLater(() => {
    // Adding all values to the view
    whitePlayerLbl.setText(game.players(White.toString))
    blackPlayerLbl.setText(game.players(Black.toString))

    resultLbl.setId("biggerText")
    resultLbl.setText(game.result)

    viewsLbl.setText(game.views.toString)
    movesLbl.setText(game.nMoves.toString)
    dateLbl.setText(game.dateTime)

    // Setting alignment on containers
    leftBox.setAlignment(Pos.CENTER_LEFT)
    middleBox.setAlignment(Pos.CENTER)
    rightBox.setAlignment(Pos.CENTER_RIGHT)
    detailsBox.setAlignment(Pos.CENTER_RIGHT)

    // Populating details containers
    viewsContainer.getChildren.addAll(new Label("Views:"), viewsLbl)
    movesContainer.getChildren.addAll(new Label("Moves:"), movesLbl)
    dateContainer.getChildren.addAll(new Label("Date:"), dateLbl)

    // Populating all with corresponding data
    leftBox.getChildren.addAll(whitePlayerLbl, new Label("(white)"))
    middleBox.getChildren.addAll(resultLbl)
    rightBox.getChildren.addAll(blackPlayerLbl, new Label("(black)"))
    detailsBox.getChildren.addAll(viewsContainer, movesContainer, dateContainer)

    // Setting up parent
    val longColumn = new ColumnConstraints { setPercentWidth(30) }
    val shortColumn = new ColumnConstraints { setPercentWidth(15) }
    val detailColumn = new ColumnConstraints { setPercentWidth(25) }
    
    getColumnConstraints.addAll(longColumn, shortColumn, longColumn, detailColumn)
    setHgap(20)
    setId("roundedBox")

    //Adding to parent
    add(leftBox, 0, 0)
    add(middleBox, 1, 0)
    add(rightBox, 2, 0)
    add(detailsBox, 3, 0)

  })

  this.setOnMouseClicked(_ => {
    this.observers.foreach(_.onGameSelected(game))
  })

}
