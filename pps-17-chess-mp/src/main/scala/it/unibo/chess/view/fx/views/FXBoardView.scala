package it.unibo.chess.view.fx.views

import javafx.application.Platform
import javafx.fxml.FXML
import javafx.geometry.{HPos, Insets}
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.layout.{GridPane, Priority}
import javafx.scene.text.TextAlignment

import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.core.{ChessColor, _}
import it.unibo.chess.model.utilities.CharFormatUtilities
import it.unibo.chess.view.fx.GenericFXView
import it.unibo.chess.view.fx.utilities.SettingPreferences
import it.unibo.chess.view.general.observers.SquareViewObserver
import it.unibo.chess.view.general.views.{RichBoardView, SquareView}

import scala.collection.mutable

/**
  * Represents the fx view for [[RichBoardView]], implementing automatically all the
  * [[it.unibo.chess.view.general.views.BoardView]]. It handle all the actions correlated to a game board, such as
  * displaying the game, moving pieces, display highlight or last move.
  */
class FXBoardView() extends GenericFXView(Some("BoardView.fxml")) with RichBoardView with SquareViewObserver {

  @FXML protected var centerGridPane: GridPane = _

  protected val boardMap = new mutable.HashMap[BoardPosition, SquareView]()
  protected var currentSelectedSquare: Option[SquareView] = None

  protected var isBoardActive = true
  protected var settingPreferences: SettingPreferences = _
  protected var playableForPlayer: Option[ChessColor] = None

  private def generateHorizontallyCenteredLabel(text: String): Label = {
    new Label(text) {
      setTextAlignment(TextAlignment.CENTER)
      setPadding(new Insets(5, 0, 5, 0))
      GridPane.setHgrow(this, Priority.ALWAYS)
      GridPane.setHalignment(this, HPos.CENTER)
    }
  }

  private def generateVerticallyCenteredLabel(text: String): Label = {
    new Label(text) {
      setPadding(new Insets(0, 10, 0, 10))
    }
  }

  private def getColor(col: Int, row: Int): ChessColor = {
    if ((row % 2 == 0 && col % 2 == 0) || (row % 2 != 0 && col % 2 != 0)) White else Black
  }

  override def initBoard(sideSize: Int, settingPreferences: SettingPreferences): Unit = {
    this.settingPreferences = settingPreferences

    // Caching images
    val imageHomeFolder = "/images/chess-images/" + this.settingPreferences.boardStyle.chessImageFolder
    val chessImages: Map[ChessColor, Map[ChessRole, Image]] = Map(
      White -> Map(
        King(White) -> new Image(getClass.getResource(imageHomeFolder + "/wK.png").toExternalForm),
        Queen -> new Image(getClass.getResource(imageHomeFolder + "/wQ.png").toExternalForm),
        Rook -> new Image(getClass.getResource(imageHomeFolder + "/wR.png").toExternalForm),
        Bishop -> new Image(getClass.getResource(imageHomeFolder + "/wB.png").toExternalForm),
        Knight -> new Image(getClass.getResource(imageHomeFolder + "/wN.png").toExternalForm),
        Pawn(White) -> new Image(getClass.getResource(imageHomeFolder + "/wP.png").toExternalForm)
      ),
      Black -> Map(
        King(Black) -> new Image(getClass.getResource(imageHomeFolder + "/bK.png").toExternalForm),
        Queen -> new Image(getClass.getResource(imageHomeFolder + "/bQ.png").toExternalForm),
        Rook -> new Image(getClass.getResource(imageHomeFolder + "/bR.png").toExternalForm),
        Bishop -> new Image(getClass.getResource(imageHomeFolder + "/bB.png").toExternalForm),
        Knight -> new Image(getClass.getResource(imageHomeFolder + "/bN.png").toExternalForm),
        Pawn(Black) -> new Image(getClass.getResource(imageHomeFolder + "/bP.png").toExternalForm)
      )
    )

    // Applying style
    Platform.runLater(() => {
      this.getStylesheets.add(getClass.getResource("/styles/board-styles/" + settingPreferences.boardStyle.cssFileName).toExternalForm)
      centerGridPane.getChildren.clear()
      boardMap.clear()

      // Create the grid
      for (row <- 1 to sideSize) {
        for (col <- 1 to sideSize) {

          // Prepare the square
          val square = new FXSquareView(ChessBoardPosition(col, row), getColor(col, row), chessImages, settingPreferences)
          square.addObserver(this)

          // Set visual improvements
          GridPane.setHgrow(square, Priority.ALWAYS)
          GridPane.setVgrow(square, Priority.ALWAYS)

          // Add it to local map and to the grid pane
          boardMap.put(ChessBoardPosition(col, row), square)
          centerGridPane.add(square, col, sideSize  - row)

        }
      }

      // Draw the rank labels
      for (row <- sideSize to 1 by -1) {
        centerGridPane.add(generateVerticallyCenteredLabel(row.toString), 0, sideSize - row)
      }

      // Draw the file labels
      for (col <- 1 to sideSize) {
        centerGridPane.add(generateHorizontallyCenteredLabel(CharFormatUtilities.intToLetter(col)), col, sideSize)
      }
    })
  }

  override def drawBoard(pieces: Map[BoardPosition, ChessPiece], lastMove: Option[(BoardPosition, BoardPosition)]): Unit = {
    Platform.runLater(() => {
      // Remove all pieces
      boardMap.filter(_._2.getPiece.nonEmpty).foreach(_._2.removePiece())

      // Remove highlight, lastMove and king check
      boardMap.filter(_._2.isHighlightEnabled).foreach(_._2.setOnHighlighted(false))
      boardMap.filter(_._2.isLastMoveEnabled).foreach(_._2.setOnLastMoved(false))
      boardMap.filter(_._2.isOnKingCheck).foreach(_._2.setOnKingCheck(false))

      // There is no current selected square
      currentSelectedSquare.foreach(square => square.setSelected(false))
      currentSelectedSquare = None

      // Show last move, if it's present and enabled
      if (settingPreferences.isLastMoveHighlightingEnabled) {
        lastMove.foreach(move => {
          boardMap(move._1).setOnLastMoved(true)
          boardMap(move._2).setOnLastMoved(true)
        })
      }

      // Add the new pieces
      pieces.foreach(piece => boardMap(piece._1).setPiece(piece._2))
    })
  }

  override def setSquaresToHighlighted(boardPositions: Set[BoardPosition]): Unit = {
    Platform.runLater(() => {
      if (isBoardActive)
        boardMap.filter(p => boardPositions.contains(p._1)).foreach(p => p._2.setOnHighlighted(true))
    })
  }

  override def setSquaresText(squaresText: Map[BoardPosition, String]): Unit = {
    Platform.runLater(() => {
      squaresText.foreach(st => boardMap(st._1).setText(st._2))
    })
  }

  override def clearSquaresText(): Unit = {
    Platform.runLater(() => {
      boardMap.foreach(_._2.clearText())
    })
  }

  override def onSquareSelected(squareView: SquareView): Unit = {
    Platform.runLater(() => {
      if (isBoardActive) {
        // Only if nothing is currently selected and new selected one has a piece inside
        if (currentSelectedSquare.isEmpty && squareView.getPiece.nonEmpty) {
          // If playable player isn't present (LOCAL) or if it's present and he is the owner of that piece (MULTIPLAYER)
          if (playableForPlayer.isEmpty || (playableForPlayer.isDefined && squareView.getPiece.get.color.equals(playableForPlayer.get))) {
            this.observers.foreach(_.onSquareSelected(squareView.boardPosition))
          }
        }

        // A square is currently selected
        else {
          // Only if the new one is highlighted
          if (squareView.isHighlightEnabled) {
            // Try to move the piece
            this.observers.foreach(_.onPieceMove(currentSelectedSquare.get.boardPosition, squareView.boardPosition)())
          } else {
            boardMap.filter(_._2.isHighlightEnabled).foreach(square => square._2.setOnHighlighted(false))
            this.observers.foreach(_.onSquareSelected(squareView.boardPosition))
          }
        }
      }
    })
  }

  override def setSquareSelected(boardPosition: BoardPosition): Unit = {
    Platform.runLater(() => {
      currentSelectedSquare.foreach(square => square.setSelected(false))
      if (isBoardActive) {
        currentSelectedSquare = Some(boardMap(boardPosition))
        currentSelectedSquare.foreach(square => square.setSelected(true))
      }
    })
  }

  override def enableBoard(): Unit = Platform.runLater(() => isBoardActive = true)

  override def disableBoard(): Unit = Platform.runLater(() => isBoardActive = false )

  override def setKingCheck(kingPosition: BoardPosition): Unit = {
    Platform.runLater(() => {
      boardMap
        .filter(_._1 == kingPosition)
        .foreach(_._2.setOnKingCheck(true))
    })
  }

  override def setPlayable(forPlayer: Option[ChessColor]): Unit = {
    this.playableForPlayer = forPlayer

    if (playableForPlayer.isDefined && playableForPlayer.get.equals(Black)) {
      Platform.runLater(() => {
        this.setRotate(180)
        centerGridPane.getChildren.forEach(_.setRotate(180))
      })
    }
  }
}
