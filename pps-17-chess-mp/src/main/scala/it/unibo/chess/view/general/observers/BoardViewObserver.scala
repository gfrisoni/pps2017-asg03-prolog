package it.unibo.chess.view.general.observers

import it.unibo.chess.model.core.position.BoardPosition

/**
  * Represents the observer for the [[it.unibo.chess.view.general.views.BoardView]]
  */
trait BoardViewObserver extends ViewObserver {

  /**
    * Action performed when a square is selected
    * @param boardPosition position of the selected square
    */
  def onSquareSelected(boardPosition: BoardPosition): Unit

  /**
    * Action performed when there is a request to move a piece from a position to another
    * @param from starting position of the piece moving
    * @param to final position of the piece moving
    * @param doOnSuccess action to perform when moving is successful
    * @param doOnFail action to perform when moving fails
    */
  def onPieceMove(from: BoardPosition, to: BoardPosition)
                 (implicit doOnSuccess: () => Unit = () => {}, doOnFail: String => Unit = _ => {}): Unit
}
