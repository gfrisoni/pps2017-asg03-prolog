package it.unibo.chess.view.fx

import it.unibo.chess.view.fx.utilities.ViewLoader
import javafx.scene.layout.BorderPane

/**
  * Represents a generic view made with JavaFX. It can have a .fxml file (defined in resources/layouts folder) and
  * it extends automatically from [[BorderPane]], since it's very common and versatile [[javafx.scene.layout.Pane]] of JavaFX.
  * Automatically, at the start, this class will provide to load and parse the eventually specified .fxml file, thanks to use
  * of the [[ViewLoader]].
  * @param layoutPath
  */
abstract class GenericFXView(val layoutPath: Option[String] = Option.empty) extends BorderPane {
  if (layoutPath.isDefined)
    ViewLoader.loadView(this, layoutPath.get)
}