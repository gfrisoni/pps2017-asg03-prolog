package it.unibo.chess.view.fx.scenes

import javafx.fxml.FXML

import it.unibo.chess.model.core.ChessPiece
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.view.FXWindow
import it.unibo.chess.view.fx.GenericFXView
import it.unibo.chess.view.fx.utilities.SettingPreferences
import it.unibo.chess.view.fx.views.FXBoardView
import it.unibo.chess.view.general.observers.BoardViewObserver
import it.unibo.chess.view.general.scenes.KnightTourScene

case class FXKnightTourScene(override val windowManager: FXWindow, settingPreferences: SettingPreferences)
  extends GenericFXView(Some("KnightTourScene.fxml")) with KnightTourScene {

  @FXML protected var boardView: FXBoardView = _

  override def init(sideSize: Int): Unit = {
    boardView.initBoard(sideSize, settingPreferences)

    observers.foreach(observer => {
      boardView.addObserver(new BoardViewObserver {

        /**
          * Action performed when a square is selected.
          *
          * @param boardPosition the position of the selected square
          */
        override def onSquareSelected(boardPosition: BoardPosition): Unit = observer.onSquareSelected(boardPosition)

        /**
          * Action performed when there is a request to move a piece from a position to another.
          *
          * @param from the starting position of the piece moving
          * @param to the final position of the piece moving
          * @param doOnSuccess the action to perform when moving is successful
          * @param doOnFail the action to perform when moving fails
          */
        override def onPieceMove(from: BoardPosition,
                                 to: BoardPosition)
                                (implicit doOnSuccess: () => Unit,
                                 doOnFail: String => Unit): Unit = observer.onPieceMove(from, to)()

      })
    })
  }

  /**
    * Draws the entire board of the game and the last move if specified.
    *
    * @param pieces the pieces to draw on the board
    * @param lastMove the two positions involved by the last move
    */
  override def drawBoard(pieces: Map[BoardPosition, ChessPiece], lastMove: Option[(BoardPosition, BoardPosition)]): Unit =
    boardView.drawBoard(pieces, lastMove)

  /**
    * Draws the specified positions with an highlight mode.
    *
    * @param boardPositions the positions to highlight
    */
  override def setSquaresToHighlighted(boardPositions: Set[BoardPosition]): Unit =
    boardView.setSquaresToHighlighted(boardPositions)

  /**
    * Puts a square into selected mode.
    *
    * @param boardPosition the position to select
    */
  override def setSquareSelected(boardPosition: BoardPosition): Unit =
    boardView.setSquareSelected(boardPosition)

  /**
    * Enables the board, permitting all the interactions.
    */
  override def enableBoard(): Unit =
    boardView.enableBoard()

  /**
    * Disables the board, blocking all interactions with it.
    */
  override def disableBoard(): Unit =
    boardView.disableBoard()

  /**
    * Sets a square to the king check mode.
    *
    * @param kingPosition the position of the square to set at king check mode
    */
  override def setKingCheck(kingPosition: BoardPosition): Unit = {}

  /**
    * Draw some text on the specified squares.
    *
    * @param squaresText the text associated to each square to update
    */
  override def setSquaresText(squaresText: Map[BoardPosition, String]): Unit =
    boardView.setSquaresText(squaresText)

  /**
    * Remove all text from the squares.
    */
  override def clearSquaresText(): Unit =
    boardView.clearSquaresText()

}
