package it.unibo.chess.view.general.observers

import it.unibo.chess.view.general.views.SquareView

/**
  * Represents the observer for the [[SquareView]]
  */
trait SquareViewObserver extends ViewObserver {

  /**
    * Action performed when a square is selected
    * @param squareView selected square
    */
  def onSquareSelected(squareView: SquareView): Unit

}
