package it.unibo.chess.view.general.observers

import org.mongodb.scala.bson.ObjectId

/**
  * Represents the observer for a [[it.unibo.chess.view.general.scenes.TvScene]]
  */
trait TvSceneObserver extends ViewObserver {

  /**
    * Action performed when view is ready
    */
  def onViewLoaded(): Unit

  /**
    * Action performed when a different type of sorting is selected
    * @param tvGameSortingType sorting type selected by the view
    */
  def onSortingSelected(tvGameSortingType: TvGameSortingType.Value): Unit

  /**
    * Action performed when the user select a game
    * @param _id id of the selected game
    */
  def onGameSelected(_id: ObjectId): Unit

  /**
    * Action performed when the user want go back on previous menu
    */
  def onBack(): Unit

}

/**
  * Represents all the ways to sort games in TvScene
  */
object TvGameSortingType extends Enumeration {
  val Date, Views = Value
  val tvGameSortingTypes = Seq(Date, Views)
}
