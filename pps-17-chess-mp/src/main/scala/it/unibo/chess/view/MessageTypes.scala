package it.unibo.chess.view

/**
  * All available message types
  */
object MessageTypes {
  sealed trait MessageType

  case object Info extends MessageType
  case object Error extends MessageType
  case object Warning extends MessageType
}
