package it.unibo.chess.view.general.intents

import it.unibo.chess.model.core.ChessGame
import it.unibo.chess.view.general.SceneType

/**
  * Represents an advanced intent of the creation of the [[it.unibo.chess.view.general.scenes.GameScene]] that
  * needs some parameters to start, plus identifies that is an intent made by a CHALLENGER.
  * @param sceneType type of the scene that will replace the current one
  * @param game game that will be showed on the [[it.unibo.chess.view.general.scenes.GameScene]]
  * @param clock clock that will be showed on the [[it.unibo.chess.view.general.scenes.GameScene]]
  */
class ChallengerGameSceneIntent(override val sceneType: SceneType.Value,
                                override val game: ChessGame) extends GameSceneIntent(sceneType, game, None)