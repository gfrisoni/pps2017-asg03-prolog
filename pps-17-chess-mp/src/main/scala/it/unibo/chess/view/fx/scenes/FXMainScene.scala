package it.unibo.chess.view.fx.scenes

import it.unibo.chess.view.GenericWindow
import it.unibo.chess.view.fx.GenericFXView
import it.unibo.chess.view.general.scenes.MainScene
import javafx.fxml.FXML
import javafx.scene.control.Button

/**
  * Represent the fx view implementation of the [[MainScene]]. It's the very first scene that appear to the user,
  * permitting to choose if start a game, go to TvMode or go to settings
  * @param windowManager window manager for this scene
  */
case class FXMainScene(override val windowManager: GenericWindow) extends GenericFXView(Some("MainScene.fxml")) with MainScene {
  @FXML protected var startLocalGameBtn: Button = _
  @FXML protected var startMultiPlayerGameBtn: Button = _
  @FXML protected var tvModeBtn: Button = _
  @FXML protected var settingsBtn: Button = _

  this.startLocalGameBtn.setOnMouseClicked(_ => {
    observers.foreach(_.onStartLocalGame())
  })

  this.startMultiPlayerGameBtn.setOnMouseClicked(_ => {
    observers.foreach(_.onStartMultiPlayerGame())
  })

  this.tvModeBtn.setOnMouseClicked(_ => {
    observers.foreach(_.onTvMode())
  })

  this.settingsBtn.setOnMouseClicked(_ => {
    observers.foreach(_.onSettings())
  })
}
