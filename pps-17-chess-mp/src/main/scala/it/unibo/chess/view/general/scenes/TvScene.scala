package it.unibo.chess.view.general.scenes

import it.unibo.chess.controller.database.ChessGameMongo
import it.unibo.chess.model.pgn.FromPgnObserver
import it.unibo.chess.view.general.ObservableView
import it.unibo.chess.view.general.observers.TvSceneObserver

/**
  * Represents the scene that permit to choose one game from a list of saved games and watch it, with interactive history
  */
trait TvScene extends ObservableView[TvSceneObserver] with GenericScene with FromPgnObserver {

  /**
    * Initialize the view
    */
  def init()

  /**
    * Show the games retrived from a source
    * @param games games that will be displayed on the scene
    */
  def showSavedGames(games: Seq[ChessGameMongo]): Unit

  /**
    * Show statistics retrived from a source
    * @param gamesNumber number of currently saved games
    * @param nMoves average move's number of saved games
    * @param duration average duration of saved games
    */
  def showStatistics(gamesNumber: Int, nMoves: Long, duration: Long): Unit

  /**
    * Enable loading of the saved games, when retriving them from a source
    */
  def enableLoading()

  /**
    * Disable all eventual loading present on current scene
    */
  def disableLoading()

}
