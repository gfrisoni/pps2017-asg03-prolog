package it.unibo.chess.view.general

/**
  * All available scenes
  */
object SceneType extends Enumeration {
  val MainScene, SettingsScene, GameScene, LocalGameCreatorScene, TvScene, MultiPlayerGameCreatorScene, KnightTour = Value
}

