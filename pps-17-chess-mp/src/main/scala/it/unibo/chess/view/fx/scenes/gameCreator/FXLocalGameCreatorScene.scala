package it.unibo.chess.view.fx.scenes.gameCreator

import it.unibo.chess.model.modes.ChessModes
import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.view.GenericWindow
import it.unibo.chess.view.fx.GenericFXView
import it.unibo.chess.view.fx.utilities.PseudoClasses
import it.unibo.chess.view.general.scenes.gameCreator.{LocalGameCreatorScene, MultiPlayerGameCreatorScene}
import javafx.application.Platform
import javafx.collections.{FXCollections, ObservableList}
import javafx.fxml.FXML
import javafx.scene.control.{Button, ComboBox, Label, TextField}
import javafx.scene.layout.HBox

import scala.collection.JavaConverters
import scala.concurrent.duration._

/**
  * Represent the fx view implementation for the [[LocalGameCreatorScene]]. It can launch directly a new local game
  * @param windowManager window manager for this scene
  */
case class FXLocalGameCreatorScene(override val windowManager: GenericWindow) extends GenericFXView(Some("LocalGameCreatorScene.fxml")) with LocalGameCreatorScene {

  private val MaxNameLength = 15

  @FXML protected var backBtn, startBtn: Button = _
  @FXML protected var whitePlayerTxt, blackPlayerTxt: TextField = _
  @FXML protected var gameModeCmb: ComboBox[ChessMode] = _
  @FXML protected var timeCmb: ComboBox[FiniteDuration] = _
  @FXML protected var errorLbl: Label = _
  @FXML protected var errorLblContainer: HBox = _

  backBtn.setOnMouseClicked(_ => {
    observers.foreach(_.onBack())
  })

  startBtn.setOnMouseClicked(_ => {
    // Reset pseudo classes
    Platform.runLater(() => {
      whitePlayerTxt.pseudoClassStateChanged(PseudoClasses.errorClass, false)
      blackPlayerTxt.pseudoClassStateChanged(PseudoClasses.errorClass, false)
      gameModeCmb.pseudoClassStateChanged(PseudoClasses.errorClass, false)
      timeCmb.pseudoClassStateChanged(PseudoClasses.errorClass, false)
      errorLblContainer.setVisible(false)
    })

    // Check for empty fields
    if (whitePlayerTxt.getText.isEmpty || blackPlayerTxt.getText.isEmpty ||
      gameModeCmb.getSelectionModel.isEmpty || timeCmb.getSelectionModel.isEmpty) {
      if (whitePlayerTxt.getText.isEmpty)
        Platform.runLater(() => whitePlayerTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true))

      if (blackPlayerTxt.getText.isEmpty)
        Platform.runLater(() => blackPlayerTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true))

      if (gameModeCmb.getSelectionModel.isEmpty)
        Platform.runLater(() => gameModeCmb.pseudoClassStateChanged(PseudoClasses.errorClass, true))

      if (timeCmb.getSelectionModel.isEmpty)
        Platform.runLater(() => timeCmb.pseudoClassStateChanged(PseudoClasses.errorClass, true))

      Platform.runLater(() => {
        errorLblContainer.setVisible(true)
        errorLbl.setText("Fields cannot be empty")
      })
    } else {

      // Check for max length
      if (whitePlayerTxt.getText.length > MaxNameLength || blackPlayerTxt.getText.length > MaxNameLength) {
        if (whitePlayerTxt.getText.length > MaxNameLength)
          Platform.runLater(() => whitePlayerTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true))

        if (blackPlayerTxt.getText.length > MaxNameLength)
          Platform.runLater(() => blackPlayerTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true))

        Platform.runLater(() => {
          errorLblContainer.setVisible(true)
          errorLbl.setText("Max " + MaxNameLength + " characters for a name")
        })
      } else {

        // Name cannot be the same
        if (blackPlayerTxt.getText == whitePlayerTxt.getText){
          Platform.runLater(() => {
            whitePlayerTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true)
            blackPlayerTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true)
            errorLblContainer.setVisible(true)
            errorLbl.setText("Name cannot be the same")
          })
        } else {
          observers.foreach(_.onStart())
        }
      }
    }
  })

  override def getWhitePlayerName: String = whitePlayerTxt.getText
  override def getBlackPlayerName: String = blackPlayerTxt.getText

  override def getSelectedGameMode: ChessMode = gameModeCmb.getSelectionModel.getSelectedItem
  override def getSelectedTime: FiniteDuration = timeCmb.getSelectionModel.getSelectedItem

  override def setSelectableGameModes(gameModes: Seq[ChessModes.ChessMode]): Unit = {
    // Enable the mode list
    val gameModesList: ObservableList[ChessMode] = FXCollections.observableArrayList[ChessMode]
    gameModesList.addAll(JavaConverters.seqAsJavaList(gameModes))

    // Enable the time list
    val timesList: ObservableList[FiniteDuration] = FXCollections.observableArrayList[FiniteDuration]
    timesList.addAll(5.minutes, 10.minutes, 15.minutes, 20.minutes, 30.minutes)

    Platform.runLater(() => {
      gameModeCmb.setItems(gameModesList)
      gameModeCmb.setValue(ChessModes.SuddenDeath)
      gameModeCmb.setDisable(false)

      timeCmb.setItems(timesList)
      timeCmb.setValue(10.minutes)
      timeCmb.setDisable(false)
    })
  }
}
