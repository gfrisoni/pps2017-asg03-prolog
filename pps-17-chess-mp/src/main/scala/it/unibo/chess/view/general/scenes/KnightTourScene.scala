package it.unibo.chess.view.general.scenes

import it.unibo.chess.view.general.ObservableView
import it.unibo.chess.view.general.observers.KnightTourSceneObserver
import it.unibo.chess.view.general.views.BoardView

trait KnightTourScene extends ObservableView[KnightTourSceneObserver] with BoardView with GenericScene {

  /**
    * Initializes the view.
    *
    * @param sideSize the size of the squared board
    */
  def init(sideSize: Int): Unit

}