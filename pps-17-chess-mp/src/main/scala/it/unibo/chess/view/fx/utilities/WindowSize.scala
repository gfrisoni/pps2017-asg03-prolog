package it.unibo.chess.view.fx.utilities

import scala.language.implicitConversions

/**
  * Represents the sizes that a window can have
  */
object WindowSize extends Enumeration {
  protected case class Val(width: Double, height: Double) extends super.Val { }

  implicit def valueToWindowSizeTypeVal(x: Value): Val = x.asInstanceOf[Val]

  val Small = Val(500, 500)
  val Big = Val(1000, 820)
  val BigSquare = Val(800, 800)
}
