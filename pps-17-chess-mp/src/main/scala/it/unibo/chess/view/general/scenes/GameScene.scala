package it.unibo.chess.view.general.scenes

import it.unibo.chess.model.core.{ChessColor, ChessPlayer}
import it.unibo.chess.view.general.ObservableView
import it.unibo.chess.view.general.observers.GameSceneObserver
import it.unibo.chess.view.general.views.{BoardView, DetailsView}
import org.joda.time.DateTime

/**
  * Represents the game scene, where players can play or / and show and interact with history, getting all the details
  * about the game is currently shown
  */
trait GameScene extends ObservableView[GameSceneObserver] with BoardView with DetailsView with GenericScene {

  /**
    * Initialize the view
    * @param players players that will be shown on the view
    * @param sideSize size of the board (if sideSize = 8, the board will be a square of 8x8 squares)
    */
  def init(players: Map[ChessColor, ChessPlayer], sideSize: Int): Unit

  /**
    * Enable a control that shows the FEN, PGN, date and time of current game
    * @param fen FEN of current game
    * @param pgn PGN of current game
    * @param dateTime date and time of current game
    */
  def showDownloadInfo(fen: String, pgn: String, dateTime: DateTime): Unit

}
