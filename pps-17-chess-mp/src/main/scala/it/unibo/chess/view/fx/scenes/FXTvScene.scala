package it.unibo.chess.view.fx.scenes

import it.unibo.chess.controller.database.ChessGameMongo
import it.unibo.chess.model.pgn.FromPgnObserver
import it.unibo.chess.view.FXWindow
import it.unibo.chess.view.fx.GenericFXView
import it.unibo.chess.view.fx.views.GamePreview
import it.unibo.chess.view.general.observers.{GamePreviewObserver, TvGameSortingType}
import it.unibo.chess.view.general.scenes.TvScene
import javafx.application.Platform
import javafx.beans.property.{SimpleBooleanProperty, SimpleDoubleProperty, SimpleIntegerProperty, SimpleStringProperty}
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.scene.control.{Button, ComboBox, Label}
import javafx.scene.layout.{BorderPane, HBox, VBox}

import scala.collection.JavaConverters

/**
  * Represents the fx view implementation of the [[TvScene]]. It displays, thanks to [[GamePreview]] all the saved games
  * @param windowManager window manager for this scene
  */
case class FXTvScene (override val windowManager: FXWindow) extends GenericFXView(Some("TvScene.fxml")) with TvScene with GamePreviewObserver with FromPgnObserver {

  @FXML protected var savedGamesLbl, averageMovesLbl, averageDurationLbl: Label = _
  @FXML protected var sortCmb: ComboBox[TvGameSortingType.Value] = _
  @FXML protected var gamesBox: VBox = _
  @FXML protected var backBtn: Button = _
  @FXML protected var loadingBox: HBox = _
  @FXML protected var gamesBoxContainer: BorderPane = _

  private val isDefiniteLoading = new SimpleBooleanProperty()
  private val lastParsed = new SimpleStringProperty()
  private val progress = new SimpleDoubleProperty()
  private val totalProgress = new SimpleIntegerProperty()
  private var movesCount: Int = 0

  sortCmb.valueProperty().addListener(_ => {
    this.observers.foreach(_.onSortingSelected(sortCmb.getSelectionModel.getSelectedItem))
  })

  backBtn.setOnMouseClicked(_=> {
    this.observers.foreach(_.onBack())
  })

  override def init(): Unit = {
    this.observers.foreach(_.onViewLoaded())

    // Fill the combo with available sorting types
    val sortList = FXCollections.observableArrayList[TvGameSortingType.Value]
    sortList.addAll(JavaConverters.seqAsJavaList(TvGameSortingType.tvGameSortingTypes))
    sortCmb.setItems(sortList)
    sortCmb.getSelectionModel.selectFirst()
  }

  override def showSavedGames(games: Seq[ChessGameMongo]): Unit = {
    Platform.runLater(() => {
      gamesBox.getChildren.clear()

      if (games.nonEmpty)
        gamesBox.getChildren.addAll(JavaConverters.seqAsJavaList(games.map(game => {
          val gamePreview: GamePreview = new GamePreview(game)
          gamePreview.addObserver(this)
          gamePreview
        }
      )))
      else
        gamesBox.getChildren.add(new Label("There are no games yet..."))
    })
  }

  override def showStatistics(gamesNumber: Int, nMoves: Long, duration: Long): Unit = {
    Platform.runLater(() => {
      savedGamesLbl.setText(gamesNumber.toString + " games")
      averageMovesLbl.setText(nMoves.toString + " moves")
      averageDurationLbl.setText(duration.toString + " seconds")
    })
  }

  override def enableLoading(): Unit = {
    Platform.runLater(() => {
      loadingBox.setManaged(true)
      loadingBox.setVisible(true)
      gamesBoxContainer.setManaged(false)
      gamesBoxContainer.setVisible(false)
    })
  }

  override def disableLoading(): Unit = {
    Platform.runLater(() => {
      isDefiniteLoading.set(false)
      loadingBox.setManaged(false)
      loadingBox.setVisible(false)
      gamesBoxContainer.setManaged(true)
      gamesBoxContainer.setVisible(true)
    })
  }

  /**
    * Notifies the total number of moves detected inside the PGN string.
    *
    * @param nMoves the total number of moves
    */
  override def totalMoves(nMoves: Int): Unit = {
    Platform.runLater(() => {
      if (nMoves > 0) {
        isDefiniteLoading.set(true)
        lastParsed.set("")
        progress.set(0)
        totalProgress.set(nMoves)
        this.windowManager.showDefiniteLoadingAlert("Loading this game...", lastParsed, progress, isDefiniteLoading)
      }
    })
  }

  /**
    * Notifies completion of a PGN move parsing.
    *
    * @param san the move expressed in Standard Algebraic Notation
    */
  override def moveParsed(san: String): Unit = {
    Platform.runLater(() => {
      lastParsed.set(san)
      movesCount += 1
      progress.set(movesCount.toDouble / totalProgress.get())

      if (movesCount == totalProgress.get()) {
        isDefiniteLoading.set(false)
      }
    })
  }

  override def onGameSelected(game: ChessGameMongo): Unit = this.observers.foreach(_.onGameSelected(game._id))
}
