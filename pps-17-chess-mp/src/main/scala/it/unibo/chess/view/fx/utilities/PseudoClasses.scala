package it.unibo.chess.view.fx.utilities

import javafx.css.PseudoClass

/**
  * Represents all pseudo-classes you can find on the JavaFX view. It's a handy mode to represent
  * the status in which a view is actually present, and styling it accordingly with css
  */
object PseudoClasses {
  val errorClass: PseudoClass = PseudoClass.getPseudoClass("error")
  val selectedClass: PseudoClass = PseudoClass.getPseudoClass("selected")
  val disabledClass: PseudoClass = PseudoClass.getPseudoClass("disabled")
  val lastMoveClass: PseudoClass = PseudoClass.getPseudoClass("lastMoved")
  val kingCheck: PseudoClass = PseudoClass.getPseudoClass("kingCheck")
  val activeClass: PseudoClass = PseudoClass.getPseudoClass("active")
}
