package it.unibo.chess.view.general.observers.gameCreator

/**
  * Represents the observer for the [[it.unibo.chess.view.general.scenes.gameCreator.MultiPlayerGameCreatorScene]]
  */
trait MultiPlayerGameCreatorSceneObserver extends GameCreatorSceneObserver
