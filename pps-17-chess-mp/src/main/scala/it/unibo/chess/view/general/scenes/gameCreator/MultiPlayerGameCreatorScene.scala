package it.unibo.chess.view.general.scenes.gameCreator

import it.unibo.chess.view.general.observers.gameCreator.MultiPlayerGameCreatorSceneObserver

/**
  * Represents a scene when the player can create a new game or participate to a game in multiplayer mode
  */
trait MultiPlayerGameCreatorScene extends GameCreatorScene[MultiPlayerGameCreatorSceneObserver] {

  /**
    * Get the role of the player (creator or challenger)
    * @return role of the player (creator or challenger)
    */
  def getSelectedPlayerRole: PlayerRole.Value

  /**
    * Get the player name
    * @return string representing the player name
    */
  def getPlayerName: String

}
