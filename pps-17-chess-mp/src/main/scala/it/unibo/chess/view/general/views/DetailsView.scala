package it.unibo.chess.view.general.views

import it.unibo.chess.model.core.ChessRoles.ChessRole
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.core.{ChessColor, ChessPiece, ChessPlayer}
import it.unibo.chess.model.history.ChessActions.ChessEndingAction
import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.model.pgn.{PgnFullMove, PgnMoveIndex}
import it.unibo.chess.view.general.ObservableView
import it.unibo.chess.view.general.observers.DetailsViewObserver

import scala.concurrent.duration.FiniteDuration

/**
  * Represents part of the logic of the board that take care of all game details
  */
trait DetailsView {

  /**
    * Updates the clock of both white and black player
    * @param clock map representing the white and black player clock and it's current value
    */
  def updateClock(clock: Map[ChessColor, FiniteDuration]): Unit

  /**
    * Updates the taken pieces of the player, when an opponent take it.
    * @param player player that must be updated
    * @param takenPieces represents all the pieces taken from the beginning since now
    */
  def updateTakenPieces(player: ChessColor, takenPieces: Seq[ChessPiece]): Unit

  /**
    * Set the current turn to the specified player, enabling / disabling wrong highlights etc...
    * @param chessColor color representing the current turn
    */
  def setTurn(chessColor: ChessColor): Unit

  /**
    * Set the history of the game.
    * @param history represents the whole history of the game, from the beginning since now
    */
  def setHistory(history: (Map[Int, PgnFullMove], Option[String])): Unit

  /**
    * Updates the index of the current move in global history.
    * @param moveIndex index that will be set as current move in global history
    */
  def setCurrentMoveInHistory(moveIndex: Option[PgnMoveIndex]): Unit

  /**
    * Enable or disable the possibility to reproduce automatically, step-by-step the game, basing movements on history.
    * If disabled, there won't be visible any controls to play and stop the history
    * @param isEnabled true to enable this mode, false to disable
    */
  def setPlayModeEnabled(isEnabled: Boolean): Unit

  /**
    * Enable or disable the control that permit to jump directly at the start of the history.
    * @param isEnabled true to enable the control, false to disable
    */
  def setFirstGameStepEnabled(isEnabled: Boolean): Unit

  /**
    * Enable or disable the control that permit to jump one step back in the history
    * @param isEnabled true to enable the control, false to disable
    */
  def setPreviousGameStepEnabled(isEnabled: Boolean): Unit

  /**
    * Enable or disable the control that permit to reproduce automatically, step-by-step the game
    * @param isEnabled true to enable this control, false to disable
    */
  def setPlayEnabled(isEnabled: Boolean): Unit

  /**
    * Enable or disable the control that permit to stop the step-by-step automatic actions performing on history
    * @param isEnabled true to enable this control, false to disable
    */
  def setStopEnabled(isEnabled: Boolean): Unit

  /**
    * Enable or disable the control that permit to jump one step forward in the history
    * @param isEnabled true to enable this control, false to disable
    */
  def setNextGameStepEnabled(isEnabled: Boolean): Unit

  /**
    * Enable or disable the control that permit to jump directly at the last step on the history
    * @param isEnabled true to enable this control, false to disable
    */
  def setLastGameStepEnabled(isEnabled: Boolean): Unit

  /**
    * Enable or disable the control that permit at WHITE player to send a draw request to BLACK player
    * @param isEnabled true to enable this control, false to disable
    */
  def setWhiteDrawRequestEnabled(isEnabled: Boolean): Unit

  /**
    * Enable or disable the control that permit at BLACK player to send a draw request to WHITE player
    * @param isEnabled true to enable this control, false to disable
    */
  def setBlackDrawRequestEnabled(isEnabled: Boolean): Unit

  /**
    * Enable or disable the control that permit to respond at the draw request
    * @param drawRequestMode [[DrawRequestMode.PendingRequest]] will enable the control to accept or reject the request,
    *                       [[DrawRequestMode.Normal]] will enable instead the controls that player can use to request for a draw
    * @param drawApplicant color of the player that is asking for a draw in case of [[DrawRequestMode.PendingRequest]] and
    *                      color of the player that accept / reject the request in case of [[DrawRequestMode.Normal]]
    */
  def setDrawRequestMode(drawRequestMode: DrawRequestMode.Value, drawApplicant: ChessColor): Unit

  /**
    * Enables the possibility to choose a piece, when it's promotion time.
    * @param boardPosition position in which the pieces is actually present
    * @param player color representing the current turn (or player)
    * @param promotableRoles roles that can be chosen to promote the piece
    */
  def enablePromotionSelection(boardPosition: BoardPosition, player: ChessColor, promotableRoles: Seq[ChessRole]): Unit

  /**
    * Enable and disable some controls that show the game is ended.
    * @param endingAction action that closed all the game
    */
  def setGameEnded(endingAction: ChessEndingAction): Unit

  /**
    * When on multiplayer mode, enabling this mode will bring the players to the lobby where they can wait or request
    * for a connection.
    * @param player color of the player, representing the role that this player will have during the game
    */
  def enablePlayerWaiting(player: ChessColor): Unit

  /**
    * Disabling player waiting will bring players to the normal game mode, when they can see all the details about the game
    */
  def disablePlayerWaiting(): Unit

  /**
    * When on multiplayer mode, the CREATOR of a game will have an IP Address. It will be displayed to the user, that can
    * communicate it to his opponent.
    * @param ip string representing the IP Address of the CREATOR
    */
  def setIP(ip: String): Unit

  /**
    * When on multiplayer mode, the CREATOR of a game will have an port. It will be displayed to the user, that can
    * communicate it to his opponent.
    * @param port a number representing the port
    */
  def setPort(port: String): Unit

  /**
    * When on multiplayer mode, the CHALLENGER will connect to an IP Address. This method permit to get user input.
    * @return string representing the IP Address where the CHALLENGER wants to connect
    */
  def getIP: String

  /**
    * When on multiplayer mode, the CHALLENGER will connect to a port. This method permit to get user input.
    * @return string representing the port where the CHALLENGER wants to connect
    */
  def getPort: Int

  /**
    * There are many modes to display a game. By default, the game is shown in "only display mode". This method permit to
    * change this and set entire game as "playable", making it interactive.
    * @param clock value of the clock to display
    * @param gameMode value of the game mode to display
    * @param missingPlayer optional parameter; it specifies that one of the players that eventually was missing, now is
    *                      well known and can be stored. This is a pair of [[ChessColor]] and [[String]]
    */
  def setPlayable(clock: Map[ChessColor, FiniteDuration], gameMode: ChessMode, missingPlayer: Option[(ChessColor, String)] = None)

  /**
    * On multiplayer mode, when the player are waiting to decision if save the game or not, this method will enable
    * a control that shows to players that they made their decision, but the opponent still not and they have to wait.
    */
  def setOnWaitingForSave()
}

/**
  * Represent a trait to a real control that implements the [[DetailsView]] trait, permitting in splitting the responsibility
  * of the game to take care about all the aspects in the [[DetailsView]] trait.
  */
trait RichDetailsView extends ObservableView[DetailsViewObserver] with DetailsView {

  /**
    * Initialize the details view.
    * @param player players that will be displayed
    */
  def initDetails(player: Map[ChessColor, ChessPlayer])

  /**
    * Disable the possibility to choose a piece, after choosing it
    */
  def disablePromotionSelection(): Unit

  /**
    * On multiplayer, it may occur that a player is unknown. This method permit to update a name of a player, if needed
    * @param player color of the player that will change the name.
    * @param name name that will be set as the player's name
    */
  def setPlayerName(player: ChessColor, name: String)
}

/**
  * Represents states of the draw requests
  */
object DrawRequestMode extends Enumeration {
  val PendingRequest, Normal = Value
}

