package it.unibo.chess.view.fx.utilities

import javafx.animation.FadeTransition
import javafx.application.Platform
import javafx.scene.Node
import javafx.util.Duration

/**
  * Represents all the animations the view can perform
  */
object Animations {

  /**
    * Represents all the fade animations the view can perform (fade-in, fade-out)
    */
  object Fade {

    val FADE_DURATION = 400

    private def fade(scene: Node, from: Double, to: Double): Unit = {
      Platform.runLater(() => {
        val fadeIn = new FadeTransition(Duration.millis(FADE_DURATION), scene)
        fadeIn.setFromValue(from)
        fadeIn.setToValue(to)
        fadeIn.play()
      })
    }

    /**
      * Perform a fade-out animation (disappearing) for the specified node
      * @param scene node in which perform the fade-out animation
      */
    def fadeOut(scene: Node): Unit = {
      require (scene != null)
      scene.setOpacity(1)
      fade(scene, 1, 0)
    }

    /**
      * Perform a fade-in animation (appearing) for the specified node
      * @param scene node in which perform the fade-in animation
      */
    def fadeIn(scene: Node): Unit = {
      require (scene != null)
      scene.setOpacity(0)
      fade(scene, 0, 1)
    }
  }
}
