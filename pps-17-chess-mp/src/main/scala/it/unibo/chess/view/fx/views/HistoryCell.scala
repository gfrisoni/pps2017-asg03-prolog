package it.unibo.chess.view.fx.views

import it.unibo.chess.model.core.{Black, ChessColor, White}
import it.unibo.chess.view.fx.utilities.PseudoClasses
import javafx.application.Platform
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.HBox

/**
  * Represents a row on the history, composed by a current turn, white move and (optionally) by black move
  * @param turn string representing the turn at this point on the history
  * @param whiteMove white move performed at this point
  * @param blackMove black move, if performed
  */
class HistoryCell(private val turn: String, private val whiteMove: String, private val blackMove: Option[String] = None) extends HBox {

  private val SMALL_LABEL_WIDTH = 38
  private val BIG_LABEL_WIDTH = 75

  // Set inside controls
  private val turnLbl: Label = generateLabel(turn, Pos.CENTER_LEFT, SMALL_LABEL_WIDTH)
  private val whiteMoveLbl: Label = generateLabel(whiteMove, Pos.CENTER, BIG_LABEL_WIDTH)
  private val blackMoveLbl: Label = generateLabel(blackMove.getOrElse(""), Pos.CENTER, BIG_LABEL_WIDTH)
  private var isEnabled: Option[ChessColor] = None

  Platform.runLater(() => {
    // Set container
    this.setSpacing(10)
    this.getChildren.addAll(turnLbl, whiteMoveLbl, blackMoveLbl)
  })

  /**
    * Permit to highlight one of the present moves or disable entirely the highlight
    * @param color optional paramter, representing the color of the player that will be highlighted. [[White]] means that
    *              white move will be highlighted, same for [[Black]]. When [[None]] is specified, will disable the highlight
    */
  def setEnabled(color: Option[ChessColor]): Unit = {
    color match {
      case Some(White) => Platform.runLater(() => {
        whiteMoveLbl.pseudoClassStateChanged(PseudoClasses.selectedClass, true)
        blackMoveLbl.pseudoClassStateChanged(PseudoClasses.selectedClass, false)
        isEnabled = Some(White)
      })
      case Some(Black) => Platform.runLater(() => {
        whiteMoveLbl.pseudoClassStateChanged(PseudoClasses.selectedClass, false)
        blackMoveLbl.pseudoClassStateChanged(PseudoClasses.selectedClass, true)
        isEnabled = Some(Black)
      })
      case _ => Platform.runLater(() => {
        whiteMoveLbl.pseudoClassStateChanged(PseudoClasses.selectedClass, false)
        blackMoveLbl.pseudoClassStateChanged(PseudoClasses.selectedClass, false)
        isEnabled = None
      })
    }
  }

  /**
    * Get the enable state of this view
    * @return optional representing currently enable highlighted color, none if nothing is enabled
    */
  def getEnableState: Option[ChessColor] = isEnabled

  /**
    * Get the last move present in current view
    * @return [[White]] if there is only white move present, [[Black]] if both moves are present
    */
  def getLastValidColor: ChessColor = {
    if (blackMove.isEmpty) White else Black
  }

  private def generateLabel(text: String, align: Pos, width: Double): Label = {
    new Label(text) {
      setPrefWidth(width)
      setMaxWidth(width)
      setMinWidth(width)
      setAlignment(align)
    }
  }
}