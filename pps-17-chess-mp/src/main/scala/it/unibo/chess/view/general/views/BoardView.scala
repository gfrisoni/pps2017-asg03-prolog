package it.unibo.chess.view.general.views

import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.core.{ChessColor, ChessPiece}
import it.unibo.chess.view.fx.utilities.SettingPreferences
import it.unibo.chess.view.general.ObservableView
import it.unibo.chess.view.general.observers.BoardViewObserver

/**
  * Represents part of the game that manage the board and pieces.
  */
trait BoardView {

  /**
    * Permit to redraw entirely the board, enabling the last moved mode (if it's present).
    * @param pieces all pieces to draw on board
    * @param lastMove optional, permit to enable on two specified positions to the last moved mode
    */
  def drawBoard(pieces: Map[BoardPosition, ChessPiece], lastMove: Option[(BoardPosition, BoardPosition)] = None): Unit

  /**
    * Permit to set on highlight mode specified position of the board.
    * @param boardPositions position that will be highlighted
    */
  def setSquaresToHighlighted(boardPositions: Set[BoardPosition]): Unit

  /**
    * Draw some text on the specified squares.
    * @param squaresText the text associated to each square to update
    */
  def setSquaresText(squaresText: Map[BoardPosition, String]): Unit

  /**
    * Remove all text from the squares.
    */
  def clearSquaresText(): Unit

  /**
    * Permit to select a square, putting it into selected mode.
    * @param boardPosition position of square to select
    */
  def setSquareSelected(boardPosition: BoardPosition): Unit

  /**
    * Enable the board, permitting all interactions you can expect from it.
    */
  def enableBoard()

  /**
    * Disable the board, blocking all interactions with it.
    */
  def disableBoard()

  /**
    * Permit to set a square, containing a king, to the king check mode.
    * @param kingPosition position of the square to set at king check mode
    */
  def setKingCheck(kingPosition: BoardPosition)
}

/**
  * A real control that implements the [[BoardView]] trait, permitting in splitting the responsibility
  * of the game to take care about all the aspects in the [[BoardView]] trait.
  */
trait RichBoardView extends ObservableView[BoardViewObserver] with BoardView {

  /**
    * Initialize the board, drawing it.
    * @param sideSize number representing the length of a square side (board of with sideSize = 8 will be a square of 8x8 squares)
    * @param settingPreferences parameters needed to determinate eventual player's preferences about the game
    */
  def initBoard(sideSize: Int, settingPreferences: SettingPreferences): Unit

  /**
    * Permit to set the board enabled only for BLACK or WHITE player, not both. If nothing passed, the board will be
    * enabled to both players.
    * @param forPlayer optional; indicates what player can interact with the board. If none is specified, all players can
    *                  iteract with the board
    */
  def setPlayable(forPlayer: Option[ChessColor] = None)
}

