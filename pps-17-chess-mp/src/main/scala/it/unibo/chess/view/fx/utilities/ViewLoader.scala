package it.unibo.chess.view.fx.utilities

import javafx.fxml.FXMLLoader
import javafx.scene.layout.Pane

/**
  * Represents a helper object that can load and parse a .fxml file and set automatically the pane as the root and
  * controller, following the JavaFX logic (that's slightly different from the MVC pattern)
  */
object ViewLoader {

  /**
    * Permit to load a .fxml file and assign the JavaFX controller and root to the indicated pane
    * @param controller represent the root of the .fxml file and the JavaFX controller
    * @param layoutPath file name (or path to it): must be located in resources/layouts path
    * @return [[Pane]] representing the control to use as JavaFX view
    */
  def loadView(controller: Pane, layoutPath: String): Pane = {
    val loader = new FXMLLoader(getClass.getResource("/layouts/" + layoutPath))
    loader.setRoot(controller)
    loader.setController(controller)
    loader.load()
  }
}
