package it.unibo.chess.view.general

import it.unibo.chess.view.general.observers.ViewObserver

/**
  * Represents a view that can be observed by a [[ViewObserver]]
  * @tparam O type of the observer that can observe this particular view
  */
trait ObservableView[O <: ViewObserver] {
  protected var observers: Set[O] = Set.empty

  /**
    * Add a observer to the view
    * @param observer observer that will observe the view
    */
  def addObserver(observer: O): Unit = {
    this.observers += observer
  }
}

