package it.unibo.chess.view.general.scenes.gameCreator

import it.unibo.chess.view.general.observers.gameCreator.LocalGameCreatorSceneObserver

/**
  * Represents a scene when the player can create a new game in local mode
  */
trait LocalGameCreatorScene extends GameCreatorScene[LocalGameCreatorSceneObserver] {

  /**
    * Get the name associated with WHITE player
    * @return string represening the name of WHITE player
    */
  def getWhitePlayerName: String

  /**
    * Get the name associated with BLACK player
    * @return string represening the name of BLACK player
    */
  def getBlackPlayerName: String

}
