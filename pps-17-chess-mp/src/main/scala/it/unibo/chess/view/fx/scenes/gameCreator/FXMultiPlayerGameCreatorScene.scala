package it.unibo.chess.view.fx.scenes.gameCreator

import it.unibo.chess.model.modes.ChessModes
import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.view.GenericWindow
import it.unibo.chess.view.fx.GenericFXView
import it.unibo.chess.view.fx.utilities.PseudoClasses
import it.unibo.chess.view.general.scenes.gameCreator.{MultiPlayerGameCreatorScene, PlayerRole}
import javafx.application.Platform
import javafx.collections.{FXCollections, ObservableList}
import javafx.fxml.FXML
import javafx.scene.control.{Button, ComboBox, Label, TextField}
import javafx.scene.layout.HBox

import scala.collection.JavaConverters
import scala.concurrent.duration._

/**
  * Represent the fx view implementation for the [[MultiPlayerGameCreatorScene]]. It can launch a new game enabled
  * to take care about all multiplayer things
  * @param windowManager window manager for this scene
  */
case class FXMultiPlayerGameCreatorScene(override val windowManager: GenericWindow) extends GenericFXView(Some("MultiPlayerGameCreatorScene.fxml")) with MultiPlayerGameCreatorScene {

  private val MaxNameLength = 15

  @FXML protected var roleCmb: ComboBox[PlayerRole.Value] = _
  @FXML protected var gameModeCmb: ComboBox[ChessMode] = _
  @FXML protected var timeCmb: ComboBox[FiniteDuration] = _

  @FXML protected var playerNameTxt: TextField = _

  @FXML protected var errorLbl: Label = _
  @FXML protected var errorLblContainer: HBox = _

  @FXML protected var backBtn, startBtn: Button = _

  private var isAllOk: Boolean = true

  roleCmb.valueProperty().addListener(_ => {
    roleCmb.getSelectionModel.getSelectedItem match {
      case PlayerRole.Challenger =>
        gameModeCmb.setVisible(false)
        timeCmb.setVisible(false)
        startBtn.setDisable(false)
        startBtn.setDisable(false)
        startBtn.setText("Connect to a game")
      case PlayerRole.Creator =>
        gameModeCmb.setVisible(true)
        timeCmb.setVisible(true)
        startBtn.setDisable(false)
        startBtn.setText("Create a new game")
      case _ =>
        startBtn.setDisable(true)
        startBtn.setText("Select role first")
    }
  })

  backBtn.setOnMouseClicked(_ => {
    observers.foreach(_.onBack())
  })

  startBtn.setOnMouseClicked(_ => {
    Platform.runLater(() => {
      // Reset pseudo classes
      roleCmb.pseudoClassStateChanged(PseudoClasses.errorClass, false)
      gameModeCmb.pseudoClassStateChanged(PseudoClasses.errorClass, false)
      timeCmb.pseudoClassStateChanged(PseudoClasses.errorClass, false)
      playerNameTxt.pseudoClassStateChanged(PseudoClasses.errorClass, false)
      errorLblContainer.setVisible(false)
      isAllOk = true

      // Check for empty fields
      if (playerNameTxt.getText.isEmpty || roleCmb.getSelectionModel.isEmpty) {

        if (playerNameTxt.getText.isEmpty)
          playerNameTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true)

        if (roleCmb.getSelectionModel.isEmpty)
          roleCmb.pseudoClassStateChanged(PseudoClasses.errorClass, true)

        errorLblContainer.setVisible(true)
        errorLbl.setText("Fields cannot be empty")
        isAllOk = false
      }

      // Length of the name
      if (playerNameTxt.getText.length > MaxNameLength) {
        playerNameTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true)
        errorLblContainer.setVisible(true)
        errorLbl.setText("Max " + MaxNameLength + " characters for a name")
        isAllOk = false
      }

      // If I'm creator, I must check for some more fields
      if (roleCmb.getSelectionModel.getSelectedItem.equals(PlayerRole.Creator) && isAllOk) {
        if (gameModeCmb.getSelectionModel.isEmpty || timeCmb.getSelectionModel.isEmpty) {

          if (gameModeCmb.getSelectionModel.isEmpty)
            gameModeCmb.pseudoClassStateChanged(PseudoClasses.errorClass, true)

          if (timeCmb.getSelectionModel.isEmpty)
            timeCmb.pseudoClassStateChanged(PseudoClasses.errorClass, true)

          errorLblContainer.setVisible(true)
          errorLbl.setText("Fields cannot be empty")
          isAllOk = false
        }
      }

      if (isAllOk) observers.foreach(_.onStart())

    })
  })

  override def getSelectedPlayerRole: PlayerRole.Value = roleCmb.getSelectionModel.getSelectedItem

  override def getSelectedGameMode: ChessMode = gameModeCmb.getSelectionModel.getSelectedItem

  override def getSelectedTime: FiniteDuration = timeCmb.getSelectionModel.getSelectedItem

  override def getPlayerName: String = playerNameTxt.getText


  override def setSelectableGameModes(gameModes: Seq[ChessModes.ChessMode]): Unit = {
    // Enable the mode list
    val gameModesList: ObservableList[ChessMode] = FXCollections.observableArrayList[ChessMode]
    gameModesList.addAll(JavaConverters.seqAsJavaList(gameModes))

    // Enable the time list
    val timesList: ObservableList[FiniteDuration] = FXCollections.observableArrayList[FiniteDuration]
    timesList.addAll(5.minutes, 10.minutes, 15.minutes, 20.minutes, 30.minutes)

    val rolesList: ObservableList[PlayerRole.Value] = FXCollections.observableArrayList[PlayerRole.Value]
    rolesList.addAll(PlayerRole.Creator, PlayerRole.Challenger)

    Platform.runLater(() => {
      gameModeCmb.setItems(gameModesList)
      gameModeCmb.setValue(ChessModes.SuddenDeath)
      gameModeCmb.setDisable(false)

      timeCmb.setItems(timesList)
      timeCmb.setValue(10.minutes)
      timeCmb.setDisable(false)

      roleCmb.setItems(rolesList)
      roleCmb.getSelectionModel.selectFirst()
      roleCmb.setDisable(false)
    })
  }
}
