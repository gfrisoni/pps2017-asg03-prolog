package it.unibo.chess.view.general.observers

import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.core.{ChessColor, ChessRoles}

/**
  * Represents the observer for the [[it.unibo.chess.view.general.views.DetailsView]]
  */
trait DetailsViewObserver extends ViewObserver {

  /**
    * Action performed when user wants to jump directly at the very start of the history
    */
  def onFirstGameStep(): Unit

  /**
    * Action performed when user wants to jump one step back on the history
    */
  def onPreviousGameStep(): Unit

  /**
    * Action performed when user wants to play automatically, step-by-step the history
    */
  def play(): Unit

  /**
    * Action performed when user wants to stop previously enabled automatic play
    */
  def stop(): Unit

  /**
    * Action performed when user wants to jump one step forward on the history
    */
  def onNextGameStep(): Unit

  /**
    * Action performed when user wants to jump directly ad the last action of the history
    */
  def onLastGameStep(): Unit

  /**
    * Action performed when user wants to save details about the game
    */
  def onDownloadRequest(): Unit

  /**
    * Action performed when user wants to exit the game
    * @param isSavingEnabled multiplayer-specific parameter, that indicates if user wanted or not to save this game.
    *                        By default the parameter is set to FALSE.
    */
  def onExitGame(isSavingEnabled: Boolean = false): Unit

  /**
    * Action performed when user select the piece after a promote request
    * @param boardPosition position when the promotable piece was
    * @param role role to assign at the piece
    * @param doOnSuccess action performed when promotion is successful
    * @param doOnFail action performed when promotion fails
    */
  def onPromotionSelected(boardPosition: BoardPosition, role: ChessRoles.ChessRole)
                         (implicit doOnSuccess: () => Unit = () => {}, doOnFail: String => Unit = _ => {}): Unit

  /**
    * Action performed when a user send a request of DRAW or when the user respond to a request.
    * @param color user that requested a DRAW or user that is responding for the DRAW
    * @param status in case of request, set this parameter on TRUE. In case of responding for a draw, this parameter
    *               is the response: TRUE means the opponent accepted the draw request, FALSE that it was rejected
    */
  def onDrawApplication(color: ChessColor, status: Boolean): Unit

  /**
    * Multiplayer action, performed when a CHALLENGER wants to join a game
    */
  def onConnect(): Unit
  
}