package it.unibo.chess.view.fx.views

import java.text.SimpleDateFormat

import it.unibo.chess.model.core.ChessRoles.ChessRole
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.history.ChessActions.{ChessEndingAction, LoseAction}
import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.model.pgn.{PgnFullMove, PgnMoveIndex}
import it.unibo.chess.model.utilities.ChessUtilities.AnUtilities
import it.unibo.chess.view.fx.GenericFXView
import it.unibo.chess.view.fx.utilities.PseudoClasses
import it.unibo.chess.view.general.views.{DrawRequestMode, RichDetailsView}
import javafx.application.Platform
import javafx.fxml.FXML
import javafx.scene.control.{Button, Label, TextField}
import javafx.scene.layout.{HBox, Priority, VBox}

import scala.collection.JavaConverters
import scala.concurrent.duration.FiniteDuration

/**
  * Represents the fx view for [[RichDetailsView]], implementing in this way all the stuff from
  * [[it.unibo.chess.view.general.views.DetailsView]]. It handle all interactions that are not correlated to the
  * game board, such as current turn, name of players, promotions, draw requests and all multiplayer things.
  */
case class FXDetailsView() extends GenericFXView(Some("DetailsView.fxml")) with RichDetailsView {

  // For the multi-player
  @FXML protected var waitingResponseBox, waitingForPlayerBox, gameBox: VBox = _
  @FXML protected var waitingLoadingBox, waitSaveResponseBox: HBox = _
  @FXML protected var waitingLbl, ipLbl, portLbl: Label = _
  @FXML protected var ipTxt, portTxt, playerNameTxt: TextField = _
  @FXML protected var connectBtn: Button = _

  // For a generic game and TV
  @FXML protected var gameModeLbl: Label = _
  @FXML protected var playerWhiteLbl, playerWhiteClockLbl, playerWhitePieces: Label = _
  @FXML protected var playerBlackLbl, playerBlackClockLbl, playerBlackPieces: Label = _
  @FXML protected var playerWhiteBox, playerBlackBox: VBox = _
  @FXML protected var historyBox, promoteBox, historyCellsBox, promoteButtonsBox: VBox = _

  @FXML protected var goBackBtn, goForwardBtn, playBtn, stopBtn, goAllBackBtn, goAllForwardBtn, downloadBtn: Button = _
  @FXML protected var resultBox, drawButtonBox, drawButtonYesNoBox: HBox = _
  @FXML protected var resultLbl, drawTitleLbl: Label = _
  @FXML protected var blackAskingForDrawBtn, whiteAskingForDrawBtn, exitBtn, exitSaveBtn, drawYesBtn, drawNoBtn: Button = _

  private val timeFormat = new SimpleDateFormat("mm:ss.SSS")
  private var currentPendingRequest: Option[ChessColor] = None
  private var playableForPlayer: Option[ChessColor] = None

  connectBtn.setOnMouseClicked(_ => {
    Platform.runLater(() => {
      // Reset error status
      ipTxt.pseudoClassStateChanged(PseudoClasses.errorClass, false)
      portTxt.pseudoClassStateChanged(PseudoClasses.errorClass, false)

      // Check for empty fields
      if (ipTxt.getText.isEmpty || portTxt.getText.isEmpty) {
        if (ipTxt.getText.isEmpty)
          ipTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true)

        if (portTxt.getText.isEmpty)
          portTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true)

      } else {
        try {
          // Check for integer conversion
          portTxt.getText.toInt
          observers.foreach(_.onConnect())
        }
        catch {
          case _: Exception =>
            // Int conversion failed
            portTxt.pseudoClassStateChanged(PseudoClasses.errorClass, true)
        }
      }
    })
  })

  // Buttons to control history and download
  goAllBackBtn.setOnMouseClicked(_ => observers.foreach(_.onFirstGameStep()))
  goBackBtn.setOnMouseClicked(_ => observers.foreach(_.onPreviousGameStep()))
  playBtn.setOnMouseClicked(_ => observers.foreach(_.play()))
  stopBtn.setOnMouseClicked(_ => observers.foreach(_.stop()))
  goForwardBtn.setOnMouseClicked(_ => observers.foreach(_.onNextGameStep()))
  goAllForwardBtn.setOnMouseClicked(_ => observers.foreach(_.onLastGameStep()))

  downloadBtn.setOnMouseClicked(_ => {
    observers.foreach(observer => observer.onDownloadRequest())
  })

  // Buttons to control draw and game exit
  blackAskingForDrawBtn.setOnMouseClicked(_ => {
    observers.foreach(_.onDrawApplication(Black, status = true))
  })

  whiteAskingForDrawBtn.setOnMouseClicked(_ => {
    observers.foreach(_.onDrawApplication(White, status = true))
  })

  drawYesBtn.setOnMouseClicked(_ => {
    if (currentPendingRequest.isDefined)
      observers.foreach(_.onDrawApplication(currentPendingRequest.get, status = true))
  })

  drawNoBtn.setOnMouseClicked(_ => {
    if (currentPendingRequest.isDefined)
      observers.foreach(_.onDrawApplication(currentPendingRequest.get, status = false))
  })

  exitBtn.setOnMouseClicked(_ => {
    observers.foreach(_.onExitGame())
  })

  exitSaveBtn.setOnMouseClicked(_ => {
    observers.foreach(_.onExitGame(true))
  })

  private def setWhiteTurn(): Unit = {
      playerWhiteBox.pseudoClassStateChanged(PseudoClasses.activeClass, true)
      playerBlackBox.pseudoClassStateChanged(PseudoClasses.activeClass, false)
  }

  private def setBlackTurn(): Unit = {
      playerWhiteBox.pseudoClassStateChanged(PseudoClasses.activeClass, false)
      playerBlackBox.pseudoClassStateChanged(PseudoClasses.activeClass, true)
  }

  private def setViewGameOver(isGameOver: Boolean): Unit = {
    Platform.runLater(() => {
      drawTitleLbl.setManaged(!isGameOver)
      drawTitleLbl.setVisible(!isGameOver)
      drawButtonBox.setManaged(!isGameOver)
      drawButtonBox.setVisible(!isGameOver)
      exitBtn.setManaged(isGameOver)
      exitBtn.setVisible(isGameOver)

      // This guys shouldn't be reactivated
      if (isGameOver) {
        drawButtonYesNoBox.setManaged(!isGameOver)
        drawButtonYesNoBox.setVisible(!isGameOver)
      }

      // If the game is over and it was a multiplayer one
      if (playableForPlayer.isDefined && isGameOver) {
        exitSaveBtn.setManaged(isGameOver)
        exitSaveBtn.setVisible(isGameOver)
      }
    })
  }

  private def takenPiecesRepresentation(player: ChessColor, takenPieces: Seq[ChessPiece]): String = {
    takenPieces
      .groupBy(_.role)
      .map(roleGroup => AnUtilities.figurine(!player, roleGroup._1) + "x" + roleGroup._2.size)
      .mkString(" ")
  }

  override def initDetails(player: Map[ChessColor, ChessPlayer]): Unit = {
    Platform.runLater(() => {
      playerWhiteLbl.setText(player(White).name)
      playerBlackLbl.setText(player(Black).name)
      setViewGameOver(true)
    })
  }

  override def setPlayable(clock: Map[ChessColor, FiniteDuration], gameMode: ChessMode, missingPlayer: Option[(ChessColor, String)]): Unit = {
    Platform.runLater(() => {
      // Set game mode label
      gameModeLbl.setText(gameMode.name)
      gameModeLbl.setManaged(true)
      gameModeLbl.setVisible(true)

      // Update the clock of both players
      updateClock(clock)
      playerWhiteClockLbl.setManaged(true)
      playerWhiteClockLbl.setVisible(true)
      playerBlackClockLbl.setManaged(true)
      playerBlackClockLbl.setVisible(true)

      // Disable "GameOver" mode
      setViewGameOver(false)
      playableForPlayer = if (missingPlayer.isEmpty) None else Some(!missingPlayer.get._1)

      // If the game is multiplayer
      if (playableForPlayer.isDefined) {
        drawTitleLbl.setText("Send draw request to " + playableForPlayer.get)

        playableForPlayer.get match {
          case White =>
            // For white, enable only the button for the black player
            blackAskingForDrawBtn.setManaged(false)
            blackAskingForDrawBtn.setVisible(false)
            whiteAskingForDrawBtn.setManaged(true)
            whiteAskingForDrawBtn.setVisible(true)
            whiteAskingForDrawBtn.setText("Send")

          case Black =>
            // For black, enable only the button for the white player
            blackAskingForDrawBtn.setManaged(true)
            blackAskingForDrawBtn.setVisible(true)
            blackAskingForDrawBtn.setText("Send")
            whiteAskingForDrawBtn.setManaged(false)
            whiteAskingForDrawBtn.setVisible(false)
        }
      }
    })
  }

  override def updateClock(clock: Map[ChessColor, FiniteDuration]): Unit = {
    Platform.runLater(() => {
      playerWhiteClockLbl.setText(timeFormat.format(clock(White)._1))
      playerBlackClockLbl.setText(timeFormat.format(clock(Black)._1))
    })
  }

  override def updateTakenPieces(player: ChessColor, takenPieces: Seq[ChessPiece]): Unit = {
    player match {
      case White => Platform.runLater(() =>
        playerWhitePieces.setText(takenPiecesRepresentation(!player, takenPieces)))
      case Black => Platform.runLater(() =>
        playerBlackPieces.setText(takenPiecesRepresentation(!player, takenPieces)))
    }
  }

  override def setTurn(chessColor: ChessColor): Unit = {
    chessColor match {
      case White => Platform.runLater(() => setWhiteTurn())
      case Black => Platform.runLater(() => setBlackTurn())
    }
  }

  override def setHistory(history: (Map[Int, PgnFullMove], Option[String])): Unit = {
    Platform.runLater(() => {
      // Clear and show the history
      historyCellsBox.getChildren.clear()
      history._1.toSeq.sortBy(_._1).foreach(value => {
        historyCellsBox.getChildren.add(new HistoryCell(value._1.toString, value._2.whiteSanMove, value._2.blackSanMove))
      })

      // Show (eventually) the result
      history._2.fold({
        resultBox.setManaged(false)
        resultBox.setVisible(false)
      })(res => {
        resultBox.setManaged(true)
        resultBox.setVisible(true)
        resultLbl.setText("Result: " + res)
      })
    })
  }

  override def setCurrentMoveInHistory(moveIndex: Option[PgnMoveIndex]): Unit = {
    Platform.runLater(() => {
      // Reset the color in each turn row
      historyCellsBox.getChildren.forEach(n => n.asInstanceOf[HistoryCell].setEnabled(None))
      // Set active cell (according to the fact that historyCellsBox has a 0-based index)
      moveIndex.foreach(index => historyCellsBox.getChildren.get(index.turnIndex - 1)
        .asInstanceOf[HistoryCell]
        .setEnabled(Some(index.playerIndex))
      )
    })
  }

  override def enablePromotionSelection(boardPosition: BoardPosition,
                                        chessColor: ChessColor,
                                        promotableRoles: Seq[ChessRole]): Unit = {
    Platform.runLater(() => {
      // Enable promotion buttons only if is LOCAL or is exactly the view of the player is currently playing
      if (playableForPlayer.isEmpty || (playableForPlayer.isDefined && playableForPlayer.get.equals(chessColor))) {
        promoteButtonsBox.getChildren.clear()
        promoteButtonsBox.getChildren.addAll(
          JavaConverters.seqAsJavaList(
            promotableRoles.map(role =>
              new HBox(new Button(AnUtilities.figurine(chessColor, role)) {
                setOnMouseClicked(_ => {
                  disablePromotionSelection()
                  observers.foreach(_.onPromotionSelected(boardPosition, role))
                })
                HBox.setHgrow(this, Priority.ALWAYS)
                setMaxWidth(Double.MaxValue)
              })
            )
          )
        )
        historyBox.setVisible(false)
        historyBox.setManaged(false)
        promoteBox.setVisible(true)
        promoteBox.setManaged(true)
      }
    })
  }

  override def disablePromotionSelection(): Unit = {
    Platform.runLater(() => {
      historyBox.setVisible(true)
      historyBox.setManaged(true)
      promoteBox.setVisible(false)
      promoteBox.setManaged(false)
    })
  }

  override def setGameEnded(endingAction: ChessEndingAction): Unit = {
    endingAction match {
      case LoseAction(loserColor, _) =>
        // Highlight the color that win
        loserColor match {
          case White => Platform.runLater(() => setBlackTurn())
          case Black => Platform.runLater(() => setWhiteTurn())
        }
      case _ =>
        // Highlight both, it's DRAW
        Platform.runLater(() => {
          playerBlackBox.pseudoClassStateChanged(PseudoClasses.activeClass, true)
          playerWhiteBox.pseudoClassStateChanged(PseudoClasses.activeClass, true)
        })
    }

    setViewGameOver(true)
  }

  override def setPlayModeEnabled(isEnabled: Boolean): Unit = {
    Platform.runLater(() => {
      playBtn.setManaged(isEnabled)
      playBtn.setVisible(isEnabled)
      stopBtn.setManaged(isEnabled)
      stopBtn.setVisible(isEnabled)
    })
  }

  override def setFirstGameStepEnabled(isEnabled: Boolean): Unit = Platform.runLater(() => goAllBackBtn.setDisable(!isEnabled))

  override def setPreviousGameStepEnabled(isEnabled: Boolean): Unit = Platform.runLater(() => goBackBtn.setDisable(!isEnabled))

  override def setPlayEnabled(isEnabled: Boolean): Unit = Platform.runLater(() => playBtn.setDisable(!isEnabled))

  override def setStopEnabled(isEnabled: Boolean): Unit = Platform.runLater(() => stopBtn.setDisable(!isEnabled))

  override def setNextGameStepEnabled(isEnabled: Boolean): Unit = Platform.runLater(() => goForwardBtn.setDisable(!isEnabled))

  override def setLastGameStepEnabled(isEnabled: Boolean): Unit = Platform.runLater(() => goAllForwardBtn.setDisable(!isEnabled))

  override def setWhiteDrawRequestEnabled(isEnabled: Boolean): Unit = {
    Platform.runLater(() => {
      whiteAskingForDrawBtn.setManaged(isEnabled)
      whiteAskingForDrawBtn.setVisible(isEnabled)
    })
  }

  override def setBlackDrawRequestEnabled(isEnabled: Boolean): Unit = {
    Platform.runLater(() => {
        blackAskingForDrawBtn.setManaged(isEnabled)
        blackAskingForDrawBtn.setVisible(isEnabled)
    })
  }

  override def setDrawRequestMode(drawRequestMode: DrawRequestMode.Value, color: ChessColor): Unit = {
    drawRequestMode match {

      case DrawRequestMode.PendingRequest => Platform.runLater(() => {
        // Only if is LOCAL or the view is of the player that I'm asking for a DRAW
        if (playableForPlayer.isEmpty || (playableForPlayer.isDefined && playableForPlayer.get.equals(!color))) {
          drawButtonYesNoBox.setManaged(true)
          drawButtonYesNoBox.setVisible(true)
          drawButtonBox.setManaged(false)
          drawButtonBox.setVisible(false)
          currentPendingRequest = Some(!color)
          drawTitleLbl.setText(color + " asked for a draw.\nDo you want to accept?")
        } else {
          // This is my view (multiplayer), I must wait for a respond
          drawButtonBox.setManaged(false)
          drawButtonBox.setVisible(false)
          drawTitleLbl.setText("Waiting " + !color + " respond...")
        }
      })

      case DrawRequestMode.Normal => Platform.runLater(() => {
        // Disable all actions
        drawButtonYesNoBox.setManaged(false)
        drawButtonYesNoBox.setVisible(false)
        drawButtonBox.setManaged(true)
        drawButtonBox.setVisible(true)
        currentPendingRequest = None

        // Check if is LOCAL (reset message) or Multiplayer (message + response or just message)
        if (playableForPlayer.isEmpty)
          drawTitleLbl.setText("Who is asking for draw?")
        else if (playableForPlayer.get.equals(!color))
          drawTitleLbl.setText(color + " refused.\nSend draw request to " + color)
        else
          drawTitleLbl.setText("Send draw request to " + !color)

      })
    }
  }

  override def enablePlayerWaiting(missingColor: ChessColor): Unit = {
    Platform.runLater(() => {
      // Disable the first window
      waitingResponseBox.setVisible(false)
      waitingResponseBox.setManaged(false)

      missingColor match {
        case White =>
          // Waiting for input to connecto to CREATOR
          waitingLbl.setText("Connecting to creator...")
          playerNameTxt.setText(playerBlackLbl.getText)
          ipLbl.setText("Creator IP:")
          ipTxt.setEditable(true)
          ipTxt.setPromptText("Insert creator IP")
          portLbl.setText("Creator port:")
          portTxt.setEditable(true)
          portTxt.setPromptText("Insert creator port")
          connectBtn.setVisible(true)
          connectBtn.setManaged(true)
          waitingLoadingBox.setVisible(false)
          waitingLoadingBox.setManaged(false)

        case Black =>
          // Waiting for a CHALLENGER making a request to me
          waitingLbl.setText("Waiting for challenger...")
          playerNameTxt.setText(playerWhiteLbl.getText)
          ipLbl.setText("Your IP:")
          ipTxt.setEditable(false)
          portLbl.setText("Your port:")
          portTxt.setEditable(false)
          connectBtn.setVisible(false)
          connectBtn.setManaged(false)
          waitingLoadingBox.setVisible(true)
          waitingLoadingBox.setManaged(true)
      }

      // Enable this window
      waitingForPlayerBox.setVisible(true)
      waitingForPlayerBox.setManaged(true)
    })
  }

  override def disablePlayerWaiting(): Unit = {
    Platform.runLater(() => {
      waitingResponseBox.setVisible(false)
      waitingResponseBox.setManaged(false)
      waitingForPlayerBox.setVisible(false)
      waitingForPlayerBox.setManaged(false)
      gameBox.setVisible(true)
      gameBox.setManaged(true)
    })
  }

  override def setIP(ip: String): Unit = Platform.runLater(() => ipTxt.setText(ip))
  override def setPort(port: String): Unit = Platform.runLater(() => portTxt.setText(port))
  override def getIP: String = ipTxt.getText
  override def getPort: Int = portTxt.getText.toInt

  override def setPlayerName(chessColor: ChessColor, name: String): Unit = {
    Platform.runLater(() => {
      chessColor match {
        case White => playerWhiteLbl.setText(name)
        case Black => playerBlackLbl.setText(name)
      }
    })
  }

  override def setOnWaitingForSave(): Unit = {
    exitSaveBtn.setVisible(false)
    exitSaveBtn.setManaged(false)
    exitBtn.setVisible(false)
    exitBtn.setManaged(false)
    waitSaveResponseBox.setVisible(true)
    waitSaveResponseBox.setManaged(true)
  }
}
