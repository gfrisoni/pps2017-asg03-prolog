package it.unibo.chess.view.general.observers.gameCreator

import it.unibo.chess.view.general.observers.ViewObserver

/**
  * Represents a generic observer for a game creator
  */
trait GameCreatorSceneObserver extends ViewObserver{

  /**
    * Action to perform when user wants to go back on previous menu
    */
  def onBack(): Unit

  /**
    * Action to perform when user wants to start the game
    */
  def onStart(): Unit

}
