package it.unibo.chess.view.general.scenes.gameCreator

/**
  * Represent all roles a player can assume during a multiplayer game
  */
object PlayerRole extends Enumeration {
  val Creator, Challenger = Value
}
