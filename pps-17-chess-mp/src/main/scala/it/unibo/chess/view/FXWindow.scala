package it.unibo.chess.view

import java.lang
import javafx.application.Platform
import javafx.beans.property.{BooleanProperty, DoubleProperty, StringProperty}
import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.scene.control.Alert.AlertType
import javafx.scene.control._
import javafx.scene.effect.BoxBlur
import javafx.scene.image.Image
import javafx.scene.layout.{BorderPane, VBox}
import javafx.scene.{Node, Scene}
import javafx.stage.{Screen, Stage, StageStyle}

import it.unibo.chess.controller.controllers.actors.MultiplayerRoles
import it.unibo.chess.controller.controllers.actors.MultiplayerRoles.{Challenger, Creator}
import it.unibo.chess.controller.controllers.game.{KnightTourSceneControllerImpl, MultiplayerGameSceneControllerImpl, NormalGameSceneControllerImpl, TvGameSceneControllerImpl}
import it.unibo.chess.controller.controllers.{LocalGameCreatorSceneControllerImpl, MainSceneControllerImpl, SettingSceneControllerImpl, _}
import it.unibo.chess.controller.database.{ChessGameDatabaseFunctions, ChessMultiplayerDatabase}
import it.unibo.chess.controller.preferences.PreferencesHandler
import it.unibo.chess.controller.traits.scenes._
import it.unibo.chess.controller.traits.scenes.game.KnightTourSceneController
import it.unibo.chess.model.core.{Black, White}
import it.unibo.chess.model.utilities.ChessUtilities
import it.unibo.chess.view.MessageTypes.{Error, Info, MessageType, Warning}
import it.unibo.chess.view.fx.scenes.gameCreator.{FXLocalGameCreatorScene, FXMultiPlayerGameCreatorScene}
import it.unibo.chess.view.fx.scenes.{FXGameScene, FXMainScene, FXSettingsScene, _}
import it.unibo.chess.view.fx.utilities.{Animations, SettingPreferences, WindowSize}
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.{ChallengerGameSceneIntent, CreatorGameSceneIntent, GameSceneIntent, Intent}
import it.unibo.chess.view.general.scenes._
import it.unibo.chess.view.general.scenes.gameCreator.{LocalGameCreatorScene, MultiPlayerGameCreatorScene}

case class FXWindow(stage: Stage) extends GenericWindow {

  private var currentScene: Option[SceneType.Value] = None
  private val windowContent = new BorderPane()

  Platform.runLater(() => {
    stage.setResizable(false)
    windowContent.getStylesheets.add(getClass.getResource("/styles/MainStyle.css").toExternalForm)
    this.stage.getIcons.addAll(
      new Image(getClass.getResource("/images/icon/icon16x16.png").toExternalForm),
      new Image(getClass.getResource("/images/icon/icon32x32.png").toExternalForm),
      new Image(getClass.getResource("/images/icon/icon64x64.png").toExternalForm))
    stage.setScene(new Scene(windowContent))
    this.stage.setOnCloseRequest(_ => closeView())
  })

  private def showMessageHelper(title: Option[String], message: String, messageType: MessageType): Unit = {
    Platform.runLater(() => {
      val alert = generateAlert(messageType)
      val dialog: DialogPane = alert.getDialogPane

      dialog.setHeaderText(title.orNull)
      dialog.setGraphic(null)
      alert.setContentText(message)

      showAlert(alert)
    })
  }

  private def generateAlert(messageType: MessageType): Alert = {
    val alertPair: (String, AlertType) = messageType match {
      case Info =>  ("Information", AlertType.INFORMATION)
      case Error => ("Error", AlertType.ERROR)
      case Warning => ("Warning", AlertType.WARNING)
    }

    new Alert(alertPair._2) {
      initOwner(stage)
      initStyle(StageStyle.TRANSPARENT)
      setTitle(alertPair._1)
      getDialogPane.getStylesheets.add(getClass.getResource("/styles/DialogStyle.css").toExternalForm)
      getDialogPane.getStyleClass.add("myDialog")
    }
  }

  private def showAlert(alert: Alert): Unit = {
    windowContent.setEffect(new BoxBlur(5, 10, 10))

    alert.showAndWait().ifPresent(_ => {
      windowContent.setEffect(new BoxBlur(0, 0, 0))
    })
  }

  private def centerOnScreen(): Unit = {
    val primScreenBounds = Screen.getPrimary.getVisualBounds
    this.stage.setX((primScreenBounds.getWidth - this.stage.getWidth) / 2)
    this.stage.setY((primScreenBounds.getHeight - this.stage.getHeight) / 2)
  }

  override def showView(): Unit = {
    require(currentScene.nonEmpty)
    Platform.runLater(() => {
      centerOnScreen()
      this.stage.show()
    })
  }

  override def closeView(): Unit = {
    this.stage.close()
    Platform.exit()
    System.exit(0)
  }

  override def isShowing: Boolean = {
    this.stage.isShowing
  }

  override def setTitle(title: String): Unit = {
    this.stage.setTitle(title)
  }

  override def getTitle: String = {
    this.stage.getTitle
  }

  override def setScene(intent: Intent): Unit = {

    currentScene = Some(intent.sceneType)

    // If there was a previous scene, animate it
    if (windowContent.getCenter != null && windowContent.getCenter.getParent != null)
      Animations.Fade.fadeOut(windowContent.getCenter)

    intent.sceneType match {
      case SceneType.MainScene =>
        val mainScene: MainScene = FXMainScene(this)
        val mainSceneController: MainSceneController = MainSceneControllerImpl()

        mainScene.addObserver(mainSceneController)
        mainSceneController.setView(mainScene)

        Platform.runLater(() => {
          windowContent.setCenter(mainScene.asInstanceOf[FXMainScene])
          Animations.Fade.fadeIn(mainScene.asInstanceOf[FXMainScene])
        })

      case SceneType.SettingsScene =>
        val settingsScene: SettingsScene = FXSettingsScene(this)
        val settingsSceneController: SettingsSceneController = SettingSceneControllerImpl()

        settingsScene.addObserver(settingsSceneController)
        settingsSceneController.setView(settingsScene)
        settingsSceneController.init()

        Platform.runLater(() => {
          windowContent.setCenter(settingsScene.asInstanceOf[FXSettingsScene])
          Animations.Fade.fadeIn(settingsScene.asInstanceOf[FXSettingsScene])
        })

      case SceneType.LocalGameCreatorScene =>
        val singlePlayerGameScene: LocalGameCreatorScene = FXLocalGameCreatorScene(this)
        val singlePlayerGameSceneController: LocalGameCreatorSceneController = LocalGameCreatorSceneControllerImpl()

        singlePlayerGameScene.addObserver(singlePlayerGameSceneController)
        singlePlayerGameSceneController.setView(singlePlayerGameScene)

        Platform.runLater(() => {
          windowContent.setCenter(singlePlayerGameScene.asInstanceOf[FXLocalGameCreatorScene])
          Animations.Fade.fadeIn(singlePlayerGameScene.asInstanceOf[FXLocalGameCreatorScene])
        })

      case SceneType.MultiPlayerGameCreatorScene =>
        val multiPlayerGameScene: MultiPlayerGameCreatorScene = FXMultiPlayerGameCreatorScene(this)
        val multiPlayerGameSceneController: MultiPlayerGameCreatorSceneController = MultiPlayerGameCreatorSceneControllerImpl()

        multiPlayerGameScene.addObserver(multiPlayerGameSceneController)
        multiPlayerGameSceneController.setView(multiPlayerGameScene)

        Platform.runLater(() => {
          windowContent.setCenter(multiPlayerGameScene.asInstanceOf[FXMultiPlayerGameCreatorScene])
          Animations.Fade.fadeIn(multiPlayerGameScene.asInstanceOf[FXMultiPlayerGameCreatorScene])
        })

      case SceneType.GameScene =>
        require(intent.isInstanceOf[GameSceneIntent])
        val gameIntent = intent.asInstanceOf[GameSceneIntent]
        val gameScene: GameScene = FXGameScene(this, SettingPreferences(
          PreferencesHandler.getLegalMovesPreference,
          PreferencesHandler.getLastMovePreference,
          PreferencesHandler.getStylePreference)
        )

        new Thread(() => {
          intent match {

            // Creates multiplayer controller without timer -> challenger will have those from creator
            case _: ChallengerGameSceneIntent =>
              val controller = new MultiplayerGameSceneControllerImpl(gameIntent.game)
              val actor = MultiplayerRoles.launchActor(Challenger)(controller, ChessGameDatabaseFunctions(ChessMultiplayerDatabase()))
              gameScene.addObserver(controller)
              controller.setView(gameScene)
              controller.localInit(actor, Black)

            // Creates controller with everything -> I'm the creator
            case _: CreatorGameSceneIntent =>
              val controller = new MultiplayerGameSceneControllerImpl(gameIntent.game){
                setGameClock(gameIntent.clock.get)
              }
              val actor = MultiplayerRoles.launchActor(Creator)(controller, ChessGameDatabaseFunctions(ChessMultiplayerDatabase()))
              gameScene.addObserver(controller)
              controller.setView(gameScene)
              controller.localInit(actor, White)

            // In any other case, if clock is defined is a local game, otherwise TvMode
            case _ =>
              if (gameIntent.clock.isDefined) {
                val controller = new NormalGameSceneControllerImpl(gameIntent.game) {
                  setGameClock(gameIntent.clock.get)
                }
                gameScene.addObserver(controller)
                controller.setView(gameScene)
              } else {
                val controller = TvGameSceneControllerImpl(gameIntent.game)
                gameScene.addObserver(controller)
                controller.setView(gameScene)
              }
          }
        }).start()

        Platform.runLater(() => {
          windowContent.setCenter(gameScene.asInstanceOf[FXGameScene])
          Animations.Fade.fadeIn(gameScene.asInstanceOf[FXGameScene])
        })

      case SceneType.TvScene =>
        val tvScene: TvScene = FXTvScene(this)
        val tvSceneController: TvSceneController = TvSceneControllerImpl()

        tvScene.addObserver(tvSceneController)
        tvSceneController.setView(tvScene)

        Platform.runLater(() => {
          windowContent.setCenter(tvScene.asInstanceOf[FXTvScene])
          Animations.Fade.fadeIn(tvScene.asInstanceOf[FXTvScene])
        })

      case SceneType.KnightTour =>
        val knightTourScene: KnightTourScene = FXKnightTourScene(this, SettingPreferences(
          PreferencesHandler.getLegalMovesPreference,
          PreferencesHandler.getLastMovePreference,
          PreferencesHandler.getStylePreference)
        )
        val knightTourSceneController: KnightTourSceneController = KnightTourSceneControllerImpl()

        knightTourScene.addObserver(knightTourSceneController)
        knightTourSceneController.setView(knightTourScene)
        knightTourScene.init(ChessUtilities.BOARD_END_INDEX)

        Platform.runLater(() => {
          windowContent.setCenter(knightTourScene.asInstanceOf[FXKnightTourScene])
          Animations.Fade.fadeIn(knightTourScene.asInstanceOf[FXKnightTourScene])
        })

      case _ => println("Missing scene")
    }

    if (currentScene.isDefined) {
      if (currentScene.get.equals(SceneType.GameScene) || currentScene.get.equals(SceneType.TvScene)) {
        if (stage.getWidth != WindowSize.Big.width || stage.getHeight != WindowSize.Big.height) {
          Platform.runLater(() => {
            stage.setHeight(WindowSize.Big.height)
            stage.setWidth(WindowSize.Big.width)
            windowContent.setId("mainUndecoratedContainer")
            centerOnScreen()
          })
        }
      } else if (currentScene.get.equals(SceneType.KnightTour)) {
        if (stage.getWidth != WindowSize.BigSquare.width || stage.getHeight != WindowSize.BigSquare.height) {
          Platform.runLater(() => {
            stage.setHeight(WindowSize.BigSquare.height)
            stage.setWidth(WindowSize.BigSquare.width)
            windowContent.setId("mainUndecoratedContainer")
            centerOnScreen()
          })
        }
      } else {
        if (stage.getWidth != WindowSize.Small.width || stage.getHeight != WindowSize.Small.height) {
          Platform.runLater(() => {
            stage.setHeight(WindowSize.Small.height)
            stage.setWidth(WindowSize.Small.width)
            windowContent.setId("mainDecoratedContainer")
            centerOnScreen()
          })
        }
      }
    }
  }

  override def getScene: Option[SceneType.Value] = currentScene

  override def showMessage(message: String, messageType: MessageType): Unit = {
    showMessageHelper(None, message, messageType)
  }

  override def showMessage(title: String, message: String, messageType: MessageType): Unit = {
    showMessageHelper(Some(title), message, messageType)
  }

  /**
    * Show a dialog with a custom content
    * @param content a node representing the content of the message
    * @param messageType type of the message
    */
  def showCustomMessage(content: Node, messageType: MessageType): Unit = {
    Platform.runLater(() => {
      val alert = generateAlert(messageType)

      alert.getDialogPane.setHeaderText(null)
      alert.getDialogPane.setGraphic(null)
      alert.getDialogPane.setContent(content)
      alert.getButtonTypes.clear()
      alert.getButtonTypes.add(new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE))

      showAlert(alert)
    })
  }

  /**
    * Show a dialog with a loading bar
    * @param title title of the loading
    * @param textDone string representing what is currently loading
    * @param progress double value between 0 (0%) and 1 (100%) representing current progress
    * @param isLoading boolean property that the message will watch. If it will became FALSE it will close
    */
  def showDefiniteLoadingAlert(title: String, textDone: StringProperty, progress: DoubleProperty, isLoading: BooleanProperty): Unit = {
    Platform.runLater(() => {
      val alert = generateAlert(MessageTypes.Info)

      alert.getDialogPane.setHeaderText(null)
      alert.getDialogPane.setGraphic(null)
      alert.getButtonTypes.clear()
      alert.getDialogPane.setContent(
        new VBox(
          new Label(title),
          new ProgressBar(){ progressProperty().bind(progress) },
          new Label(){ textProperty().bind(textDone) }
        ){ setSpacing(5) }
      )

      isLoading.addListener(new ChangeListener[lang.Boolean] {
        override def changed(observable: ObservableValue[_ <: lang.Boolean], oldValue: lang.Boolean, newValue: lang.Boolean): Unit = {
          Platform.runLater(() => {
            if (!isLoading.get()) {
              alert.getButtonTypes.clear()
              alert.getDialogPane.getButtonTypes.addAll(ButtonType.CANCEL)
              alert.close()
            }
          })
        }
      })

      showAlert(alert)
    })
  }

}