package it.unibo.chess.view.general.scenes

import it.unibo.chess.view.general.ObservableView
import it.unibo.chess.view.general.observers.MainSceneObserver

/**
  * Represents the main scene, that will open at the very start of the application
  */
trait MainScene extends ObservableView[MainSceneObserver] with GenericScene
