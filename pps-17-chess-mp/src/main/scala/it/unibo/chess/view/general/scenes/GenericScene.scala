package it.unibo.chess.view.general.scenes

import it.unibo.chess.view.GenericWindow

/**
  * Trait representing all the scenes. They all will have a Window Manager, that's a [[GenericWindow]] with
  * some features a scene can call
  */
trait GenericScene {

  /**
    * Represents the window manager for the current scene
    */
  val windowManager: GenericWindow
}
