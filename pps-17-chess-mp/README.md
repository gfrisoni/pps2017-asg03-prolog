# PPS-17-chess-mp

This project aims at making a multiplayer chess game.

## Versioning

We use [SemVer](http://semver.org/) for versioning.  

* **[0.1.0]** 27/08/2018 [(Backlog)](/backlogs/sprint1_backlog.md)  
* **[0.2.0]** 03/09/2018 [(Backlog)](/backlogs/sprint2_backlog.md)  
* **[0.3.0]** 10/09/2018 [(Backlog)](/backlogs/sprint3_backlog.md)  
* **[0.4.0]** 17/09/2018 [(Backlog)](/backlogs/sprint4_backlog.md)  
* **[0.5.0]** 24/09/2018 [(Backlog)](/backlogs/sprint5_backlog.md)  
* **[0.6.0]** 01/10/2018 [(Backlog)](/backlogs/sprint6_backlog.md)  
* **[1.0.0]** 08/10/2018 [(Backlog)](/backlogs/sprint7_backlog.md)  
* **[1.1.0]** 13/10/2018

## Authors

* **Giacomo Frisoni** - https://bitbucket.org/gfrisoni/
* **Leonardo Montini** - https://bitbucket.org/montini/
* **Marcin Pabich** - https://bitbucket.org/marcin_pabich/
* **Sofia Rossi** - https://bitbucket.org/SofiaRossi/

## Trello
[Board](https://trello.com/b/gHz8yaRD/chess-pps)