|  **Priorità** | **Oggetto** | **Stima iniziale** | **1** | **2** | **3** | **4** | **5** | **6** | **7** | **8** |
|  :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: |
|  1 | Sviluppo componenti base del gioco | 25 | 25 | 22 |  |  |  |  |  |  |
|  2 | Sviluppo componente grafica della schermata di menù | 9 | 9 | 8 |  |  |  |  |  |  |
|  3 | Modellazione base di una partita in locale | 7 |  |  |  |  |  |  |  |  |
|  4 | Gestione della history di gioco | 3 | 3 |  |  |  |  |  |  |  |
|  5 | Gestione della notazione algebrica | 11 | 11 |  |  |  |  |  |  |  |
|  6 | Gestione base del FEN  | 6 | 6 | 3 |  |  |  |  |  |  |
|  7 | Modellazione completa di una partita in locale | 25 | 25 | 12 |  |  |  |  |  |  |
|  8 | Sviluppo componente grafica della schermata di gioco | 12 | 12 | 12 | 12 | 12 | 7 | 3 |  |  |
|  9 | Gestione della notazione PGN | 14 | 12 | 12 | 9 | 2 | 2 |  |  |  |
|  10 | Gestione base di una partita in locale (interazioni Model-Controller) | 10 | 10 | 13 | 4 | 1 |  |  |  |  |
|  11 | Sviluppo differenti modalità di gioco | 5 | 5 | 5 | 5 |  |  |  |  |  |
|  12 | Sviluppo multiplayer peer-to-peer | 13 | 13 | 13 | 13 | 7 |  |  |  |  |
|  13 | Gestione di una partita in remoto | 13 | 13 | 13 | 13 | 13 | 3 |  | 3 |  |
|  14 | Possibilità di ripercorrere una partita step by step | 6 | 6 | 4 | 3 | 3 | 3 | 1 |  |  |
|  15 | Modalità TV | 8 | 8 | 8 | 8 | 8 | 8 | 2 |  |  |
|  16 | Sviluppo del minigioco | 3 | 3 | 3 | 3 | 3 | 3 | 3 | 3 | 3 |
|  17 | Menù per le impostazioni | 5 | 5 | 5 | 5 | 5 | 5 | 5 |  |  |
|  18 | Suoni | 4 | 4 | 4 | 4 | 4 | 4 | 4 | 4 | 4 |