#Sprint 1 backlog

|  **Oggetto Backlog** | **Sprint Task** | **Responsabile** | **Stima iniziale** | **1** | **2** | **3** | **4** | **5** | **6** |
|  :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: |
|  Sviluppo componenti base del gioco | Posizione | Giacomo Frisoni | 8 | 4 | 4 | 4 | 6 | 6 |  |
|   | Pezzo | Leonardo Montini | 1 | 1 | 1 | 1 |  |  |  |
|   | Scacchiera | Leonardo Montini | 5 | 5 | 5 | 5 | 4 |  |  |
|   | Movimento dei ruoli | Leonardo Montini | 8 | 8 | 6 | 2 |  |  |  |
|   | Colore | Leonardo Montini | 3 | 3 | 3 |  |  |  |  |
|  Sviluppo componenti grafici del gioco | Studio della libreria ScalaFX con relativo testing | Marcin Pabich | 5 | 7 | 6 | 4 | 4 | 2 |  |
|   | Sviluppo di una basilare schermata di avvio | Marcin Pabich | 4 | 4 | 5 | 4 | 2 | 2 |  |
|  Modellazione base di una partita in locale | Scelta dei nomi | Sofia Rossi | 1 | 1 | 1 | 1 | 1 | 1 |  |
|   | Istanziazione della partita | Sofia Rossi | 4 | 4 | 4 | 4 | 4 | 4 |  |
|   | Alternanza dei turni | Sofia Rossi | 2 | 2 | 2 | 2 | 2 | 2 |  |