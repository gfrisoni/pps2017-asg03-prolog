|  **Oggetto Backlog** | **Sprint Task** | **Responsabile** | **Stima iniziale** | **1** | **2** | **3** | **4** | **5** | **6** |
|  :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: |
|  Gestione della notazione PGN | Rappresentazione PGN del Game | Giacomo Frisoni | 8 | 10 | 8 | 7 | 5 | 2 |  |
|   | Creazione di un Game a partire dalla relativa rappresentazione in formato PGN | Giacomo Frisoni | 1 | 1 | 1 | 1 | 1 |  |  |
|  Gestione base di una partita in locale (interazioni Model-Controller) | Sviluppo di una logica temporale relativa al clock rate (uniformazione dei tick di gioco con l'aggiornamento del timer) | Leonardo Montini | 3 | 2 | 1 |  |  |  |  |
|  Sviluppo differenti modalità di gioco | Gestione di SuddenDeath, Hourglass, Compensation | Leonardo Montini | 2 | 1 |  |  |  |  |  |
|  Sviluppo server multiplayer | Studio e test implementativi delle possibili tecnologie per lo sviluppo del backend | Sofia Rossi | 6 | 6 | 5 | 5 |  |  |  |