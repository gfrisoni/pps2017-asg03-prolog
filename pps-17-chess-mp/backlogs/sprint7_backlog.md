|  **Sprint Task** | **Responsabile** | **Stima iniziale** | **1** | **2** | **3** | **4** | **5** | **6** |
|  :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: |
|  Aggancio alla View e introduzione di Play/Stop | Giacomo Frisoni | 1 | 4 | 3 | 2 |  |  |  |
|  Rappresentazione dei pezzi mediante immagini | Marcin Pabich | 1 | 1 | 1 | 1 | 1 |  |  |
|  Gestione della patta lato View | Marcin Pabich | 2 | 2 | 1 | 1 | 1 | 1 |  |
|  Gestione della dialog di download | Marcin Pabich | 3 | 3 | 2 | 2 | 1 | 1 |  |
|  Visual improvements | Marcin Pabich | 1 | 1 | 1 | 1 | 1 | 1 |  |
|  Salvataggio delle preferenze lato Controller | Giacomo Frisoni | 2 | 2 | 2 |  |  |  |  |
|  Gestione completa della schermata lato View | Marcin Pabich | 2 | 2 | 2 | 2 | 2 | 1 |  |
|  Gestione dei temi | Marcin Pabich | 1 | 1 | 1 | 1 | 1 |  |  |
|  Creazione del TVObserver e implementazione del controller per l'aggancio alla View | Sofia Rossi | 2 | 2 | 2 | 1 | 1 |  |  |
|  Progettazione e implementazione della schermata TV | Marcin Pabich | 3 | 3 | 3 | 2 | 2 | 1 |  |
|  Implementazione del controller per il download della PGN (su File) e la consultazione della FEN legata alla situazione corrente di gioco | Giacomo Frisoni | 2 | 2 | 2 | 2 | 2 |  |  |
|  Gestione della patta lato Controller | Leonardo Montini | 2 | 2 | 2 | 1 |  |  |  |